import datetime
import json
import lmdb
import numpy as np
from pathlib import Path
import shutil
from sqlitedict import SqliteDict
from tqdm import tqdm
from typing import Dict, List, Union
import requests
import zipfile

import var_setup

np.random.seed(var_setup.random_state)

### JSON ###

"""
NOTE: 
- JSON does not handle streaming / batching large files
- JSON does not support appending
"""

def load_json(
    file_path: str
    ) -> dict:
    """
    Load dict from JSON file

    Input:
    - file_path : path to JSON file

    Output:
    - json_dict : dict stored in JSON file
    """
    with open(file_path, "r") as f:
        json_dict = json.load(f)

    print(f"..... done loading {len(json_dict)} entries from JSON")

    return json_dict

def save_json(
    file: Union[dict, list], 
    file_name: str,
    file_dir: str
    ):
    """
    Save Python object (e.g. dict, list) to JSON file

    Input:
    - file : obj
    - file_name : saved file name
    - file_dir : saved directory name
    """
    print(f"..... saving {len(file)} entries to JSON")

    # create file_dir if not exists
    Path(file_dir).mkdir(
        parents=True, 
        exist_ok=True
        )

    file_path = Path(file_dir).joinpath(file_name)

    with open(file_path, "w") as f:
        json.dump(file, f, ensure_ascii=False, indent=4)

### TXT ###

def load_txt(
    file_path: str
    ) -> List[str]:
    """
    Load list from TXT file

    Input:
    - file_path : path to TXT file

    Output:
    - stored_data : list stored in TXT file
    """

    with open(file_path, "r") as f:
        stored_data = [line.strip() for line in f.readlines() if line.endswith("\n") and line.strip() != ""]

    print(f"..... done loading {len(stored_data)} entries from TXT")
    
    return stored_data

def stream_txt(
    file_path: str
    ) -> str:
    """
    Stream-load list or dict from TXT file

    Input:
    - file_path : path to TXT file

    Output:
    - stored_data : list stored in TXT file
    """

    with open(file_path, "r") as f:
        for line in f.readlines():
            if line.endswith("\n") and line.strip() != "":
                yield line.strip()

def save_txt(
    file: List[str], 
    file_name: str,
    file_dir: str,
    mode: str
    ):
    """
    Save Python list of texts to TXT file

    Input:
    - file : list of texts
    - file_name : saved file name
    - file_dir : saved directory name
    - mode : either "w" for write or "a" for append
    """
    print(f"..... saving {len(file)} entries to TXT")

    # create file_dir if not exists
    Path(file_dir).mkdir(
        mode=0o755,
        parents=True, 
        exist_ok=True
        )

    file_path = Path(file_dir).joinpath(file_name)
    with open(file_path, mode) as f:
        for item in tqdm(file):
            f.write(f"{item}\n")

### ZIP ###

def download_file(
    data_url: str,
    data_dir: str,
    folder_name: str,
    recompute: bool
    ) -> str:
    """
    Download ZIP file from url and save into specified path

    Input
    - data_url : url of the file
    - data_dir : saved data directory name
    - folder_name : name for parent folder of the file (inside data_dir)
    - recompute : whether to re-download the file, if the file already exists

    Output
    - file_path : path to the downloaded ZIP file
    """
    print(f"..... downloading file from {data_url} {datetime.datetime.now()}")

    # create data_dir if not exists
    # default: data/glove/
    file_dir = Path(data_dir).joinpath(folder_name)
    file_dir.mkdir(
        mode=0o755,
        parents=True, 
        exist_ok=True
        )

    # retrieve file name
    # default: globe.6B.zip
    file_name = data_url.split("/")[-1]

    print(f"..... to save {file_name} into {file_dir}/")
    
    # get file path
    # default: data/glove/globe.6B.zip
    file_path = Path.joinpath(file_dir, file_name)

    # remove folder if re-compute anyway
    if recompute and file_path.is_file():
        Path(file_path).unlink()

    # download zipped file if not already exists
    if not Path(file_path).is_file():
        with requests.get(data_url, stream=True) as r:
            # check if https request returns error status
            r.raise_for_status()
            with open(file_path, "wb") as f:
                # low chunk size to save memory
                # but too low add time to switch btw r/w
                for chunk in r.iter_content(chunk_size=4096):
                    f.write(chunk)
    
    return str(file_path)

def extract_zip(
    file_path: str,
    remove_zip: bool = False,
    recompute: bool = False
    ):
    """
    Extract ZIP file save into specified path

    Input
    - file_path : path to the extracted file
    - remove_zip : whether to remove the ZIP file after extraction
    - recompute : whether to re-extract the ZIP file, if the folder already exists
    """
    folder_path = Path(file_path.replace(".zip", ""))

    # remove folder if re-compute anyway
    if recompute and folder_path.is_dir():
        shutil.rmtree(save_dir)

    # extract zipped file into folder if folder not already existed (or re-compute anyway)
    if not folder_path.is_dir():
        print(f"..... extracting zip file {Path(file_path).name} {datetime.datetime.now()}")
        # extract zipped file into folder
        zip_ref = zipfile.ZipFile(file_path, "r")

        for file_in_zip in tqdm(zip_ref.namelist()):
            save_member_path = Path(file_path).parent.joinpath(file_in_zip)
            if not save_member_path.is_file():
                print(f"....... processing {save_member_path.name}  {datetime.datetime.now()}")
                zip_ref.extract(
                    member=file_in_zip,
                    path=save_member_path.parent
                    )
                print(f"....... DONE")
            else:
                print(f"....... used cache {save_member_path.name}")
        zip_ref.close()

    # remove zipped file after extract completed, to save space
    if remove_zip:
        Path(file_path).unlink()

def download_and_extract_zip(
    data_url: str,
    data_dir: str, 
    folder_name: str,
    remove_zip: bool = False,
    recompute: bool = False
    ) -> str:
    """
    Download and extract ZIP files 
    (e.g. Here: GLoVe embeddings, WebOfScience database)

    Input:
    - data_url : download url of the data
    - data_dir : folder to save data
    - folder_name : folder inside data_dir to store extracted files

    Output:
    - file_dir : name of folder containing the extracted files

    Examples:
    wos_dir = data_helper.download_and_extract_zip(
        data_url="https://data.mendeley.com/public-files/datasets/9rw3vkcfy4/files/c9ea673d-5542-44c0-ab7b-f1311f7d61df/file_downloaded", 
        data_dir="data", 
        folder_name="wos"
        )
    """
    file_path = download_file(
        data_url=data_url, 
        data_dir=data_dir, 
        folder_name=folder_name,
        recompute=recompute
        )
    extract_zip(
        file_path=file_path, 
        remove_zip=remove_zip,
        recompute=recompute
        )
    return str(Path(file_path).parent)

### NPY ###

def load_npy(
    file_path: str,
    mmap_mode: str = "r"
    ) -> np.ndarray:
    return np.load(
        file_path,
        mmap_mode=mmap_mode
        )

def save_npy(
    file: np.ndarray,
    file_name: str,
    file_dir: str
    ):
    """
    Save np.array to .npy file

    Input:
    - file : obj
    - file_name : saved file name
    - file_dir : saved directory name
    """
    print(f"..... saving np object of shape {file.shape} to NPY")

    # create file_dir if not exists
    Path(file_dir).mkdir(
        parents=True, 
        exist_ok=True
        )

    file_path = Path(file_dir).joinpath(file_name)

    with open(file_path, "wb") as f:
        np.save(f, file)

### LMDB ###

def load_lmdb(
    folder_path: str
    ) -> lmdb.Transaction:
    """
    Load LMDB folder, which is usually in the following structure:

        lmdb-folder/
        ├── data.mdb
        └── lock.mdb

    Input:
    - folder_path : path to the LMDB folder

    Output:
    - txn : lmdb.Transaction object which allows fetching value by key
    """
    # check, o.w. lmdb still load even if folder not exists
    if not Path(folder_path).is_dir():
        raise FileNotFoundError(f"... path {folder_path} is not folder!")

    # LMDB path must be string, not Path
    # TODO: update map_size
    env_db = lmdb.open(
        str(folder_path), 
        readonly=True,
        map_size=int(1e12),
        readahead=False,
        lock=False
        )
    
    txn = env_db.begin()

    return txn

def save_lmdb(
    d: Dict[str, Union[np.ndarray, List[int]]],
    save_dir: str,
    recompute: bool
    ):
    """
    Convert Python dict with NP.ARRAY OR LIST OF INTEGERS value to lmdb in save_dir

    Input:
    - d : Python dict
    - save_dir : folder to save lmdb to
    - recompute : whether to overwrite lmdb or update
    """
    print(f"----- converting dict to LMDB in {Path(save_dir).name} {datetime.datetime.now()}")

    print(f"----- saving {len(d)} entries (= number of rows for np.array or list of integers)")

    if recompute and Path(save_dir).is_dir():
        shutil.rmtree(save_dir)

    Path(save_dir).parent.mkdir(
        parents=True,
        exist_ok=True
        )

    # TODO: update map_size
    env = lmdb.open(
        str(save_dir), 
        map_size=int(1e12),
        readahead=False
        )

    with env.begin(write=True) as txn:

        for key, value in tqdm(d.items()):

            try:
                # need .encode() 
                # o.w. TypeError: Won't implicitly convert Unicode to bytes
                byte_key = key.encode()
                byte_value = bytes(value)

                txn.put(byte_key, byte_value)

            except Exception as e:
                print(f"Exception at KEY {key}: {e}")
                pass

    env.close()

    print("... done")

def save_lmdb_text_val(
    d: Dict[str, Union[np.ndarray, List[int]]],
    save_dir: str,
    recompute: bool
    ):
    """
    Convert Python dict with TEXT value to lmdb in save_dir

    Input:
    - d : Python dict
    - save_dir : folder to save lmdb to
    - recompute : whether to overwrite lmdb or update
    """
    print(f"----- converting dict to LMDB in {Path(save_dir).name} {datetime.datetime.now()}")

    print(f"----- saving {len(d)} entries (= number of rows for np.array or list of integers)")

    if recompute and Path(save_dir).is_dir():
        shutil.rmtree(save_dir)

    Path(save_dir).parent.mkdir(
        parents=True,
        exist_ok=True
        )

    # TODO: update map_size
    env = lmdb.open(
        str(save_dir), 
        map_size=int(1e12),
        readahead=False
        )

    with env.begin(write=True) as txn:

        for key, value in tqdm(d.items()):

            try:
                # need .encode() 
                # o.w. TypeError: Won't implicitly convert Unicode to bytes
                byte_key = key.encode()
                byte_value = value.encode()

                txn.put(byte_key, byte_value)

            except Exception as e:
                print(f"Exception at KEY {key}: {e}")
                pass

    env.close()

    print("... done")

def get_npy_value_lmdb(
    txn: lmdb.Transaction, 
    key: str,
    dtype: str
    ) -> np.ndarray: 
    """
    Get NPY value for the specified key from LMDB

    Input:
    - txn : txn.Transaction object of embeddings dict
    - key: interested token
    """
    test_npy_byte = txn.get(str(key).encode(), None)

    if test_npy_byte is not None:

        test_npy = np.frombuffer(
            test_npy_byte,
            dtype=dtype
            )

        return test_npy

    else:
        raise KeyError

def get_text_value_lmdb(
    txn: lmdb.Transaction, 
    key: str,
    dtype: str
    ) -> np.ndarray: 
    """
    Get TEXT value for the specified key from LMDB

    Input:
    - txn : txn.Transaction object of embeddings dict
    - key: interested token
    """
    test_npy_byte = txn.get(str(key).encode(), None)

    if test_npy_byte is not None:

        test_npy = test_npy_byte.decode()

        return test_npy

    else:
        raise KeyError


### SQLITEDICT ###

def load_sqlitedict(
    file_path: str
    ) -> SqliteDict:
    """
    Load SQLiteDict from .sqlite file

    Input:
    - file_path : path to the SQLITE file

    Output:
    - db : sqlite.SqliteDict object which allows fetching value by key
    """
    # check, o.w. lmdb still load even if folder not exists
    if not Path(file_path).is_file():
        raise FileNotFoundError(f"... path {file_path} is not file!")

    db = SqliteDict(
        file_path, 
        outer_stack=False
        )

    # to close: db.close()

    return db

def save_sqlitedict(
    d: Dict[str, Union[np.ndarray, List[int]]],
    file_name: str,
    file_dir: str,
    recompute: bool
    ):
    """
    Convert Python dict with np.array or list of integer value to sqlitedict in save_dir

    Input:
    - d : Python dict
    - file_name : saved file name
    - file_dir : saved directory name
    - recompute : whether to overwrite sqlitedict or update
    """
    print(f"----- converting dict to SQLITEDICT in {Path(file_dir).name}/{file_name} {datetime.datetime.now()}")

    print(f"----- saving {len(d)} entries (= number of rows for np.array or list of integers)")

    file_path = Path(file_dir).joinpath(file_name)

    if recompute:
        Path(file_dir).joinpath(file_name).unlink(missing_ok=True)

    # create file_dir if not exists
    Path(file_dir).mkdir(
        parents=True, 
        exist_ok=True
        )

    with SqliteDict(Path(file_dir).joinpath(file_name), outer_stack=False, autocommit=True) as db:
        for key, value in tqdm(d.items()):
            try:
                db[key] = value
            except Exception as e:
                print(f"Exception at KEY {key}: {e}")
                pass

    print("... done")

### MISC ###

def list2chunks(
    l: list, 
    chunk_size: int
    ):
    """
    Stream-load list's slice in chosen chunk size

    Input:
    - l : the list
    - chunk_size : chunk size for generator

    Output:
    - *yield* list's slice of the given chunk size iteratively
    """
    for i in range(0, len(l), chunk_size):
        yield l[i: i+chunk_size]


def arg_dict_to_cmd_list(
    d: dict
    ) -> List[str]:
    """
    Convert a dict of Python arguments to command use-able on terminal

    Input:
    - d : argument dict

    Output:
    - cmd_string : string of command use-able on terminal
    """
    cmd_string = []
    for k, v in d.items():
        if type(v) in [float, int, str]:
            cmd_string += [f"--{k}"] + [str(v)]
        if type(v) == list:
            cmd_string += [f"--{k}"] + [str(sub_v) for sub_v in v]
        if type(v) == bool:
            cmd_string += [f"--no-{k}"]
    return cmd_string