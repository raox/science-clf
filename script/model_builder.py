import numpy as np
import os
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from typing import Dict, List

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.initializers import Constant
from tensorflow.keras.layers import Dense, Dropout, Embedding, Flatten, Input
from tensorflow.keras.layers import Conv1D, GlobalMaxPool1D, MaxPool1D 
from tensorflow.keras.layers import GRU
from tensorflow.keras.metrics import CategoricalAccuracy, Precision, Recall
from tensorflow_addons.metrics import F1Score 
from tensorflow.keras.optimizers import RMSprop

import var_setup

np.random.seed(var_setup.random_state)
tf.keras.utils.set_random_seed(var_setup.random_state)
tf.random.set_seed(var_setup.random_state)
tf.config.experimental.enable_op_determinism()

print(f"MODEL BUILDER")

tf.debugging.set_log_device_placement(False)
gpu_list = tf.config.list_logical_devices("GPU")

print(f"... {len(gpu_list)} visible 'logical' GPUs: {gpu_list}")

print(f"... setting up MirroredStrategy")

strategy =  tf.distribute.MirroredStrategy(gpu_list)

print(f"... {strategy.num_replicas_in_sync} visible GPUs in MirroredStrategy")

def get_f1_macro(y_true, y_pred):
    return f1_score(y_true, y_pred, average="macro")

def get_f1_micro(y_true, y_pred):
    return f1_score(y_true, y_pred, average="micro")

def get_recall(y_true, y_pred):
    return recall_score(y_true, y_pred)

def get_precision(y_true, y_pred):
    return precision_score(y_true, y_pred)

def acc_threshold(y_true, y_pred, threshold: float = 0.30): 
    y_pred_threshold_adjusted = (y_pred > threshold).numpy().astype("int32")
    y_true_np = y_true.numpy()
    return accuracy_score(y_true_np, y_pred_threshold_adjusted)

def build_model(
    model_type: str,

    emb_matrix: np.array,

    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop = RMSprop(learning_rate=0.001),

    **kwargs
    ) -> Model:
    """
    Modularize Build Neural Network (NN) model with Distributed Learning

    Input:
    - model_type : model type, e.g. "test" or "dnn"
    - emb_matrix : 

    - max_token_id_seq_len : maximum sequence length of text vectors, which looks like:

        max = 5

        "I am" --> "2 3 0 0 0" (pad to get length 5)
        "I am but I am not" --> "2 3 4 2 3" (drop "not" to get length 5)

    - emb_trainable : whether to train the embeddings or freeze as is
    - num_class : number of output classes (here: categories)
    - optimizer : loss optimizer

    Output:
    - model : NN model built from configuration
    """
    print("BUILDING MODEL")

    model_param_dict = locals().copy()

    # fetch model function
    model_fn = model_fn_dict[model_type]

    model_param_dict.pop("model_type")

    fetched_model = model_fn(**model_param_dict)

    try:
        print(f"... model summary: {fetched_model.summary()}")


    except AttributeError:
        pass

    """
    print(f"... number of total weights: {len(fetched_model.weights)}")
    print(f"... number of trainable weights: {len(fetched_model.trainable_weights)}")
    print(f"... number of non-trainable weights: {len(fetched_model.non_trainable_weights)}")    
    """
    
    with strategy.scope():
        print("... to train using multiple GPUs")
        return fetched_model

def test_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model consisting of only Embedding layer
    """
    vocab_size, emb_dim = emb_matrix.shape

    input_vectorized_seq = Input(
        shape=(None,), 
        dtype="int64"
        )

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    """
    embedded_seq = emb_layer(input_vectorized_seq)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[embedded_seq]
        )

    model.compile(
        loss="categorical_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", Precision(), Recall()] 
        )

    return model
    """

    return emb_layer

def dnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "DNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    input_vectorized_seq = Input(
        shape=(None,), 
        dtype="int64"
        )

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)

    ### different from test ###

    num_layer = 8
    num_node = 100
    dropout = 0.5

    x = GlobalMaxPool1D()(embedded_seq)
    x = Dropout(rate=dropout)(x)

    for i in range(num_layer):
        x = Dense(
                units=num_node, 
                activation="relu"
                )(x)
        x = Dropout(rate=dropout)(x)

    y = Dense(
            num_class, 
            activation="softmax"
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="categorical_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")] 
        )

    return model

def cnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "CNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    ### different from test ###

    input_vectorized_seq = Input(
        shape=(max_token_id_seq_len, ), 
        dtype="int64"
        )

    ### end ###

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)
    
    ### different from test ###

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(embedded_seq)

    print(x.shape)

    x = MaxPool1D(pool_size=3)(x)

    print(x.shape)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=3
        )(x)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=2
        )(x)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=2
        )(x)
    
    x = Flatten()(x)

    x = Dense(
            units=30, 
            activation="relu"
            )(x)

    y = Dense(
            num_class, 
            activation="softmax"
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="categorical_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")] 
        )

    return model

def rnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "RNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    input_vectorized_seq = Input(
        shape=(None,), 
        dtype="int64"
        )

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)

    ### different from test ###

    x = GRU(
            units=30, 
            dropout=0.2, 
            recurrent_dropout=0.0, # change from 0.2 to use cudnn
            return_sequences=True
            )(embedded_seq)

    x =GRU(
            units=30, 
            dropout=0.2, 
            recurrent_dropout=0.0 # change from 0.2 to use cudnn
            )(x)

    y = Dense(
            num_class, 
            activation="softmax"
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="categorical_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")] 
        )

    return model

def multilabel_dnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "DNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    input_vectorized_seq = Input(
        shape=(None,), 
        dtype="int64"
        )

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)

    ### different from test ###

    num_layer = 8
    num_node = 100
    dropout = 0.5

    x = GlobalMaxPool1D()(embedded_seq)
    x = Dropout(rate=dropout)(x)

    for i in range(num_layer):
        x = Dense(
                units=num_node, 
                activation="relu"
                )(x)
        x = Dropout(rate=dropout)(x)

    y = Dense(
            num_class, 
            activation="sigmoid" # CHANGE
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="binary_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", acc_threshold, Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")],
        run_eagerly=True 
        )

    return model

def multilabel_cnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "CNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    ### different from test ###

    input_vectorized_seq = Input(
        shape=(max_token_id_seq_len, ), 
        dtype="int64"
        )

    ### end ###

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)
    
    ### different from test ###

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(embedded_seq)

    print(x.shape)

    x = MaxPool1D(pool_size=3)(x)

    print(x.shape)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=3
        )(x)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=2
        )(x)

    x = Conv1D(
            filters=128, 
            kernel_size=5, 
            activation="relu"
            )(x)

    x = MaxPool1D(
        pool_size=2
        )(x)
    
    x = Flatten()(x)

    x = Dense(
            units=30, 
            activation="relu"
            )(x)

    y = Dense(
            num_class, 
            activation="sigmoid" # CHANGE
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="binary_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", acc_threshold, Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")],
        run_eagerly=True
        )

    return model

def multilabel_rnn_model_setup(
    emb_matrix: np.array,
    max_token_id_seq_len: int,
    emb_trainable: bool,
    num_class: int,
    optimizer: RMSprop,
    **kwargs
    ) -> Model:
    """
    Build test Neural Network model "RNN"
    """
    vocab_size, emb_dim = emb_matrix.shape

    input_vectorized_seq = Input(
        shape=(None,), 
        dtype="int64"
        )

    emb_layer = Embedding(
        input_dim=vocab_size,
        output_dim=emb_dim,
        embeddings_initializer=Constant(emb_matrix),
        mask_zero=True,
        input_length=max_token_id_seq_len,
        trainable=emb_trainable
        )

    embedded_seq = emb_layer(input_vectorized_seq)

    ### different from test ###

    x = GRU(
            units=30, 
            dropout=0.2, 
            recurrent_dropout=0.0, # change from 0.2 to use cudnn
            return_sequences=True
            )(embedded_seq)

    x =GRU(
            units=30, 
            dropout=0.2, 
            recurrent_dropout=0.0 # change from 0.2 to use cudnn
            )(x)

    y = Dense(
            num_class, 
            activation="sigmoid" # CHANGE
            )(x)

    model = Model(
        inputs=[input_vectorized_seq],
        outputs=[y]
        )

    ### end ###

    model.compile(
        loss="binary_crossentropy", # output in proba array
        optimizer=optimizer,
        metrics=["categorical_accuracy", acc_threshold, Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")], 
        run_eagerly=True
        )

    return model

# dict for model list, so that build_model can be modularized
# can't be put on top of file! otherwise, undefined error
model_fn_dict = {
    "test": test_model_setup,
    "dnn": dnn_model_setup,
    "cnn": cnn_model_setup,
    "rnn": rnn_model_setup,
    "multilabel_dnn": multilabel_dnn_model_setup,
    "multilabel_cnn": multilabel_cnn_model_setup,
    "multilabel_rnn": multilabel_rnn_model_setup
    }