from fastapi import APIRouter, HTTPException
from pydantic import BaseModel, Field

import json
import mlflow
import numpy as np
import os
from pathlib import Path
import pickle
from typing import List

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

from tensorflow.keras.layers import TextVectorization
from tensorflow.keras.metrics import CategoricalAccuracy, Precision, Recall
from tensorflow_addons.metrics import F1Score 

shared_wos_dir = "/home/pleum/Documents/shared-wos" #TOCHANGE
output_dir = "/home/pleum/Documents/thesis-POST-THESIS/all-output-POST-THESIS/mlruns/" #TOCHANGE

num_level = 2 #TOCHANGE

# MAKE DICT TO KEEP CACHED VECTORIZER

cached_vectorizer_dict = dict()

# MAKE DICT TO KEEP CACHED VOCAB LIST

cached_vocab_list_dict = dict()

for vocab_list_path in Path(shared_wos_dir).joinpath("vocab_list").glob("*_vocab_list.txt"):
    with open(vocab_list_path, "r") as f:
        vocab_list = [line.strip() for line in f.readlines() if line.endswith("\n") and line.strip() != ""]

    vocab_type = vocab_list_path.stem.replace("_vocab_list", "")

    print(f"caching vocab list {vocab_type}")

    cached_vocab_list_dict[vocab_type] = vocab_list

# MAKE DICT TO KEEP CACHED MODELS

cached_model_dict = dict()

# MAKE DICT TO KEEP CACHED BINARIZERS

cached_mlb_dict = dict()

# MAKE CODE-TO-CLASS MAP

try:

	with open(Path(output_dir).parent.joinpath("model_map.json"), "r") as f: #TOCHANGE
	    model_to_experiment_dict = json.load(f)
	    
except FileNotFoundError:
	print("missing model_map.json file")

def make_model_inference(
    request
    ):
    experiment = model_name_to_experiment(
        model_name=request.model_name
        )
    model_dir = str(Path(output_dir).joinpath(experiment))

    # load model set-up
    with open(Path(model_dir).joinpath("artifacts", "model_set_up", "model_set_up.json"), "rb") as f:
        model_set_up_dict = json.load(f)

    vocab_type = model_set_up_dict["vocab_type"]
    output_seq_len = model_set_up_dict["max_token_id_seq_len"]

    if experiment not in cached_model_dict:
        print(f"model {experiment} not cached, caching now")
        model = mlflow.keras.load_model(
            str(Path(model_dir).joinpath("artifacts", "model")),
            custom_objects={}
            )
        print(model.summary())

        if vocab_type not in cached_vocab_list_dict:

            with open(Path(shared_wos_dir).joinpath("vocab_list", f"{vocab_type}_vocab_list.txt"), "r") as f:
                vocab_list = [line.strip() for line in f.readlines() if line.endswith("\n") and line.strip() != ""]

            cached_vocab_list_dict[vocab_type] = vocab_list

        else:

            vocab_list = cached_vocab_list_dict[vocab_type]

        vectorizer = TextVectorization(
            output_sequence_length=output_seq_len
            )

        vectorizer.set_vocabulary(vocab_list)

        cached_vectorizer_dict[experiment] = vectorizer
        cached_model_dict[experiment] = model

        # load model set-up
        with open(Path(model_dir).joinpath("artifacts", "model_set_up", "temp_binarizer.pickle"), "rb") as f:
            mlb = pickle.load(f)
            cached_mlb_dict[experiment] = mlb
    else:
        print(f"loading model {experiment} from cache")
        model = cached_model_dict[experiment]
        vectorizer = cached_vectorizer_dict[experiment]
        mlb = cached_mlb_dict[experiment]

    arr = vectorizer(request.text).numpy()

    text_vector = arr if arr.shape[0] > 0 else np.zeros(shape=(output_seq_len,), dtype="int64")

    print(list(text_vector))

    output_proba = model.predict(np.array([text_vector], dtype="int64"))[0]

    print(model.predict(np.array([text_vector])))

    print(output_proba)

    output_class_code_post_mlb = np.zeros(shape=output_proba.shape) 

    output_class_code_post_mlb[np.argmax(output_proba)] = 1 #NOTE change if change threshold for +-1 in multi-label

    output_class_code = list(mlb.inverse_transform(np.array([output_class_code_post_mlb]))[0])

    return output_proba, output_class_code

def model_name_to_experiment(
    model_name: str,
    model_type: str = "rnn"
    ):
    return model_to_experiment_dict[model_name]
