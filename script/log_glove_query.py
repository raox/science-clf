"""
Embedding Benchmark: Text vs LMDB vs SQLite

Command:
python3 script/log_glove_query.py --shared_dir /home/pleum/Documents/shared-wos/ --output_dir /home/pleum/Documents/glove_resource_log/ --emb_type glove --emb_token_size 6 --emb_dim 50 --load_from txt
"""


from argparse import ArgumentParser, BooleanOptionalAction
import datetime
import os
from pathlib import Path
import random
import statistics
import time
from tqdm import tqdm

import data_helper
import data_loader
import resource_tracker
import var_setup

random.seed(var_setup.random_state)

print("BENCHMARKING EMBEDDING QUERY TIME")

pid = os.getpid()

print(f"- Process ID: {pid}")

parser = ArgumentParser()

parser.add_argument("--shared_dir", type=str, help="directory of shared resource with embedding, vocab_list, etc., e.g. '/data/shared-wos/' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
parser.add_argument("--emb_type", type=str, help="embedding type, e.g. 'glove' for glove.6B.50 ")
parser.add_argument("--emb_token_size", type=int, default=None, help="embedding token size, e.g. '6' for glove.6B.50 ")
parser.add_argument("--emb_dim", type=int, default=None, help="embedding dimension e.g. '50' for glove.6B.50 ")
parser.add_argument("--load_from", type=str, help="whether to load from 'txt' or 'lmdb' or 'sqlite' ")
parser.add_argument("--num_query", type=int, default=400000, help="number of query to make for benchmarking, default = 400000 ~ 4 * # glove vocabs")

parser.add_argument("--recompute_emb", action=BooleanOptionalAction, default=False, help="whether recompute embedding components, default = False")

options = parser.parse_args()

print(f"... options: {vars(options)}")

log_file_name = f"{options.emb_type}.{options.emb_token_size}B.{options.emb_dim}d_{options.load_from}"
    
# initialize output dir
    
Path(options.output_dir).mkdir(
    mode=0o755,
    exist_ok=True, 
    parents=True
    )

# initialize resource loggers

overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )

cpu_ram_p = resource_tracker.log_cpu_ram(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )


# load vocab list

vocab_list = data_loader.load_vocab_list(
    load_dir=Path(options.shared_dir).joinpath("vocab_list"),
    vocab_type="glove.6B"
    )

## sample (w/ replacement)

random_query_list = random.choices(vocab_list, k=options.num_query)

# load emb

emb_dict = data_loader.load_word_embedding(
    load_dir=Path(options.shared_dir).joinpath("word_embeddings"),
    emb_type=options.emb_type,
    token_size=options.emb_token_size,
    emb_dim=options.emb_dim,
    recompute=options.recompute_emb,
    load_from=options.load_from
    )

time_list = []

# query emb

if options.load_from == "lmdb":
    for q in tqdm(random_query_list):

        try:
            start_time = datetime.datetime.now()

            test_npy = data_helper.get_npy_value_lmdb(
                txn=emb_dict,
                key=q,
                dtype="float64"
                )

            end_time = datetime.datetime.now()

            #print(f"start time: {start_time}")
            #print(f"end time: {end_time}")

            time_list.append((end_time - start_time).total_seconds())
        except KeyError:
            print(f"Error at query {q}")
            pass

elif options.load_from in ["txt", "sqlite"]:
    for q in tqdm(random_query_list):

        try:
            start_time = datetime.datetime.now()

            test_npy = emb_dict[q]

            end_time = datetime.datetime.now()

            #print(f"start time: {start_time}")
            #print(f"end time: {end_time}")

            time_list.append((end_time - start_time).total_seconds())

        except KeyError:
            print(f"Error at query {q}")
            pass


# end resource loggers
time.sleep(1)

overall_gpu_p.terminate()
proc_gpu_p.terminate()
cpu_ram_p.terminate()


# log static resource (query time)

log_path = Path(options.output_dir).joinpath(f"{log_file_name}_static_log.json")

if log_path.is_file():
    log_dict = data_helper.load_json(
        file_path=log_path
        )
else:
    log_dict = dict()

## query time: avg, 99-percentile, full list

log_dict["query-time-seconds-average"] = statistics.fmean(time_list) # better numerical stability

log_dict["query-time-seconds-99-percentile"] = statistics.quantiles(
    time_list, 
    n=100, 
    method="exclusive"
    )[-1] # index -2 if method="inclusive"

log_dict["query-time-seconds"] = time_list

# save static resource

data_helper.save_json(
    file=log_dict,
    file_name=log_path.name,
    file_dir=log_path.parent
    )