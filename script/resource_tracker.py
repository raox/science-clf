from pathlib import Path
import subprocess

def log_gpu(
    pid: str,
    model_name: str,
    save_dir: str
    ):
    with open(Path(save_dir).joinpath(f"{model_name}_gpu_overall_log.csv"), "w") as f: 
        overall_gpu_p = subprocess.Popen(
            ["nvidia-smi", "-l", "1", "--query-gpu=timestamp,name,pci.bus_id,driver_version,pstate,pcie.link.gen.max,pcie.link.gen.current,temperature.gpu,utilization.gpu,utilization.memory,memory.total,memory.free,memory.used", "--format=csv"],
            stdout=f
            )
    
    # make separate script to avoid shell=True
    save_path = Path(save_dir).joinpath(f"{model_name}_gpu_process_log.csv")

    proc_gpu_p = subprocess.Popen(
            [str(Path(__file__).parent.joinpath("proc_gpu_tracker.sh")), str(pid), str(save_path)]
            )

    return overall_gpu_p, proc_gpu_p

def log_cpu_ram(
    pid: str,
    model_name: str,
    save_dir: str
    ):
    # make separate script due to quotes (", ') parsing difficulty
    save_path = Path(save_dir).joinpath(f"{model_name}_cpu_ram_log.txt")

    cpu_ram_p = subprocess.Popen(
        [str(Path(__file__).parent.joinpath("cpu_ram_tracker.sh")), str(pid), str(save_path)]
        )

    return cpu_ram_p