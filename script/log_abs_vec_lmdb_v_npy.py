"""
Embedding Benchmark: LMDB vs NPY for abstract vector

Command:
python3 script/log_abs_vec_lmdb_v_npy.py --model_dir /home/pleum/Documents/test-model/ --output_dir /home/pleum/Documents/glove_resource_log/ --load_from lmdb

"""

from argparse import ArgumentParser
import datetime
import numpy as np
import os
from pathlib import Path
import psutil
from psutil._common import bytes2human
import random
import statistics
import time
from tqdm import tqdm

import data_helper
import data_loader
import resource_tracker
import var_setup

np.random.seed(var_setup.random_state)
random.seed(var_setup.random_state)

print("BENCHMARKING ABSTRACT VECTOR QUERY TIME")

pid = os.getpid()

print(f"- Process ID: {pid}")

parser = ArgumentParser()

parser.add_argument("--model_dir", type=str, help="directory of model resource with train/test split, etc., e.g. '/data/model/' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
parser.add_argument("--load_from", type=str, help="whether to load from 'lmdb' or 'npy' ")
parser.add_argument("--num_query", type=int, default=400000, help="number of query to make for benchmarking, default = 400000 ~ 4 * # glove vocabs")


options = parser.parse_args()

print(f"... options: {vars(options)}")

log_file_name = f"{options.load_from}_{options.num_query}q_ACM_infk_abstract_vec"
    
# initialize output dir
    
Path(options.output_dir).mkdir(
    mode=0o755,
    exist_ok=True, 
    parents=True
    )

# initialize resource loggers

overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )

cpu_ram_p = resource_tracker.log_cpu_ram(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )


# load id

id_list = [file.stem for file in Path(options.model_dir).joinpath("ACM_infk_abstract_vec_npy").iterdir()]

## sample (w/ replacement)

random_query_list = random.choices(id_list, k=options.num_query)

# initialize log

log_path = Path(options.output_dir).joinpath(f"{log_file_name}_static_log.json")

if log_path.is_file():
    log_dict = data_helper.load_json(
        file_path=log_path
        )
else:
    log_dict = dict()

time_list = []

# query emb

if options.load_from == "lmdb":
    abstract_vec_dict = data_helper.load_lmdb(
        folder_path=Path(options.model_dir).joinpath("ACM_infk_abstract_vec_lmdb")
        )

    # log ram

    proc = psutil.Process(pid)

    ram_usage = bytes2human(proc.memory_info().rss)

    log_dict["ram-usage-before-query"] = ram_usage

    for q in tqdm(random_query_list):

        try:
            start_time = datetime.datetime.now()

            test_npy = data_helper.get_npy_value_lmdb(
                txn=abstract_vec_dict,
                key=q,
                dtype="int64"
                )

            end_time = datetime.datetime.now()

            #print(f"start time: {start_time}")
            #print(f"end time: {end_time}")

            time_list.append((end_time - start_time).total_seconds())

        except KeyError:
            print(f"Error at query {q}")
            pass


elif options.load_from == "npy":

    # log ram

    proc = psutil.Process(pid)

    ram_usage = bytes2human(proc.memory_info().rss)

    log_dict["ram-usage-before-query"] = ram_usage

    for q in tqdm(random_query_list):

        try:
            start_time = datetime.datetime.now()

            test_npy = data_helper.load_npy(
                Path(options.model_dir).joinpath("ACM_infk_abstract_vec_npy", f"{q}.npy")
                )

            end_time = datetime.datetime.now()

            #print(f"start time: {start_time}")
            #print(f"end time: {end_time}")

            time_list.append((end_time - start_time).total_seconds())
            
        except FileNotFoundError:
            print(f"Error at query {q}")
            pass



# end resource loggers
time.sleep(1)

overall_gpu_p.terminate()
proc_gpu_p.terminate()
cpu_ram_p.terminate()


# log static resource (query time)

## query time: avg, 99-percentile, full list

log_dict["query-time-seconds-average"] = statistics.fmean(time_list) # better numerical stability

log_dict["query-time-seconds-99-percentile"] = statistics.quantiles(
    time_list, 
    n=100, 
    method="exclusive"
    )[-1] # index -2 if method="inclusive"

log_dict["query-time-seconds"] = time_list

# save static resource

data_helper.save_json(
    file=log_dict,
    file_name=log_path.name,
    file_dir=log_path.parent
    )