import pandas as pd
import gc

df_sample_list = list()

for stem in ["ACM_infk", "JEL_econ", "MSC_math"]:
	df = pd.read_parquet(f"/data/ppiriyata/all-keyword-wos/{stem}_PaperFieldsOfStudy_top3000_single.parquet")
	df_sample = df.sample(
		n=10000, 
		random_state=12
		)
	df_sample_list.append(df_sample)

	del df, df_sample
	gc.collect()

df_all_sample = pd.concat(
	df_sample_list, 
	ignore_index=True
	)

df_all_sample.to_parquet(
	"/data/ppiriyata/keyword-analysis/ACM_JEL_MSC_sample10000ea_PaperFieldsOfStudy_top3000_single.parquet", 
	engine="pyarrow",
	index=False
	)

breakpoint()