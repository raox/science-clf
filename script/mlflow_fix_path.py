from pathlib import Path
import yaml

print("FIXING MLRUNS PATH")

for meta_file in Path("mlruns").rglob("meta.yaml"):
	print(f"... processing {meta_file}")
	with open(meta_file) as f:
		list_doc = yaml.load(f, Loader=yaml.FullLoader)

	new_val = meta_file.parent.resolve().as_uri()

	if "artifact_uri" in list_doc.keys():
		list_doc["artifact_uri"] = new_val
	elif "artifact_location" in list_doc.keys():
		list_doc["artifact_location"] = new_val
	else:
		pass

	with open(meta_file, "w") as f:
		yaml.dump(list_doc, f)
