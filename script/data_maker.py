import datetime
import numpy as np
import pandas as pd
from pathlib import Path
from sqlitedict import SqliteDict
import subprocess
from typing import Dict, List, Tuple, Union

import data_helper, var_setup

np.random.seed(var_setup.random_state)

journal_to_discipline_dict = var_setup.journal_to_discipline_dict
random_state = var_setup.random_state

### MODEL DATA ###

def make_model_data(
    label_type: str,
    load_dir: str,
    save_dir: str,
    journal_list: List[str],
    num_level: int,
    train_ratio: float = 0.60,
    random_state: int = random_state
    ):
    """
     Make model data, including
    - label_type: whether single or multi
    - model list (num_lvl 2 : per journal a.k.a. "cat0")
    - train/test partition, list of classes (per model)
    - label (num_lvl 2 : per journal a.k.a. "cat0" + level)

    Input:
    - load_dir : directory for loading model input data (e.g. wos/)
    - save_dir : directory for saving model data (e.g. model/)
    - journal_list : list of journals to compute, e.g. "ACM JEL"
    - num_level : depth level of model (1: infk/math/..., 3: ours)
    - train_ratio : proportion of input data to be in training (instead of validation)
    - random_state : control parameter for split
    """

    print(f"... making {label_type.upper()} model data")

    make_model_fn = make_model_data_dict[label_type]

    make_model_fn(
        load_dir=load_dir,
        save_dir=save_dir,
        journal_list=journal_list,
        num_level=num_level,
        train_ratio=train_ratio,
        random_state=random_state
        )

def singlelabel_make_model_data(
    load_dir: str,
    save_dir: str,
    journal_list: List[str],
    num_level: int,
    train_ratio: float = 0.60,
    random_state: int = random_state
    ):
    """
    Make model data for SINGLE label (See make_model_data)
    """
    print(f"MAKING DATA FOR ML DL MODEL {datetime.datetime.now()}")

    # make save directory if not already existed
    Path(save_dir).mkdir(
        mode=0o755,
        parents=True, 
        exist_ok=True
        )

    model_list = []

    # sub-slice journal_to_discipline dict only for desired journal list
    sub_journal_to_discipline_dict = {journal: journal_to_discipline_dict[journal] for journal in journal_list}

    for cat0_label, (journal, discipline) in enumerate(sub_journal_to_discipline_dict.items()):
        print(f"... processing {journal}, {discipline} {datetime.datetime.now()}")

        load_file_path = Path(load_dir).joinpath(f"{journal}_{discipline}_processed.parquet")

        # cat0 <=> discipline ("infk", "math"), etc.
        cat_col_list = [f"cat{lvl}_coding" for lvl in range(num_level)]

        print(f"... loading categorical columns {cat_col_list}")

        df = pd.read_parquet(
            load_file_path, 
            engine="pyarrow",
            columns=(["PaperId", "abstract"] + cat_col_list)
            )

        #breakpoint()

        # for col in cat_col_list:
        #     if type(df[col][0]) == np.ndarray:
        #         df[col] = df[col].apply(lambda x: tuple(x.tolist()))

        #breakpoint()

        print(f"..... total {df.shape[0]} entries")

        print(f"... adding column 'output_id' for {journal}, {discipline} {datetime.datetime.now()}")
        # make column "output_id" : "id-{infk/econ/math}-xx"
        df["output_id"] = "id" + "-" + discipline + "-" + df.index.astype(str)

        # do stratified train/test split per each model

        print(f"... add (dummy) column 'split_type' for {journal}, {discipline} {datetime.datetime.now()}")
        # make column "split_type" (dummy, to be updated later)
        df["split_type"] = "val"

        print(f"... stratified sampling from each (cat0, cat1, ...)")
        sample_df_by_cat = df.groupby(cat_col_list).sample(frac=train_ratio, random_state=random_state)

        print(f"... done sampling {int(train_ratio * 100)}%, getting {sample_df_by_cat.shape[0]} train entries")

        # record back the "training" assignment
        df.loc[list(sample_df_by_cat.index),"split_type"] = "train"

        #breakpoint()

        # for col in cat_col_list:
        #     if type(df[col][0]) == tuple:
        #         df[col] = df[col].apply(list)

        #breakpoint()

        print(f"... saving back parquet after modification")
        save_file_path = Path(save_dir).joinpath(f"{journal}_{discipline}_processed_model_ready.parquet")
        df.to_parquet(
            save_file_path, 
            engine="pyarrow",
            index=False
            )

        # for col in cat_col_list:
        #     df = df.explode(col, ignore_index=True)

        for lvl in range(1, num_level):
            lvl_cat_col_list = cat_col_list[:lvl]
            lvl_cat_grouped_df = df.groupby(lvl_cat_col_list)

            print(f"... retrieving model list for {lvl_cat_col_list}")
            # return named-tuple : [Pandas(cat0_coding=0, cat1_coding=2), ...]
            lvl_model_list = df[lvl_cat_col_list].drop_duplicates().itertuples(index=False)

            for model in lvl_model_list:
                print(f"... processing data for model {model} {datetime.datetime.now()}")

                group_id = tuple(model) if len(model) > 1 else model[0]

                # get part of df that belongs to this model
                sub_df = lvl_cat_grouped_df.get_group(group_id)

                # map = fastest way, ref. StackExchange
                model_name = "model" + "-" + "-".join(map(str, model))

                # save train/test split
                for split_type in ["train", "val"]:
                    print(f"... saving {split_type} sample for model {model} {datetime.datetime.now()}")
                    
                    split_type_id_list = sorted(sub_df[sub_df["split_type"] == split_type]["output_id"].unique().tolist())

                    data_helper.save_json(split_type_id_list, f"{model_name}_id_{split_type}.json", save_dir)

                model_list += [model_name]

                # save list of classes
                print(f"... saving list of classes for model {model} {datetime.datetime.now()}")
                class_list = sorted(sub_df[cat_col_list[lvl]].unique().tolist())
                data_helper.save_json(class_list, f"{model_name}_class.json", save_dir)

                # save label dict
                print(f"... saving label dict for model {model} {datetime.datetime.now()}")
                """
                label_dict = dict(zip(sub_df["output_id"], sub_df[cat_col_list[lvl]]))
                data_helper.save_json(label_dict, f"{model_name}_label_dict_TEST.json", save_dir)
                """

                sub_df = sub_df.groupby("output_id")[cat_col_list[lvl]].apply(tuple).reset_index()

                label_dict = dict(zip(sub_df["output_id"], sub_df[cat_col_list[lvl]]))
                data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)

            print("done computing", len(model_list), model_list)

    singlelabel_update_level0_model_data(save_dir=save_dir)

def singlelabel_update_level0_model_data(
    save_dir: str
    ):
    """
    Update model data for top-level model, including
    - model list
    - model list
    - train/test partition
    - label

    Input:
    - save_dir: directory for saving model data (e.g. model/)
    """
    model_list = []

    model = "model"
    model_name = "model"
    model_list += [model_name]

    model_list += [str(file_path.name).split("_")[0] for file_path in Path(save_dir).glob(f"model-*_id_train.json")]

    print("done updating model list", len(model_list), model_list)

    print(f"... saving model list {datetime.datetime.now()}")
    data_helper.save_json(model_list, f"model_list.json", save_dir)

    df = pd.concat([pd.read_parquet(
        file_path, 
        engine="pyarrow",
        columns=["PaperId", "abstract", "cat0_coding", "output_id", "split_type"]
        ) for file_path in Path(save_dir).glob(f"*_processed_model_ready.parquet")]) 

    # cat_col_list = [col for col in df.columns if col.startswith("cat") and col.endswith("_coding")]

    # for col in cat_col_list:
    #         df[col] = df[col].apply(lambda x: tuple(x.tolist()))
    #         df[col] = df[col].apply(list)
    #         df = df.explode(col, ignore_index=True)

    # save train/test split
    for split_type in ["train", "val"]:
        print(f"... saving {split_type} sample for model {model_name} {datetime.datetime.now()}")
        
        split_type_id_list = sorted(df[df["split_type"] == split_type]["output_id"].unique().tolist())
        data_helper.save_json(split_type_id_list, f"{model_name}_id_{split_type}.json", save_dir)

    # save list of classes
    print(f"... saving list of classes for model {model} {datetime.datetime.now()}")
    class_list = df["cat0_coding"].unique().tolist()
    data_helper.save_json(class_list, f"{model_name}_class.json", save_dir)

    # save label dict
    print(f"... saving label dict for model {model} {datetime.datetime.now()}")
    """
    label_dict = dict(zip(df["output_id"], df["cat0_coding"]))
    data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)
    """

    sub_df = df.groupby("output_id")["cat0_coding"].apply(tuple).reset_index()

    label_dict = dict(zip(sub_df["output_id"], sub_df["cat0_coding"]))
    data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)

def multilabel_make_model_data(
    load_dir: str,
    save_dir: str,
    journal_list: List[str],
    num_level: int,
    train_ratio: float = 0.60,
    random_state: int = random_state
    ):
    """
    Make model data for MULTI label (See make_model_data)
    """
    print(f"MAKING DATA FOR ML DL MODEL {datetime.datetime.now()}")

    # make save directory if not already existed
    Path(save_dir).mkdir(
        mode=0o755,
        parents=True, 
        exist_ok=True
        )

    model_list = []

    # sub-slice journal_to_discipline dict only for desired journal list
    sub_journal_to_discipline_dict = {journal: journal_to_discipline_dict[journal] for journal in journal_list}

    for cat0_label, (journal, discipline) in enumerate(sub_journal_to_discipline_dict.items()):
        print(f"... processing {journal}, {discipline} {datetime.datetime.now()}")

        load_file_path = Path(load_dir).joinpath(f"{journal}_{discipline}_processed.parquet")

        # cat0 <=> discipline ("infk", "math"), etc.
        # cat_col_list = [f"cat{lvl}_coding" for lvl in range(num_level)]

        # print(f"... loading categorical columns {cat_col_list}")

        df = pd.read_parquet(
            load_file_path, 
            engine="pyarrow",
            columns = ["PaperId", "abstract", "cat_coding"]# columns=(["PaperId", "abstract"] + cat_col_list)
            )

        # breakpoint()

        # for col in cat_col_list:
        #     if type(df[col][0]) == np.ndarray:
        #         df[col] = df[col].apply(lambda x: tuple(x.tolist()))

        if type(df["cat_coding"][0]) == np.ndarray:
            df["cat_coding"] = df["cat_coding"].apply(lambda x: tuple(x.tolist()))        

        # breakpoint()

        print(f"..... total {df.shape[0]} entries")

        print(f"... adding column 'output_id' for {journal}, {discipline} {datetime.datetime.now()}")
        # make column "output_id" : "id-{infk/econ/math}-xx"
        df["output_id"] = "id" + "-" + discipline + "-" + df.index.astype(str)

        # do stratified train/test split per each model

        print(f"... add (dummy) column 'split_type' for {journal}, {discipline} {datetime.datetime.now()}")
        # make column "split_type" (dummy, to be updated later)
        df["split_type"] = "val"

        print(f"... stratified sampling from each (cat0, cat1, ...)")
        # sample_df_by_cat = df.groupby(cat_col_list).sample(frac=train_ratio, random_state=random_state)
        sample_df_by_cat = df.groupby("cat_coding").sample(frac=train_ratio, random_state=random_state)

        print(f"... done sampling {int(train_ratio * 100)}%, getting {sample_df_by_cat.shape[0]} train entries")

        # record back the "training" assignment
        df.loc[list(sample_df_by_cat.index),"split_type"] = "train"

        # breakpoint()

        # for col in cat_col_list:
        #     if type(df[col][0]) == tuple:
        #         df[col] = df[col].apply(list)

        if type(df["cat_coding"][0]) == tuple:
            df["cat_coding"] = df["cat_coding"].apply(list)   

        # breakpoint()

        print(f"... saving back parquet after modification")
        save_file_path = Path(save_dir).joinpath(f"{journal}_{discipline}_processed_model_ready.parquet")
        df.to_parquet(
            save_file_path, 
            engine="pyarrow",
            index=False
            )

        print("... exploding column with list into different rows")
        # for col in cat_col_list:
        #     df = df.explode(col, ignore_index=True)

        df = df.explode("cat_coding", ignore_index=True)

        cat_col_list = [f"cat{lvl}_coding" for lvl in range(num_level)]

        print("... splitting each cat_coding e.g. 0-0-0 into separate columns")
        # added
        df[cat_col_list] = df["cat_coding"].str.split("-", expand=True)
        df[cat_col_list] = df[cat_col_list].astype(int)

        # breakpoint()

        for lvl in range(1, num_level):
            lvl_cat_col_list = cat_col_list[:lvl]
            lvl_cat_grouped_df = df.groupby(lvl_cat_col_list)

            print(f"... retrieving model list for {lvl_cat_col_list}")
            # return named-tuple : [Pandas(cat0_coding=0, cat1_coding=2), ...]
            lvl_model_list = df[lvl_cat_col_list].drop_duplicates().itertuples(index=False)

            for model in lvl_model_list:
                print(f"... processing data for model {model} {datetime.datetime.now()}")

                group_id = tuple(model) if len(model) > 1 else model[0]

                # get part of df that belongs to this model
                sub_df = lvl_cat_grouped_df.get_group(group_id)

                # map = fastest way, ref. StackExchange
                model_name = "model" + "-" + "-".join(map(str, model))

                # save train/test split
                for split_type in ["train", "val"]:
                    print(f"... saving {split_type} sample for model {model} {datetime.datetime.now()}")
                    
                    split_type_id_list = sorted(sub_df[sub_df["split_type"] == split_type]["output_id"].unique().tolist())

                    data_helper.save_json(split_type_id_list, f"{model_name}_id_{split_type}.json", save_dir)

                model_list += [model_name]

                # save list of classes
                print(f"... saving list of classes for model {model} {datetime.datetime.now()}")
                class_list = sorted(sub_df[cat_col_list[lvl]].unique().tolist())
                data_helper.save_json(class_list, f"{model_name}_class.json", save_dir)

                # save label dict
                print(f"... saving label dict for model {model} {datetime.datetime.now()}")
                """
                label_dict = dict(zip(sub_df["output_id"], sub_df[cat_col_list[lvl]]))
                data_helper.save_json(label_dict, f"{model_name}_label_dict_TEST.json", save_dir)
                """

                sub_df = sub_df.groupby("output_id")[cat_col_list[lvl]].apply(tuple).reset_index()

                label_dict = dict(zip(sub_df["output_id"], sub_df[cat_col_list[lvl]]))
                data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)

            print("done computing", len(model_list), model_list)

    # breakpoint()

    multilabel_update_level0_model_data(save_dir=save_dir)

def multilabel_update_level0_model_data(
    save_dir: str
    ):
    """
    Update model data for top-level model, including
    - model list
    - model list
    - train/test partition
    - label

    Input:
    - save_dir: directory for saving model data (e.g. model/)
    """
    model_list = []

    model = "model"
    model_name = "model"
    model_list += [model_name]

    model_list += [str(file_path.name).split("_")[0] for file_path in Path(save_dir).glob(f"model-*_id_train.json")]

    print("done updating model list", len(model_list), model_list)

    print(f"... saving model list {datetime.datetime.now()}")
    data_helper.save_json(model_list, f"model_list.json", save_dir)

    # df = pd.concat([pd.read_parquet(
    #     file_path, 
    #     engine="pyarrow",
    #     columns=["PaperId", "abstract", "cat0_coding", "output_id", "split_type"]
    #     ) for file_path in Path(save_dir).glob(f"*_processed_model_ready.parquet")]) 

    df = pd.concat([pd.read_parquet(
        file_path, 
        engine="pyarrow",
        columns=["PaperId", "abstract", "cat_coding", "output_id", "split_type"]
        ) for file_path in Path(save_dir).glob(f"*_processed_model_ready.parquet")])

    df = df.explode("cat_coding", ignore_index=True)

    df["cat0_coding"] = df["cat_coding"].apply(lambda x: x.split("-")[0])

    # save train/test split
    for split_type in ["train", "val"]:
        print(f"... saving {split_type} sample for model {model_name} {datetime.datetime.now()}")
        
        split_type_id_list = sorted(df[df["split_type"] == split_type]["output_id"].unique().tolist())
        data_helper.save_json(split_type_id_list, f"{model_name}_id_{split_type}.json", save_dir)

    # save list of classes
    print(f"... saving list of classes for model {model} {datetime.datetime.now()}")
    class_list = df["cat0_coding"].unique().tolist()
    data_helper.save_json(class_list, f"{model_name}_class.json", save_dir)

    # save label dict
    print(f"... saving label dict for model {model} {datetime.datetime.now()}")
    """
    label_dict = dict(zip(df["output_id"], df["cat0_coding"]))
    data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)
    """

    sub_df = df.groupby("output_id")["cat0_coding"].apply(tuple).reset_index()

    label_dict = dict(zip(sub_df["output_id"], sub_df["cat0_coding"]))
    data_helper.save_json(label_dict, f"{model_name}_label_dict.json", save_dir)

def check_cached_journal(
    journal_list: List[str],
    save_dir: str
    ) -> Tuple[List[str], List[str]]:
    """
    Check which journal is already cached and which is unseen

    Input:
    - journal_list : list of journals to compute, e.g. "ACM JEL"
    - save_dir: directory for saving model data (e.g. model/)

    Output:
    - cached_journal_list : list of journals already cached
    - unseen_journal_list: list of journals never seen before
    """
    print(f"... checking cached journals")
    if journal_list is None:
        return [], []

    cached_journal_list = []
    unseen_journal_list = journal_list.copy()

    for journal in journal_list:
        pq_file_name = f"{journal}_{journal_to_discipline_dict[journal]}_processed_model_ready.parquet"
        lmdb_dir_name = f"{journal_to_discipline_dict[journal]}_abstract_vec_lmdb"

        if Path(save_dir).joinpath(pq_file_name).exists() and Path(save_dir).joinpath(lmdb_dir_name).is_dir():
            cached_journal_list += [journal]
            unseen_journal_list.remove(journal)

    print(f"... done, returning")
    print(f"... cached journals -- {cached_journal_list}")
    print(f"... new journals to recompute -- {unseen_journal_list}")

    return cached_journal_list, unseen_journal_list

# dict for embedding list, so that load_embedding can be modularized
# can't be put on top of file! otherwise, undefined error
make_model_data_dict = {
    "single": singlelabel_make_model_data,
    "multi": multilabel_make_model_data
    }

### WORD EMBEDDING ###

def make_word_embedding(
    save_dir: str,
    emb_type: str,
    token_size: int = None,
    emb_dim: int = None
    ):
    """
    Modularize make_word_embedding. Use for Neural Network (NN)

    Input:
    - save_dir : directory for saving word embedding (e.g. shared-wos/)
        - inside will make folder emb_type
    - emb_type : type of embedding (e.g. "glove", "distilbert")
    - token_size : [GLoVe-ONLY] token size e.g. "6" for 6B model
    - emb_dim : [GLoVe-ONLY] embedding dimension e.g. "50"
    """
    print(f"RE-COMPUTING {emb_type} EMBEDDING {datetime.datetime.now()}")

    # make save directory if not already existed
    Path(save_dir).mkdir(
        mode=0o755,
        parents=True, 
        exist_ok=True
        )

    emb_param_dict = locals().copy()

    print(f"... parameters are {emb_param_dict}")

    compatibility_test_word_embedding(
        emb_type=emb_type,
        token_size=token_size,
        emb_dim=emb_dim
        )

    emb_fn = make_emb_fn_dict[emb_type]

    emb_param_dict.pop("emb_type")

    emb_dict = emb_fn(**emb_param_dict)

    print("... DONE")

def compatibility_test_word_embedding(
    emb_type: str,
    token_size: int,
    emb_dim: int
    ):
    """
    Test compatibility on parameters of word embedding, e.g. GLoVe 6B has dim 50/100/200/300
    
    Input:
    - emb_type : type of embedding (e.g. glove, distilbert)
    - token_size : [GLoVe-ONLY] token size e.g. "6" for 6B model
    - emb_dim : [GLoVe-ONLY] embedding dimension e.g. "50"

    Output: None (raise Error for invalid values)
    """
    print(f"... performing compatibility check on parameters {datetime.datetime.now()}")

    if emb_type == "glove":
        if token_size == 6:
            valid_emb_dim_list = [50, 100, 200, 300]
        elif token_size in [42, 840]:
            valid_emb_dim_list = [300]
        else:
            raise ValueError(f"... token size of {token_size} is invalid! should be in [6, 42, 840]")

        if emb_dim not in valid_emb_dim_list:
            raise ValueError(f"... emb_dim must be in {valid_emb_dim_list}")

    elif emb_type == "distilbert":
        if token_size is not None:
            raise ValueError(f"... token_size is NOT applicable to embedding type {emb_type}")
        elif emb_dim not in [768, None]:
            raise ValueError(f"... emb_dim cannot be chosen for embedding type {emb_type} (fixed at 768)")

    print("... DONE")

def make_glove_word_embedding(
    save_dir: str,
    token_size: int,
    emb_dim: int
    ):
    """
    Make GLoVe word embeddings

    Input: See make_word_embedding
    """

    # handle different downlaod url for 6B tokens
    if token_size == 6:
        emb_part = ""
    else:
        emb_part = f".{emb_dim}d"

    glove_folder_name = f"glove.{token_size}B"

    # download .zip and extract into .txt
    data_helper.download_and_extract_zip(
        # NO DOT IN THE emb_part
        data_url=f"https://nlp.stanford.edu/data/wordvecs/glove.{token_size}B{emb_part}.zip", 
        data_dir=save_dir, 
        folder_name=glove_folder_name
        )

    # convert .txt into LMDB folder
    folder_path = Path(save_dir).joinpath(glove_folder_name)

    print("... saving glove .txt into lmdb and sqlite folder")

    for file_path in folder_path.glob(f"glove.{token_size}B*.txt"):
        lmdb_dir_name = Path(str(file_path).replace(".txt", "_lmdb"))

        # make Python dict
        emb_dict = dict()
        with open(file_path, encoding="utf-8") as f:
            for line in f:
                line_content_list = line.split()
                try:
                    word = line_content_list[0]
                    emb = np.array(line_content_list[1:], dtype=float)
                    emb_dict[word] = emb
                except Exception as e:
                    print(f"... exception {e} at line {line}, line skipped")

        # lmdb
        if lmdb_dir_name.is_dir():
            print(f"..... used cache for {lmdb_dir_name.name}")
        else:
            print(f"..... processing {lmdb_dir_name.name} {datetime.datetime.now()}")
            # convert Python dict to LMDB
            data_helper.save_lmdb(
                d=emb_dict, 
                save_dir=lmdb_dir_name,
                recompute=True
                )

        # sqlite
        sqlite_file_name = Path(str(file_path.name).replace(".txt", ".sqlite"))

        if sqlite_file_name.is_file():
            print(f"..... used cache for {sqlite_file_name}")
        else:
            data_helper.save_sqlitedict(
                d=emb_dict, 
                file_name=sqlite_file_name,
                file_dir=str(file_path.parent),
                recompute=True
                )
        
        print(f"..... DONE")

def make_distilbert_word_embedding(
    save_dir: str
    ):
    """
    Make DistilBert word embeddings

    Input: See make_word_embedding
    """
    # TODO
    breakpoint()

# dict for embedding list, so that load_embedding can be modularized
# can't be put on top of file! otherwise, undefined error
make_emb_fn_dict = {
    "glove": make_glove_word_embedding,
    "distilbert": make_distilbert_word_embedding
    }


# NOTE: need to be here, o.w. circular import
def sample_word_embedding(
    emb_dict
    ) -> np.ndarray:
    """
    Get GLoVe embedding for token "the"

    Input:
    - emb_dict : txn.Transaction object of embeddings dict
    """
    if isinstance(emb_dict, dict) or isinstance(emb_dict, SqliteDict):
        return emb_dict.get("the")
    else:
        test_npy_byte = emb_dict.get(str("the").encode())
        test_npy = np.frombuffer(
            test_npy_byte, 
            dtype="float64"
            ) if test_npy_byte != 0 else 0
        return test_npy

'''
def get_value_word_embedding(
    emb_dict, 
    key: str
    ) -> np.ndarray: 
    """
    Get GLoVe embedding for the specified token

    Input:
    - emb_dict : txn.Transaction object of embeddings dict
    - key: interested token
    """
    if isinstance(emb_dict, dict):
        return emb_dict.get(key, 0)
    else:
        test_npy_byte = emb_dict.get(str(key).encode(), 0)
        test_npy = np.frombuffer(
            test_npy_byte, 
            dtype="float64"
            ) if test_npy_byte != 0 else 0
        return test_npy
'''

### TEXT VECTORIZATION ###
def make_text_vector(
    load_dir: str,
    save_dir: str,
    journal_list: List[str],
    vocab_type: str,
    output_seq_len: int,
    compute_from: str#,
    #num_df_split: int = 128
    ) -> int:
    """
    Build embedding matrix from vocabulary list 

    (RUN ON ANOTHER SCRIPT, DUE TO MULTIPROCESSING)

    Input:
    - load_dir : directory for loading vocab list (e.g. shared-wos/)

        ["i", "am", "but", "not"]

    - save_dir : directory for saving text vectors (e.g. model/)
    - journal_list : list of journals to compute, e.g. "ACM JEL"
    - vocab_type : e.g. topcount3000 
    - output_seq_len : maximum sequence length of text vectors (see model_builder.py)
    - num_df_split : number of splits for pre-processing
    - compute_from : whether to use pandas or vaex or etc. to compute

    Output:
    - text vectors, which transforms text to a sequene of token IDs, e.g.

    Return:
    - text_vectorizer PID

        "I am but am not" --> np.array([2, 3, 4, 2])
        "I am but" --> np.array([2, 3, 4, 0])

    Note:
        - + 2 index because from vectorizer, '' and '<UNK>' are placed at index 0, 1
        - by parameter 'output_mode' = 'int', default output is as above
        AND NOT ONE-/MULTI-HOT ENCODING
        - this way, output is compatible with keras.layers.Embedding
    """
    command = ["python3", str(Path(__file__).parent.joinpath(f"text_vectorizer_{compute_from}.py"))]
    command += ["--load_dir"] + [load_dir]
    command += ["--save_dir"] + [save_dir]
    command += ["--journal_list"] + journal_list
    command += ["--vocab_type"] + [vocab_type]
    command += ["--output_seq_len"] + [output_seq_len]
    #command += ["--num_df_split"] + [num_df_split]

    command = [str(c) for c in command]

    try:
        subprocess.run(command, check=True)
        # proc = subprocess.Popen(command, shell=False)
        # return proc.pid
    except subprocess.CalledProcessError as e:
        raise e

def make_emb_matrix(
    vocab_list: List[str],
    emb_dict: Union[Dict[str, np.array]],
    unk_entity: str = None
    ) -> np.ndarray:
    """
    Build embedding matrix from vocabulary list

    Input:
    - vocab_list : vocabulary list, e.g. from vectorizer.get_vocabulary()
    - emb_dict : embedding dict (should be txn.Transaction object)
    - unk_entity : <UNK> that already has embedding in the emb_dict

        <unk> for 'glove'

    Output:
    - emb_matrix with
        - number of rows: size of vocabulary list + 2 (0 = '', 1 = <UNK>)
        - number of columns: dimension of each embedding vector 
        ... (e.g. 768 for BERT)
    """
    emb_dim = sample_word_embedding(emb_dict).shape[0]
    vocab_size = len(vocab_list)

    emb_matrix = np.zeros(shape=(vocab_size+2, emb_dim))
    
    print(f"... building embedding matrix of size {emb_matrix.shape[0]} rows x {emb_matrix.shape[1]} cols")
    print(f"..... each row = vocab from list of size {vocab_size}, plus 2 for padding '' and <UNK> ")
    print(f"..... each columns = embedding of dimension {emb_dim}")

    for i, word in enumerate(vocab_list):

        try:
            emb_vector = data_helper.get_npy_value_lmdb(
                txn=emb_dict, 
                key=word,
                dtype="float64"
                )
        except KeyError:
            emb_vector = np.zeros(shape=(emb_dim, ))

        emb_matrix[i+2, :] = emb_vector

    if unk_entity is not None:
        print(f"... setting 1st-index emb to UNK, based on TextVectorization")
        
        try:
            emb_vector = data_helper.get_npy_value_lmdb(
                txn=emb_dict, 
                key=unk_entity,
                dtype="float64"
                )
        except KeyError:
            emb_vector = np.zeros(shape=(emb_dim, ))

        print(f"..... first-10 embedding: {emb_vector[:10]}")
        emb_matrix[1, :] = emb_vector

    print(f"... done, returning")

    return emb_matrix

def make_emb_matrix_multiword(
    vocab_list: List[str],
    emb_dict: Union[Dict[str, np.array]],
    unk_entity: str = None
    ) -> np.ndarray:
    """
    Build embedding matrix from vocabulary list

    Input:
    - vocab_list : vocabulary list, e.g. from vectorizer.get_vocabulary()
    - emb_dict : embedding dict (should be txn.Transaction object)
    - unk_entity : <UNK> that already has embedding in the emb_dict

        <unk> for 'glove'

    Output:
    - emb_matrix with
        - number of rows: size of vocabulary list + 2 (0 = '', 1 = <UNK>)
        - number of columns: dimension of each embedding vector 
        ... (e.g. 768 for BERT)
    """
    emb_dim = sample_word_embedding(emb_dict).shape[0]
    vocab_size = len(vocab_list)

    emb_matrix = np.zeros(shape=(vocab_size+2, emb_dim))
    
    print(f"... building embedding matrix of size {emb_matrix.shape[0]} rows x {emb_matrix.shape[1]} cols")
    print(f"..... each row = vocab from list of size {vocab_size}, plus 2 for padding '' and <UNK> ")
    print(f"..... each columns = embedding of dimension {emb_dim}")

    for i, word in enumerate(vocab_list):

        emb_vector_list = []

        for word_part in word.split():

            try:
                emb_vector = data_helper.get_npy_value_lmdb(
                    txn=emb_dict, 
                    key=word_part,
                    dtype="float64"
                    )

                emb_vector_list.append(emb_vector)

            except KeyError:
                 # CHANGE emb_vector = np.zeros(shape=(emb_dim, ))
                pass

        if emb_vector_list != []:

            emb_matrix[i+2, :] = np.average(emb_vector_list, axis=0) # CHANGE

        else:
            emb_matrix[i+2, :] = np.zeros(shape=(emb_dim, ))        

    if unk_entity is not None:
        print(f"... setting 1st-index emb to UNK, based on TextVectorization")
        
        try:
            emb_vector = data_helper.get_npy_value_lmdb(
                txn=emb_dict, 
                key=unk_entity,
                dtype="float64"
                )
        except KeyError:
            emb_vector = np.zeros(shape=(emb_dim, ))

        print(f"..... first-10 embedding: {emb_vector[:10]}")
        emb_matrix[1, :] = emb_vector

    print(f"... done, returning")

    return emb_matrix