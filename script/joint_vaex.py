import vaex
from argparse import ArgumentParser

parser = ArgumentParser()

parser.add_argument("--df1", type=str, help="path to df1")
parser.add_argument("--df2", type=str, help="path to df2")

options = parser.parse_args()

print(f"... options: {vars(options)}")

df1 = vaex.open(options.df1, progress=False)
df2 = vaex.open(options.df2, progress=False)

joint_df = df1.join(df2, on="PaperId", how="inner", allow_duplication=True)

joint_df.export_parquet("/data/ppiriyata/dummy.parquet", progress=False)