"""
Embedding Benchmark: Text vs LMDB vs SQLite

Command:
python3 script/log_glove_ram_load.py --shared_dir /home/pleum/Documents/test-shared-wos/ --output_dir /home/pleum/Documents/glove_resource_log/ --emb_type glove --emb_token_size 6 --emb_dim 50 --load_from txt
"""

from argparse import ArgumentParser, BooleanOptionalAction
import datetime
import os
from pathlib import Path
import psutil
from psutil._common import bytes2human
import time

import data_helper
import data_loader
import resource_tracker

print("BENCHMARKING EMBEDDING RAM + LOAD TIME")

pid = os.getpid()

print(f"- Process ID: {pid}")

parser = ArgumentParser()

parser.add_argument("--shared_dir", type=str, help="directory of shared resource with embedding, vocab_list, etc., e.g. '/data/shared-wos/' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
parser.add_argument("--emb_type", type=str, help="embedding type, e.g. 'glove' for glove.6B.50 ")
parser.add_argument("--emb_token_size", type=int, default=None, help="embedding token size, e.g. '6' for glove.6B.50 ")
parser.add_argument("--emb_dim", type=int, default=None, help="embedding dimension e.g. '50' for glove.6B.50 ")
parser.add_argument("--load_from", type=str, help="whether to load from 'txt' or 'lmdb' or 'sqlite' ")

parser.add_argument("--recompute_emb", action=BooleanOptionalAction, default=False, help="whether recompute embedding components, default = False")

options = parser.parse_args()

print(f"... options: {vars(options)}")

log_file_name = f"{options.emb_type}.{options.emb_token_size}B.{options.emb_dim}d_{options.load_from}"
    
# initialize output dir

Path(options.output_dir).mkdir(
    mode=0o755,
    exist_ok=True, 
    parents=True
    )

# initialize resource loggers

overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )

cpu_ram_p = resource_tracker.log_cpu_ram(
    pid=pid,
    model_name=log_file_name,
    save_dir=options.output_dir
    )

# load emb

start_time = datetime.datetime.now()

print(f"start time: {start_time}")

emb_dict = data_loader.load_word_embedding(
    load_dir=Path(options.shared_dir).joinpath("word_embeddings"),
    emb_type=options.emb_type,
    token_size=options.emb_token_size,
    emb_dim=options.emb_dim,
    recompute=options.recompute_emb,
    load_from=options.load_from
    )

end_time = datetime.datetime.now()
print(f"end time: {start_time}")

# end resource loggers
time.sleep(1)

overall_gpu_p.terminate()
proc_gpu_p.terminate()
cpu_ram_p.terminate()

# log static resource (load time, final ram usage, etc.)

log_path = Path(options.output_dir).joinpath(f"{log_file_name}_static_log.json")

if log_path.is_file():
    log_dict = data_helper.load_json(
        file_path=log_path
        )
else:
    log_dict = dict()

## load time

log_dict["load-time-seconds"] = (end_time - start_time).total_seconds()

## final ram usage

proc = psutil.Process(pid)

ram_usage = bytes2human(proc.memory_info().rss)

log_dict["ram-usage"] = ram_usage

# save static resource

data_helper.save_json(
    file=log_dict,
    file_name=log_path.name,
    file_dir=log_path.parent
    )