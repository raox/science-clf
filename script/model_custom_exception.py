class CustomModelException(Exception):
	"""
	Custom Exception class for re-spawning the model training when triggered
	- e.g. when the number of threads is above threshold
	"""
	pass