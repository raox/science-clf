"""
python3 script/merge_top_model_data.py --parent_dir /home/pleum/Documents/ --output_dir /home/pleum/Documents/test-merge/ --journal_dir_list test-ACM test-JEL
"""

from argparse import ArgumentParser
import lmdb
from pathlib import Path
import shutil

import data_helper

parser = ArgumentParser()

parser.add_argument("--parent_dir", type=str, help="parent directory of model folders, etc., e.g. '/data/srao/*-model' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory to save to under parent_dir, e.g. 'all-model' to save to '/data/srao/all-model' in mtec-cae-gpu01 ")
parser.add_argument("--journal_dir_list", nargs="+", help="journal directory name list under parent directory (space between), e.g. 'Chem-output Agri-output'  in mtec-cae-gpu01")

options = parser.parse_args()

print("MERGING MODEL_LIST")

Path(options.output_dir).mkdir(parents=True, exist_ok=True)

# symlink

for journal in options.journal_dir_list:
    # processed pq
    for file in Path(options.parent_dir).joinpath(journal).glob("*_processed_model_ready.parquet"):
        Path(options.output_dir).joinpath(file.name).unlink(missing_ok=True)
        Path(options.output_dir).joinpath(file.name).symlink_to(file)
    
    # model json
    for file in Path(options.parent_dir).joinpath(journal).glob("model-*_*.json"):
        Path(options.output_dir).joinpath(file.name).unlink(missing_ok=True)
        Path(options.output_dir).joinpath(file.name).symlink_to(file)

    """
    # abstract vec lmdb
    for folder in Path(options.parent_dir).joinpath(journal).glob("*_abstract_vec_lmdb"):
        Path(options.output_dir).joinpath(folder.name).unlink(missing_ok=True)
        Path(options.output_dir).joinpath(folder.name).symlink_to(folder)
    """

# list info

info_list = ["class", "id_train", "id_val", "list"]

for info in info_list:
    print(f"... processing info {info}")
    merge_data = list()

    for journal in options.journal_dir_list:
        print(f"...adding journal {journal}")
        merge_data += data_helper.load_json(file_path=Path(options.parent_dir).joinpath(journal, f"model_{info}.json"))

        if info == "list":
            merge_data.remove("model")

    if info == "list":
        merge_data.insert(0, "model")

    print(f"... saving info {info}")
    Path(options.output_dir).joinpath(f"model_{info}.json").unlink(missing_ok=True)
    data_helper.save_json(
        file=merge_data,
        file_name=f"model_{info}.json",
        file_dir=options.output_dir
        )

    del merge_data

# dict info (label_dict)

for info in ["label_dict"]:
    print(f"... processing info {info}")
    merge_data = dict()

    for journal in options.journal_dir_list:
        print(f"...adding journal {journal}")
        merge_data.update(data_helper.load_json(file_path=Path(options.parent_dir).joinpath(journal, f"model_{info}.json")))

    print(f"... saving info {info}")
    Path(options.output_dir).joinpath(f"model_{info}.json").unlink(missing_ok=True)
    data_helper.save_json(
        file=merge_data,
        file_name=f"model_{info}.json",
        file_dir=options.output_dir
        )

    del merge_data

"""
# lmdb text vectors

print("... processing text vectors lmdb")

lmdb_folder = Path(options.output_dir).joinpath("abstract_vec_lmdb")

if lmdb_folder.is_dir():
    if lmdb_folder.is_symlink():
        lmdb_folder.unlink()
    else:
        shutil.rmtree(lmdb_folder)

merge_env = lmdb.open(
    str(lmdb_folder),
    map_size=int(1e12)
    )

with merge_env.begin(write=True) as merge_txn:
    for journal in options.journal_dir_list:
        for folder in Path(options.parent_dir).joinpath(journal).glob("*_abstract_vec_lmdb"):
            txn = data_helper.load_lmdb(
                folder_path=folder
                )

            db = txn.cursor()

            for (k, v) in db:
                try:
                    merge_txn.put(k, v)

                except Exception as e:
                    print(f"Exception {e}")
                    pass

            db.close()
"""