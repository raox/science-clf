import pandas as pd
from pathlib import Path
import vaex

import data_helper

print(f"loading our ACM/JEL/MSC pq")

our_df = vaex.open_many([
	"/data/ppiriyata/all-wos/ACM_infk_processed.parquet", 
	"/data/ppiriyata/all-wos/JEL_econ_processed.parquet",
	"/data/ppiriyata/all-wos/MSC_math_processed.parquet"
	# "/home/pleum/Documents/test-wos/ACM_infk_processed.parquet",
	# "/home/pleum/Documents/test-wos/JEL_econ_processed.parquet"
	])

print(f"... sanity check: shape = {our_df.shape}")

print(our_df.head(1))

print(f"fetching unique PaperIds from our pq")

unique_paperid = our_df.PaperId.unique(
	dropnan=True, 
	dropmissing=True, 
	limit_raise=False, 
	progress=True
	)

print(f"... sanity check: shape = {len(unique_paperid)}")

print(unique_paperid[:10])

print(f"creating dummy pq for OUR PaperId")

dummy_paperid_df = vaex.from_arrays(PaperId=unique_paperid)

print(f"loading paper2kw df")

paper2kw_df = vaex.from_csv(
	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/PaperFieldsOfStudy.txt",
	# "/home/pleum/Documents/PaperFieldsOfStudy_test.txt",
	header=None,
	usecols=[0, 1, 2],
	names=["PaperId", "FieldOfStudyId", "FieldOfStudyProba"],
	delimiter="\t",
	progress=True
	)

print(f"filtering only OUR paper2kw df")

our_paper2kw_df = paper2kw_df.join(
	dummy_paperid_df, 
	on="PaperId",
	how="inner",
	allow_duplication=True
	)

print(f"... sanity check: shape after joining = {our_paper2kw_df.shape}")

print(our_paper2kw_df.head(1))

print(f"saving OUR paper2kw df")

our_paper2kw_df.export_parquet(
	"/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_PaperFieldsOfStudy.parquet",
	# "/home/pleum/Documents/test-wos/ACM-JEL_PaperFieldsOfStudy_test.parquet",
	progress=True,
	parallel=True
	)

# del our_paper2kw_df

print(f"making OUR kw2count df")

kw2count_df = pd.DataFrame(
	our_paper2kw_df.FieldOfStudyId.value_counts(
		dropna=True,
		dropnan=True,
		dropmissing=True,
		ascending=False,
		progress=True
		)
	)

kw2count_df.reset_index(
	inplace=True
	)
kw2count_df.columns = ["FieldOfStudyId", "count"]

print(f"... sanity check: num unique keywords = {kw2count_df.shape[0]}")

print(kw2count_df.head(1))

print(f"saving OUR kw2count df")

kw2count_df.to_parquet(
	"/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_FieldsOfStudyCount_dummy.parquet",
	# "/home/pleum/Documents/test-wos/ACM-JEL_FieldsOfStudyCount.parquet",
	engine="pyarrow",
	index=False
	)

del kw2count_df

print(f"loading kw df and kw2count df again")

kw_df = vaex.from_csv(
	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/FieldsOfStudy.txt",
	# "/home/pleum/Documents/FieldsOfStudy_test.txt",
	header=None,
	usecols=[0, 2],
	names=["FieldOfStudyId", "FieldOfStudy"],
	delimiter="\t",
	progress=True
	)

kw2count_df = vaex.open(
	"/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_FieldsOfStudyCount_dummy.parquet",
	# "/home/pleum/Documents/test-wos/ACM-JEL_FieldsOfStudyCount.parquet",
	progress=True
	)

print(f"adding kw name to kw2count df")

sorted_kw2count_df = kw2count_df.join(
	kw_df,
	on="FieldOfStudyId",
	how="inner",
	allow_duplication=False
	)

print(f"sorting by count MAX to MIN")

sorted_kw2count_df = sorted_kw2count_df.sort(
	"count",
	ascending=False
	)

print(f"... sanity check: num unique keywords AFTER ADDING FULL KW NAME = {sorted_kw2count_df.shape[0]} CHECK SORT")

print(sorted_kw2count_df.head(3))

sorted_kw2count_df.export_parquet(
	"/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_FieldsOfStudyCount.parquet",
	# "/home/pleum/Documents/test-wos/ACM-JEL_FieldsOfStudyCount.parquet",
	progress=True,
	parallel=True
	)

print(f"getting 3000 keywords")

top3000kw_list = sorted_kw2count_df.head(3000).to_dict(column_names=["FieldOfStudy"])["FieldOfStudy"].tolist()

print(f"... sanity check: num 3000 keywords to save = {len(top3000kw_list)}")

print(top3000kw_list[:10])

print(f"saving top 3000 kw")

data_helper.save_txt(
	file=top3000kw_list,
	file_name="topkeywordcount3000_vocab_list.txt",
	file_dir="/data/ppiriyata/shared-wos/vocab_list/",
	mode="w"
	)

print(f"getting paper2kw df with top3000 kw")

top3000kw_id_list = sorted_kw2count_df.head(3000).to_dict(column_names=["FieldOfStudyId"])["FieldOfStudyId"].tolist()

print(f"... sanity check: num 3000 keyword IDs to save = {len(top3000kw_id_list)}")

print(top3000kw_id_list[:10])

print(f"saving top 3000 kw IDs")

data_helper.save_txt(
	file=top3000kw_id_list,
	file_name="topkeywordcount3000_id_list.txt",
	file_dir="/data/ppiriyata/shared-wos/vocab_list/",
	mode="w"
	)

our_paper2kw_df_top3000 = our_paper2kw_df[our_paper2kw_df.FieldOfStudyId.isin(top3000kw_id_list)]

print(f"... sanity check: num unique rows = {our_paper2kw_df_top3000.shape[0]}")

print(our_paper2kw_df_top3000.head(3))

print(f"saving OUR paper2kw - top 3000 kw df")

our_paper2kw_df_top3000.export_parquet(
	"/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_PaperFieldsOfStudy_top3000.parquet",
	# "/home/pleum/Documents/test-wos/ACM-JEL_PaperFieldsOfStudy_test.parquet",
	progress=True,
	parallel=True
	)

Path("/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_FieldsOfStudyCount_dummy.parquet").unlink()