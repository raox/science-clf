from pyspark import SparkConf
from pyspark.sql import SparkSession
from argparse import ArgumentParser

spark = SparkSession.builder.master("local").getOrCreate()
sc = spark.sparkContext

parser = ArgumentParser()

parser.add_argument("--df1", type=str, help="path to df1")
parser.add_argument("--df2", type=str, help="path to df2")

options = parser.parse_args()

print(f"... options: {vars(options)}")

df1 = spark.read.parquet(options.df1)
df2 = spark.read.parquet(options.df2)

joint_df = df1.join(df2, on="PaperId", how="inner")

joint_df.write.parquet("/data/ppiriyata/dummy.parquet")