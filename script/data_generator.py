# -*- coding: utf-8 -*-
import random
from typing import Dict, List

import numpy as np
from sklearn.preprocessing import MultiLabelBinarizer

import data_helper


def data_generator_transformer(
		txn_dict: dict,
		partition_list: List[str],
		label_dict: Dict[str, int],
		batch_size: int,
		max_token_id_seq_len: int,
		shuffle: bool,
		binarizer: MultiLabelBinarizer
		):
	"""
	Stream-load data for each batch of model training

	Input:
	- load_dir :
	- model_name :
	- partition_list :
	- label_dict :
	- batch_size :
	- max_token_id_seq_len :
	- shuffle :
	"""

	if shuffle:
		print("... shuffling partition_list")
		random.shuffle(partition_list)

	for batch_partition_list in data_helper.list2chunks(l=partition_list, chunk_size=batch_size):
		# print("... initializing batch X and y")
		X = np.zeros(
			shape=(batch_size, max_token_id_seq_len), dtype=np.str_
			)  # dtype needed, o.w. can't convert to tensor # CHANGE from model_runner.py
		# raw_y = [(-1, )] * batch_size # not used?

		X = np.array(
			[
				data_helper.get_text_value_lmdb(
					txn=txn_dict[partition.split("-")[1]],  # CHANGE from model_runner.py
					key=partition,
					dtype="int64"
					)
				for partition in batch_partition_list]
			)
		y = binarizer.transform(np.array([tuple(label_dict[partition]) for partition in batch_partition_list]))

		yield X, y


def data_generator_cnn_rnn(
		txn_dict: dict,
		partition_list: List[str],
		label_dict: Dict[str, int],
		batch_size: int,
		max_token_id_seq_len: int,
		shuffle: bool,
		binarizer: MultiLabelBinarizer,

		):
	"""
	Stream-load data for each batch of model training

	Input:
	- load_dir :
	- model_name :
	- partition_list :
	- label_dict :
	- batch_size :
	- max_token_id_seq_len :
	- shuffle :
	"""

	if shuffle:
		print("... shuffling partition_list")
		random.shuffle(partition_list)

	for batch_partition_list in data_helper.list2chunks(l=partition_list, chunk_size=batch_size):
		# print("... initializing batch X and y")
		X = np.zeros(
			shape=(batch_size, max_token_id_seq_len), dtype=np.int64
			)  # dtype needed, o.w. can't convert to tensor
		raw_y = [(-1,)] * batch_size

		X = np.array(
			[
				data_helper.get_npy_value_lmdb(
					txn=txn_dict[partition.split("-")[1]],
					key=partition,
					dtype="int64"
					)
				for partition in batch_partition_list]
			)
		y = binarizer.transform(np.array([tuple(label_dict[partition]) for partition in batch_partition_list]))

		# print(X)

		yield X, y
