from fastapi import APIRouter, HTTPException
from pydantic import BaseModel, Field

import json
import requests
from typing import List

from script.inference_by_model import InferenceRequestByModel
from script import inference_utils

# ROUTER

router = APIRouter()

# AUTO-DETERMINE EXPERIMENT
# logic: 
# - find out top-level model (directory)
# - use inference_by_model top-level model
# - get label (says single, but can also be multi) 
# - find corresponding model (directory) 
# - use inference_by_model there

class InferenceRequestAutoModel(BaseModel):
    text: str = Field(
        description="text to get inference",
        example="This is mathematics text."
        )

class InferenceReplyAutoModel(BaseModel):
    text: str = Field(
        description="original input text"
        )
    prediction: List[str] = Field(
        description="predicted label"
        )

@router.post(
    "/inference_auto_model",
    response_model=InferenceReplyAutoModel,
    description="ask the api to predict text label"
    )
async def model_inference(request: InferenceRequestAutoModel):

    output_class_list = []

    # top-level
    encoded_input = InferenceRequestByModel(
        text=request.text,
        experiment=inference_utils.model_name_to_experiment(model_name=f"model")
        )

    output_proba, output_class_code = inference_utils.make_model_inference(encoded_input)
    output_class_list += output_class_code

    output_class_list = ["1"] # TEST !

    for level in range(inference_utils.num_level):

        next_level_output_class_list = []

        for output_class in output_class_list:
            print(f"evaluating model {output_class}")
            encoded_input = InferenceRequestByModel(
                text=request.text,
                experiment=inference_utils.model_name_to_experiment(model_name=f"model-{output_class}")
                )
            output_proba, output_class_code = inference_utils.make_model_inference(encoded_input)
            next_level_output_class_list += [f"{output_class}-{code}" for code in output_class_code]       

        output_class_list = next_level_output_class_list 

        print(output_class_list)

    reply = InferenceReplyAutoModel(
        text=request.text,
        prediction=output_class_list
        )
    return reply