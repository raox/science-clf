from argparse import ArgumentParser, BooleanOptionalAction
from pathlib import Path
import shutil
import subprocess

parser = ArgumentParser()

parser.add_argument("--to_benchmark", nargs="+", help="list of things to benchmark (space between), e.g. 'glove abstract_vec' ")

options = parser.parse_args()

if "glove" in options.to_benchmark:

    valid_glove_B_d_list = [
        (6, 50),
        (6, 100),
        (6, 200),
        (6, 300),
        (42, 300),
        (840, 300)
        ]

    for B_d in valid_glove_B_d_list:
        B, d = B_d
        print(f"BENCHMARKING glove.{B}b.{d}.d")

        for source in ["txt", "lmdb", "sqlite"]:

            command = ["python3", str(Path(__file__).parent.joinpath("log_glove_ram_load.py"))]
            command += ["--shared_dir"] + ["/data/ppiriyata/shared-wos/"]
            command += ["--output_dir"] + ["/data/ppiriyata/benchmark-result/glove/"]
            command += ["--emb_type"] + ["glove"]
            command += ["--emb_token_size"] + [B]
            command += ["--emb_dim"] + [d]
            command += ["--load_from"] + [source]

            command = [str(c) for c in command]

            try:
                subprocess.run(command, check=True)
            except subprocess.CalledProcessError as e:
                raise e

            command2 = command
            command2[1] = str(Path(__file__).parent.joinpath("log_glove_query.py"))
            command2 += ["--num_query"] + [100000]

            command2 = [str(c) for c in command2]

            try:
                subprocess.run(command2, check=True)
            except subprocess.CalledProcessError as e:
                raise e

if "abstract_vec_store" in options.to_benchmark:
    print(f"BENCHMARKING abstract_vec_store")

    for source in ["npy", "lmdb"]:
        print(f"BENCHMARKING abstract_vec via {source}")

        for data_size in [10000]:

            command = ["python3", str(Path(__file__).parent.joinpath("log_abs_vec_lmdb_v_npy.py"))]
            command += ["--model_dir"] + [f"/data/ppiriyata/benchmark-dataset/test-ACM-{data_size}/"]
            command += ["--output_dir"] + ["/data/ppiriyata/benchmark-result/abstract_vec_store/"]
            command += ["--load_from"] + [source]
            command += ["--num_query"] + [int(10*data_size)]

            command = [str(c) for c in command]

            try:
                subprocess.run(command, check=True)
            except subprocess.CalledProcessError as e:
                raise e

if "abstract_vec_compute" in options.to_benchmark:
    print(f"BENCHMARKING abstract_vec_compute")

    for source in ["pandas", "vaex", "dask"]:

        for data_size in [100, 1000, 10000, 100000, 1000000]:
            print(f"BENCHMARKING abstract_vec via {source}")

            """
            shutil.rmtree(
                f"/home/pleum/Documents/test-ACM-{data_size}/ACM_infk_abstract_vec_lmdb/",
                ignore_errors=True
                )

            command2 = ["python3", str(Path(__file__).parent.joinpath(f"text_vectorizer_{source}.py"))]
            command2 += ["--load_dir"] + [f"/home/pleum/Documents/shared-wos/"]
            command2 += ["--save_dir"] + [f"/home/pleum/Documents/test-ACM-{data_size}/"]
            command2 += ["--journal_list"] + [f"ACM"]
            command2 += ["--vocab_type"] + [f"topcount3000"]
            command2 += ["--output_seq_len"] + [200]

            command2 += ["--log_dir"] + [f"/home/pleum/Documents/abstract_vec_compute/"]
            """

            shutil.rmtree(
                f"/data/ppiriyata/benchmark-dataset/test-ACM-{data_size}/ACM_infk_abstract_vec_lmdb/",
                ignore_errors=True
                )

            command2 = ["python3", str(Path(__file__).parent.joinpath(f"text_vectorizer_{source}.py"))]
            command2 += ["--load_dir"] + [f"/data/ppiriyata/shared-wos/"]
            command2 += ["--save_dir"] + [f"/data/ppiriyata/benchmark-dataset/test-ACM-{data_size}/"]
            command2 += ["--journal_list"] + [f"ACM"]
            command2 += ["--vocab_type"] + [f"topcount3000"]
            command2 += ["--output_seq_len"] + [200]

            command2 += ["--log_dir"] + [f"/data/ppiriyata/benchmark-result/abstract_vec_compute/"]

            command2 = [str(c) for c in command2]

            try:
                subprocess.run(command2, check=True)
            except subprocess.CalledProcessError as e:
                raise e