from fastapi import APIRouter, HTTPException
from pydantic import BaseModel, Field

import json
import mlflow
import numpy as np
import os
from pathlib import Path
import pickle
from typing import List

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

from tensorflow.keras.layers import TextVectorization

from script import inference_utils

# ROUTER

router = APIRouter()

class InferenceRequestByModel(BaseModel):
    text: str = Field(
        description="text to get inference",
        example="This is mathematics text."
        )
    model_name: str = Field(
        description="model name",
        example="model-1"
        )

class InferenceReplyByModel(BaseModel):
    text: str = Field(
        description="original input text"
        )
    prediction: List[int] = Field(
        description="predicted label"
        )

@router.post(
    "/inference_by_model",
    response_model=InferenceReplyByModel,
    description="ask the api to predict text label"
    )
async def model_inference(request: InferenceRequestByModel): # instead of async def, so that can be called from another API endpoint

    output_proba, output_class_code = inference_utils.make_model_inference(
        request
        )

    reply = InferenceReplyByModel(
        text=request.text,
        prediction=output_class_code
        )
    return reply