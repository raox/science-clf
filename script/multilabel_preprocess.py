## PREPROCESSING CODING PART

print("PREPROCESSING CODING")

# from argparse import ArgumentParser
import gc
from pathlib import Path
from pyspark import SparkConf
from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType
import vaex

import var_setup

folder_to_coding_dict = {
	"CS": 0,
	"Econ": 1
}

folder_to_discipline_dict = {
	"CS": "infk",
	"Econ": "econ"
}

folder_to_journal_dict = {
	"CS": "ACM",
	"Econ": "JEL"
}

stem_list = []

for folder in folder_to_discipline_dict:
	discipline = folder_to_discipline_dict[folder]
	journal = folder_to_journal_dict[folder]
	stem_list += [f"{journal}_{discipline}"]

main_folder = "/data/srao/wos/old-parquet-field-files"

save_folder = "/data/ppiriyata/all-multilabel-wos-post-thesis"

Path(save_folder).mkdir(parents=True, exist_ok=True)

spark = SparkSession.builder.master("local").getOrCreate()
sc = spark.sparkContext

for folder in folder_to_coding_dict:

	# handle abstract

	file_path_abstract = Path(main_folder).joinpath(folder, f"field_abstract_{folder.lower()}")

	print(f"... loading ABSTRACT from {file_path_abstract}")

	df_abstract = vaex.open(str(file_path_abstract))

	df_id_list = df_abstract.PaperId.to_numpy().tolist() # don't use list() o.w. dtype = np.int!!!

	save_path_abstract = Path(save_folder).joinpath(f"{folder_to_journal_dict[folder]}_{folder_to_discipline_dict[folder]}_processed_abstract.parquet")

	print(f"... saving ABSTRACT to {save_path_abstract}")

	df_abstract[["PaperId", "abstract"]].export_parquet(
		str(save_path_abstract), 
		progress=True
		)

	del df_abstract
	gc.collect()

	# handle coding

	file_path_coding = sorted(Path(main_folder).joinpath(folder).glob("_filtered_noabstract_cat123_num_coding.pkl.*"))[0]

	print(f"... loading CODING from {file_path_coding}")

	df_coding_multi = spark.read.parquet(str(file_path_coding))

	df_coding_multi = df_coding_multi.withColumn("cat0", F.lit(folder_to_discipline_dict[folder]))
	df_coding_multi = df_coding_multi.withColumn("cat0_coding", F.lit(folder_to_coding_dict[folder]))

	df_coding_multi = df_coding_multi.withColumn(
		"cat_coding", 
		F.concat(
			df_coding_multi.cat0_coding.cast(StringType()),
			F.lit("-"),
			df_coding_multi.cat1_coding.cast(StringType()),
			F.lit("-"),
			df_coding_multi.cat2_coding.cast(StringType())
			)
	)

	df_coding_single = df_coding_multi.groupBy("PaperId").agg(
		F.collect_set("cat_coding").alias("cat_coding")
		)

	# df_coding_single = df_coding_single[df_coding_single.PaperId.isin(df_id_list)] # too slow !

	save_path_coding = Path(save_folder).joinpath(f"{folder_to_journal_dict[folder]}_{folder_to_discipline_dict[folder]}_processed_coding.parquet")

	print(f"... saving CODING to {save_path_coding}")

	df_coding_single.write.parquet(str(save_path_coding))

	del df_coding_multi
	del df_coding_single
	gc.collect()

sc.stop()

## JOINING TABLES

print("JOINING TABLES")

for stem in stem_list:

	# handle abstract

	file_path_abstract = Path(save_folder).joinpath(f"{stem}_processed_abstract.parquet")

	print(f"... loading ABSTRACT to {file_path_abstract}")

	df_abstract = vaex.open(str(file_path_abstract))

	print("shape", df_abstract.shape)

	# handle coding

	file_path_coding = Path(save_folder).joinpath(f"{stem}_processed_coding.parquet")

	print(f"... loading CODING to {file_path_coding}")

	df_coding = vaex.open(str(file_path_coding))

	print("shape", df_coding.shape)

	print(f"... processing MERGE")

	df_merge = df_abstract.join(df_coding, on="PaperId", how="inner")

	print("shape", df_merge.shape)

	"""
	print(f"... converting coding to list")

	for coding_col in [col for col in df_merge.colum_names if col.startswith("cat") and col.endswith("_coding")]:
		df_merge[coding_col] = 
	"""

	save_path_merge = Path(save_folder).joinpath(f"{stem}_processed.parquet")

	print(f"... saving MERGE to {save_path_merge}")

	df_merge.export_parquet(
		str(save_path_merge),
		progress=True
		)