import numpy as np
from sklearn.metrics import accuracy_score, f1_score, recall_score, precision_score
from sklearn.metrics import classification_report
import os
from typing import Dict, Tuple

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

import tensorflow as tf
from tensorflow.keras.callbacks import Callback

import var_setup

np.random.seed(var_setup.random_state)
tf.keras.utils.set_random_seed(var_setup.random_state)
tf.random.set_seed(var_setup.random_state)
tf.config.experimental.enable_op_determinism()



"""
model.fit(x_train,y_train, epochs=10, 
          callbacks=[PerfMatrics(train=(x_train,y_train),validation=(x_val,y_val)),es])
"""

# print(f"... fitting model {datetime.datetime.now()}")

class PerfMatrics(Callback):
    """
    Custom Callback class for tracking perf metrics for *each* epoch (and not each batch)
    """
    def __init__(
        self,
        train: Tuple[np.array, np.array],
        validation: Tuple[np.array, np.array]
        ):
        """
        Initialize the class + train and val data
        """
        super(PerfMatrics, self).__init__()
        self.train = train
        self.validation = validation

    def on_epoch_end(
        self,
        epoch: int,
        logs: Dict[str, float] = dict()
        ):
        """
        Log performance metrics for *each* epoch
        - acc, f1, recall, precision
        """

        for data_type in ["train", "validation"]:

            feature, targ = getattr(self, data_type)

            predict = np.asarray(self.model.predict(feature))        
        
            acc = round(accuracy_score(targ, predict), 4)
            f1 = round(f1_score(targ, predict), 4)
            recall = round(recall_score(targ, predict), 4)     
            precision = round(precision_score(targ, predict), 4)
            report = classification_report(targ, predict)

            logs[f"epoch_{data_type}_acc"] = acc
            logs[f"epoch_{data_type}_f1"] = f1
            logs[f"epoch_{data_type}_recall"] = recall
            logs[f"epoch_{data_type}_precision"] = precision
            logs[f"epoch_{data_type}_report"] = report
            
            print(f"— acc: {acc}  — f1: {f1} — precision: {precision}, — recall: {recall}")
            print(f"classification report: {classification_report}")