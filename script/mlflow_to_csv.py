import json
import pandas as pd
from pathlib import Path
import subprocess
from urllib.parse import urlparse

def get_model_setup(artifact_uri):
    try:
        model_setup_path = Path(urlparse(artifact_uri).path).joinpath("artifacts", "model_set_up")

        with open(list(Path(model_setup_path).glob("model*_class.json"))[0], "r") as f:
            class_list = json.load(f)

        with open(list(Path(model_setup_path).glob("model*_id_train.json"))[0], "r") as f:
            id_train = json.load(f)

        with open(list(Path(model_setup_path).glob("model*_id_val.json"))[0], "r") as f:
            id_val = json.load(f)


        return pd.Series([len(id_train), len(id_val), len(class_list)])
    except:
        return pd.Series([None, None, None])

def get_model_type(artifact_uri):
    try:
        model_setup_path = Path(urlparse(artifact_uri).path).joinpath("artifacts", "model_set_up")

        with open(Path(model_setup_path).joinpath("model_set_up.json"), "r") as f:
            model_set_up = json.load(f)

        model_type = [r for r in model_set_up["model_type_list"] if r != "dummy"][0]

        return model_type
    except:
        return None

Path("csv-folder").mkdir(parents=True, exist_ok=True)

for path in Path("mlruns").glob("*"):
    exp_name = path.name
    subprocess.run(["mlflow", "experiments", "csv", "-x", exp_name, "-o", f"csv-folder/test-{exp_name}.csv"])

df_all = pd.concat([pd.read_csv(file_path) for file_path in Path("csv-folder").glob("*")], ignore_index=True)

df_all[["size_train_set", "size_test_set", "num_class"]] = df_all["artifact_uri"].apply(get_model_setup)

df_all["model_type"] = df_all["artifact_uri"].apply(get_model_type)

df_all.dropna(subset=["end_time", "metrics.categorical_accuracy"], inplace=True)

df_all.to_csv("all-result.csv", columns=[col for col in list(df_all.columns) if "f1_m" not in col], index=False)

df_model_map = pd.DataFrame()

df_model_map["model_name"] = df_all["tags.mlflow.runName"].str.replace("test_", "")[df_all["model_type"] == "rnn"]
df_model_map["directory"] = df_all["experiment_id"].astype(str) + "/" + df_all["run_id"][df_all["model_type"] == "rnn"]
model_map_dict = df_model_map.set_index("model_name").to_dict()["directory"]

with open("model_map.json", "w") as f:
    json.dump(model_map_dict, f, indent=4, ensure_ascii=False)