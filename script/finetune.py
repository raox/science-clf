# -*- coding: utf-8 -*-
import datetime
import gc
import logging
import multiprocessing
import os
import random
from argparse import ArgumentParser, BooleanOptionalAction
from pathlib import Path

import platform
import pickle
import data_generator, data_helper, data_loader

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # need to come before import tf
os.environ["TFHUB_MODEL_LOAD_FORMAT"] = "UNCOMPRESSED"

import math
import mlflow
from keras.engine.functional import Functional
from keras.models import load_model
from math import ceil
from sklearn.preprocessing import MultiLabelBinarizer
import tensorflow as tf

from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.metrics import CategoricalAccuracy, Precision, Recall
from tensorflow_addons.metrics import F1Score 
from tensorflow.keras.optimizers import RMSprop, Adam
from tensorflow.keras import layers, models
import tensorflow_hub as hub
import tensorflow_text as text # need even if not used

# np.random.seed(var_setup.random_state)
# random.seed(var_setup.random_state)
# tf.experimental.numpy.random.seed(var_setup.random_state)
# tf.keras.utils.set_random_seed(var_setup.random_state)
# tf.random.set_seed(var_setup.random_state)
# tf.config.experimental.enable_op_determinism()


logging.getLogger("mlflow").setLevel(logging.DEBUG)

parser = ArgumentParser()
# parser.add_argument("--model_saved_path", type=str, help="directory of saved model in mlflow, e.g., /data-nfs/wos/wos/all-output/mlruns/265/5831a2afe97c456a9b68ac460894b686/artifacts/model/data/model")
parser.add_argument("--vocab_type", type=str, default = 'topcount3000', help="vocabulary type, e.g. 'topcount3000' ")
parser.add_argument("--model_dir", type=str, help="directory of wos, e.g. '/data-nfs/wos/wos/all-model/' in mtec-cae-gpu01 ")
parser.add_argument("--batch_size", type=int, default=1024, help="batch size, e.g. '1024' ")
parser.add_argument("--pretrain_epoch", type=int, default=1, help="number of pretrain epochs, default = 1")
parser.add_argument("--finetune_epoch", type=int, default=10, help="number of finetune epochs, default = 1")
parser.add_argument(
	"--dgen_shuffle", action=BooleanOptionalAction, default=True,
	help="whether shuffle in data generator, default = False"
	)
parser.add_argument(
	"--max_token_id_seq_len", type=int, default=200, help="max. token sequence length to consider from each text"
	)
parser.add_argument(
	"--output_dir", type=str, default="data-nfs/wos/wos/all-output/",
	help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 "
	)
parser.add_argument(
	"--transformer", action=BooleanOptionalAction, default=False,
	help="whether the loaded model is transformer, default = False"
	)

options = parser.parse_args()
print(options)

MODEL_NAME: str = "model-1"
FIELD: str = "econ"
MODEL_INPUTS_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data"
MODEL_SAVED: str = "/data-nfs/wos/wos/all-transformer-output-POST-THESIS/mlruns/327/19b8c60d632a4bb7899ef9d11c2b2bcc/artifacts" # transfomer
# "/data-nfs/wos/wos/all-output/mlruns/53/eb260f8a4d724beb8494f1f5a732d03c/artifacts" # dnn
# "/data-nfs/wos/wos/all-output/mlruns/1573/3645fc5dd19141fd9b3dd551cee8745c/artifacts" # rnn
MODEL_SAVED_PATH: str =  MODEL_SAVED + "/model/data/model"
# "/data-nfs/wos/wos/all-output/mlruns/265/5831a2afe97c456a9b68ac460894b686/artifacts/model/data/model" # cnn

FINETUNE_ONWARDS: int = 3  # Finetune from this layer onwards
PRETRAIN_EPOCHS: int = 1
FINETUNE_EPOCHS: int = 1
TRANSFORMER: bool = False


txn_dict = dict()
if options.transformer:
	txn_dict[FIELD] = data_helper.load_lmdb(folder_path="/data-nfs/wos/wos/all-model/econ_abstract_lmdb")
else:
	txn_dict[FIELD] = data_helper.load_lmdb(folder_path="/data-nfs/wos/wos/all-model/econ_abstract_vec_lmdb")
partition_train = data_loader.load_model_data(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME,
	split_type="train"
	)
label_dict = data_loader.load_label_dict(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME
	)
input_batch_size = int(options.batch_size)
batch_size = min(
	2 ** (math.floor(math.log(len(partition_train), 2))),
	input_batch_size
	)
class_list = data_loader.load_class(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME
	)
num_class: int = len(class_list)
binarizer = MultiLabelBinarizer()
binarizer.fit([tuple(class_list)])



### MLFLOW ###
new_uri = Path(options.output_dir).joinpath("mlruns").resolve().as_uri()
mlflow.set_tracking_uri(new_uri)
print(new_uri)

client = mlflow.tracking.MlflowClient()
experiment_name = f"test_{datetime.datetime.now()}"
mlflow.set_experiment(experiment_name)
experiment = mlflow.get_experiment_by_name(experiment_name)
mlflow.start_run(
	experiment_id=experiment.experiment_id,
	run_name=f"test_{MODEL_NAME}"
	)
print('experiment_id:', experiment.experiment_id)
print('model_dir:', options.model_dir)

for file_path in Path(options.model_dir).glob(f"{MODEL_NAME}_*"):
	mlflow.log_artifact(
		file_path,
		artifact_path="model_set_up"
		)
# log conda.yaml
for file_path in Path(__file__).parent.parent.glob(f"*.yaml"):
	mlflow.log_artifact(
		file_path,
		artifact_path="env_set_up"
		)
# log env_vars.sh (for tf setup)
for file_path in Path(__file__).parent.parent.glob(f"*.sh"):
	mlflow.log_artifact(
		file_path,
		artifact_path="env_set_up"
		)

### MLFLOW ###
model_set_up = vars(options).copy()
model_set_up.pop("model_name_list", None)
model_set_up["model_name"] = MODEL_NAME
model_set_up["device_name"] = platform.node()
model_set_up['vocab_type'] = options.vocab_type
model_set_up['max_token_id_seq_len'] = options.max_token_id_seq_len
model_set_up["num_class"] = num_class
# TODO: record finetune flag, the model type, pretrained model and finetuned model pair automatically

mlflow.log_dict(
	model_set_up, 
	artifact_file=Path("model_set_up").joinpath("model_set_up.json")
	)

with open("temp_binarizer.pickle", "wb") as f:
	pickle.dump(binarizer, f)

mlflow.log_artifact(
	local_path="temp_binarizer.pickle",
	artifact_path="model_set_up"
	)

Path("temp_binarizer.pickle").unlink()

mlflow.tensorflow.autolog()
gpu_list = tf.config.list_logical_devices("GPU")
mlflow.log_param("num_gpu_used", len(gpu_list))
### MLFLOW ###

if options.transformer:
	print('Loading transformer model ...')

	training_generator = data_generator.data_generator_transformer(
		txn_dict=txn_dict,
		partition_list=partition_train,
		label_dict=label_dict,
		batch_size=batch_size,
		max_token_id_seq_len=1,  # CHANGE from model_runner.py
		shuffle=(options.dgen_shuffle or options.num_epoch > 1),
		binarizer=binarizer,
		)

	# Define the paths to the saved model and preprocessing module
	model_path = MODEL_SAVED_PATH
	# Load the saved model from disk
	model:Functional = tf.keras.models.load_model(model_path, compile=False, custom_objects={'KerasLayer':hub.KerasLayer})

	model.trainable = True

	# Fine-tune from this layer onwards
	fine_tune_at = 2
	# Freeze all the layers before the `fine_tune_at` layer
	for layer in model.layers[:fine_tune_at]:
		layer.trainable = False

	for layer in model.layers[fine_tune_at:]:
		layer.trainable = True

	model.compile(
		loss="categorical_crossentropy", # output in proba array
		optimizer=Adam(learning_rate=2e-5),
		metrics=["categorical_accuracy", Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")]
		)
else:
	model: Functional = load_model(MODEL_SAVED_PATH, compile=False, custom_objects={"_tf_keras_metric": tf.keras.metrics})
	# model = load_model(options.model_saved_path, compile=False, custom_objects={"_tf_keras_metric": tf.keras.metrics})
	# Log the trained model to MLflow
	mlflow.keras.log_model(model, "model")

	model.compile(
		loss="categorical_crossentropy",  # output in proba array
		optimizer=RMSprop(learning_rate=0.001),
		metrics=["categorical_accuracy", Precision(), Recall(),
				F1Score(num_classes=num_class, average="micro", name="f1_micro"),
				F1Score(num_classes=num_class, average="macro", name="f1_macro")]
		)
	model.trainable = True
	# Freeze all the layers before the `fine_tune_at` layer
	for layer in model.layers[:FINETUNE_ONWARDS]:
		layer.trainable = False

	training_generator = data_generator.data_generator_cnn_rnn(
		txn_dict=txn_dict,
		partition_list=partition_train,
		label_dict=label_dict,
		batch_size=batch_size,
		max_token_id_seq_len=options.max_token_id_seq_len,
		shuffle=(options.dgen_shuffle or options.num_epoch > 1),
		binarizer=binarizer
		)

total_epochs = options.pretrain_epoch + options.finetune_epoch
finetune_history = model.fit(
	x=training_generator,
	steps_per_epoch=ceil(len(partition_train) / batch_size),
	use_multiprocessing=True,
	workers=multiprocessing.cpu_count(),
	epochs=total_epochs,
	initial_epoch=options.pretrain_epoch,
	)

print(model.summary())

# Log the trained model to MLflow
mlflow.keras.log_model(model, "model")

mlflow.end_run()
### MLFLOW ###

# shutil.rmtree(resource_log_dir)
del model
tf.keras.backend.clear_session()
gc.collect()
