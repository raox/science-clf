from argparse import ArgumentParser
import datetime
import vaex
from pathlib import Path

import data_helper
import var_setup

print(f"... parsing arguments {datetime.datetime.now()}")

parser = ArgumentParser()

parser.add_argument("--model_dir", type=str, help="directory of wos, e.g. '/data/model/' in mtec-cae-gpu01 ")
parser.add_argument("--compute_journal_list", nargs="+", help="journals to recompute (space between), e.g. 'ACM JEL' ")

options = parser.parse_args()

print(f"... options: {vars(options)}")

for journal in options.compute_journal_list:

	print("... transforming journal", journal)

	df_model_ready = vaex.open(str(Path(options.model_dir).joinpath(f"{journal}_{var_setup.journal_to_discipline_dict[journal]}_processed_model_ready.parquet")))

	output_id_list = [str(output_id) for output_id in df_model_ready.to_dict(column_names=["output_id"])["output_id"]]
	abstract_list = [str(abstract) for abstract in df_model_ready.to_dict(column_names=["abstract"])["abstract"]]

	output_id_to_abstract_dict = dict(zip(output_id_list, abstract_list))

	data_helper.save_lmdb_text_val(
		d=output_id_to_abstract_dict,
		save_dir=str(Path(options.model_dir).joinpath(f"{var_setup.journal_to_discipline_dict[journal]}_abstract_lmdb")),
		recompute=True
		)

	del df_model_ready
	del output_id_list, abstract_list, output_id_to_abstract_dict