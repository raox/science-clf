import datetime
import lmdb
import numpy as np
from pathlib import Path
from tqdm import tqdm
from typing import Dict, List, Tuple, Union

import data_helper, data_maker, var_setup

np.random.seed(var_setup.random_state)

def load_class(
    load_dir: str,
    model_name: str
    ) -> Dict[str, int]:
    """
    Load label dict, which should look like

        {
            "id-infk-1": 0
        }

    Input:
    - load_dir: directory that store (generated) model data (e.g. model/)
    - model_name: model name in the following format: "model" or "model-0-2"

    Output:
    - label dict for the specified model
    """

    print(f"LOADING LABEL DICT FOR MODEL {model_name} {datetime.datetime.now()}")

    file_path = Path(load_dir).joinpath(f"{model_name}_class.json")

    return data_helper.load_json(
        file_path=file_path
        )

def load_label_dict(
    load_dir: str,
    model_name: str
    ) -> Dict[str, int]:
    """
    Load label dict, which should look like

        {
            "id-infk-1": 0
        }

    Input:
    - load_dir: directory that store (generated) model data (e.g. model/)
    - model_name: model name in the following format: "model" or "model-0-2"

    Output:
    - label dict for the specified model
    """

    print(f"LOADING LABEL DICT FOR MODEL {model_name} {datetime.datetime.now()}")

    file_path = Path(load_dir).joinpath(f"{model_name}_label_dict.json")

    return data_helper.load_json(
        file_path=file_path
        )

def load_model_data(
    load_dir: str,
    model_name: str,
    split_type: str,
    ) -> List[str]:
    """
    Load model data (=list of document IDs) by split type (train / test), which look like
    
        [
            "id-infk-1"
        ]

    Input:
    - load_dir: directory that store (generated) model data (e.g. model/)
    - model_name: model name in the following format: "model" or "model-0-2"
    - split_type: either "train" or "val"

    Output:
    - train/val data (as specified) for the specified model
    """

    print(f"LOADING MODEL DATA {split_type} FOR {model_name} {datetime.datetime.now()}")

    compatibility_test_split_type(
        split_type=split_type
        )

    file_path = Path(load_dir).joinpath(f"{model_name}_id_{split_type}.json")

    return data_helper.load_json(
        file_path=file_path
        )

def compatibility_test_split_type(
    split_type: str
    ):
    """
    Test compatibility on "split type" parameter of model data
    
    Input:
    - split_type: either "train" or "val"

    Output: None (raise Error for invalid values)
    """
    print(f"... performing compatibility check on 'split type' {datetime.datetime.now()}")

    valid_split_type_list = ["train", "val"]

    if split_type not in valid_split_type_list:
        raise ValueError(f"... split type must be in valid_split_type_list")

    print("... DONE")

def load_model_list(
    load_dir: str
    ) -> List[str]:
    """
    Load model list, which look like

        [
            "model",
            "model-0"
        ]

    Input:
    - load_dir: directory that store (generated) model data (e.g. model/)

    Output:
    - list of all models
    """

    print(f"LOADING MODEL LIST {datetime.datetime.now()}")

    file_path = Path(load_dir).joinpath(f"model_list.json")

    return data_helper.load_json(
        file_path=file_path
        )

def load_vocab_list(
    load_dir: str,
    vocab_type: str,
    load_from_txt: bool = True
    ) -> List[str]:
    """
    Load vocabulary list, which look like

        [
            "the",
            "of"
        ]

    Input:
    - load_dir : directory that store vocab list (e.g. shared-wos/vocab_list)
    - vocab_type : e.g. topcount3000
    - load_from_txt : whether to load from .txt file

    Output:
    - list of vocabulary
    """
    if load_from_txt:
        return data_helper.load_txt(
            file_path=Path(load_dir).joinpath(f"{vocab_type}_vocab_list.txt")
            )

def load_word_embedding(
    load_dir: str,
    emb_type: str,
    token_size: int = None,
    emb_dim: int = None,
    load_from: str = "txt",
    recompute: bool = False
    ) -> Dict[str, np.array]:
    """
    Modularize load_word_embedding. Use for e.g. Neural Network (NN).

    Input:
    - load_dir : directory for loading word embedding (e.g. shared-wos/word_embedding)
    - emb_type : type of embedding (e.g. glove, distilbert)
    - token_size : [GLoVe-ONLY]
    - emb_dim : [GLoVe-ONLY]
    - load_from: whether to load from "txt" or "lmdb" or "sqlite"
    - recompute : re-compute or re-download embedding

    Output:
    - emb_dict : dict with word as key and its embedding as value
    """

    emb_param_dict = locals().copy()

    print(f"LOADING {emb_type} EMBEDDING {datetime.datetime.now()}")

    print(f"... parameters are {emb_param_dict}")

    data_maker.compatibility_test_word_embedding(
        emb_type=emb_type,
        token_size=token_size,
        emb_dim=emb_dim
        )

    if recompute:
        data_maker.make_word_embedding(
            save_dir=load_dir,
            emb_type=emb_type,
            token_size=token_size, 
            emb_dim=emb_dim
            )
    
    emb_param_dict.pop("recompute", None)

    emb_fn = load_emb_fn_dict[emb_type]

    emb_param_dict.pop("emb_type")

    emb_dict = emb_fn(**emb_param_dict)

    return emb_dict

def load_glove_word_embedding(
    load_dir: str,
    token_size: int,
    emb_dim: int,
    load_from: str
    ) -> Union[Dict[str, np.array]]:
    """
    Load GLoVe pre-trained context-less word embeddings. Used for e.g. Neural Network (NN).

    Input/Output: See function load_word_embedding, except
    - load_from_txt: whether to load from "txt" or "lmdb" or "sqlite" (same data)
    """

    valid_load_from_list = ["txt", "lmdb", "sqlite"]

    if load_from not in valid_load_from_list:
        raise ValueError(f"... load_from must be from valid source {valid_load_from_list}")

    file_dir = Path(load_dir).joinpath(f"glove.{token_size}B")

    if load_from == "txt":
        file_path = file_dir.joinpath(f"glove.{token_size}B.{emb_dim}d.txt")
    elif load_from == "lmdb":
        file_path = file_dir.joinpath(f"glove.{token_size}B.{emb_dim}d_lmdb")
    elif load_from == "sqlite":
        file_path = file_dir.joinpath(f"glove.{token_size}B.{emb_dim}d.sqlite")

    print(f"... importing glove embeddings from {file_path} into Python dict {datetime.datetime.now()}")

    if load_from == "txt":
        emb_dict = dict()
        with open(file_path, encoding="utf-8") as f:
            for line in tqdm(f):
                line_content_list = line.split()
                try:
                    word = line_content_list[0]
                    emb = np.asarray(line_content_list[1:], dtype="float64")
                    emb_dict[word] = emb
                except:
                    print(f"... issue at KEY {line_content_list[0]}, line skipped")

    elif load_from == "lmdb":
        emb_dict = data_helper.load_lmdb(
            folder_path=file_path
            )

    elif load_from == "sqlite":
        emb_dict = data_helper.load_sqlitedict(
            file_path=file_path
            )

    print(f"... finish loading, printing first-10 embedding for word: 'the': {data_maker.sample_word_embedding(emb_dict)[:10]}")
    return emb_dict


def load_distilbert_word_embedding(
    load_dir: str,
    recompute: bool
    ) -> Dict[str, np.array]:
    """
    Load DistilBert pre-trained contextUALIZED word embeddings. Use for e.g. Neural Network (NN).

    Input/Output: See function load_word_embedding
    """

    # TODO
    breakpoint()
    

# dict for embedding list, so that load_embedding can be modularized
# can't be put on top of file! otherwise, undefined error
load_emb_fn_dict = {
    "glove": load_glove_word_embedding,
    "distilbert": load_distilbert_word_embedding
    }