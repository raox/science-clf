import gc
import numpy as np
import os
from pathlib import Path
from typing import List
import vaex

vaex.settings.cache.memory_size_limit = "400GB"

vaex.settings.main.memory_tracker.type = "default"
vaex.settings.main.memory_tracker.max = "400GB"

try:
    dummy = os.environ["CUDA_VISIBLE_DEVICES"]
except KeyError:
    dummy = "-1"
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

from tensorflow.keras.layers import TextVectorization

import data_helper

# print(f"reading MULTI-row df")

# df_multi = vaex.open(
#     "/data/ppiriyata/all-keyword-wos/ACM-JEL-MSC_PaperFieldsOfStudy_top3000.parquet",
#     #"/home/pleum/Documents/ACM-JEL-MSC_PaperFieldsOfStudy_top3000_test.parquet",
#     progress=True
#     )

# df_multi["FieldOfStudyId"] = df_multi.FieldOfStudyId.astype("str")

# print(f"grouping by PaperId into single row")

# df_single = df_multi.groupby(
#     by="PaperId",
#     agg=[vaex.agg.list("FieldOfStudyId", dropna=True, dropnan=True, dropmissing=True)],
#     progress=True
#     )

# print(f"joining list of kw into sentence")

# # df_single["FieldOfStudyId_sentence"] = df_single.FieldOfStudyId_list.str.join(" ")
# df_single["FieldOfStudyId_sentence"] = df_single.apply(
#     lambda x: " ".join(x),
#     arguments=[df_single.FieldOfStudyId_list],
#     vectorize=False, # NEEDED !!! OTHERWISE CONFUSE
#     multiprocessing=True
#     )

# for path in [
#     "/data/ppiriyata/all-model/ACM_infk_processed_model_ready.parquet",
#     "/data/ppiriyata/all-model/JEL_econ_processed_model_ready.parquet",
#     "/data/ppiriyata/all-model/MSC_math_processed_model_ready.parquet"
#     #"/home/pleum/Documents/test-model/ACM_infk_processed_model_ready.parquet",
#     #"/home/pleum/Documents/test-model/JEL_econ_processed_model_ready.parquet"
#     ]:
#     df_discipline = vaex.open(
#         path,
#         progress=True
#         )

#     print(f"sanity check: discipline df shape {df_discipline.shape}")

#     print(df_discipline.head(1))

#     joint_df = df_discipline.join(
#         df_single,
#         on="PaperId",
#         how="inner",
#         allow_duplication=False
#         )

#     print(f"sanity check: joint df shape {joint_df.shape}")

#     print(joint_df.head(1))

#     journal_discipline =  path.split("_")[0].split("/")[-1] + "_" + path.split("_")[1]

#     joint_df[["PaperId", "output_id", "FieldOfStudyId_sentence"]].export_parquet(
#         f"/data/ppiriyata/all-keyword-wos/{journal_discipline}_PaperFieldsOfStudy_top3000_single.parquet",
#         #"/home/pleum/Documents/ACM-JEL-MSC_PaperFieldsOfStudy_top3000_test_single.parquet",
#         progress=True,
#         parallel=True
#         )

#     del df_discipline, joint_df
#     gc.collect()

# del df_multi, df_single
# gc.collect()

# skip for speed
# print(f"making paper id : kw vector")

# outputid2textvec_dict_dummy = df_single.to_dict(
#   column_names=["PaperId", "text_vector"],
#   parallel=True,
#   chunk_size=262144
#   )

# for chunk in outputid2textvec_dict_dummy:

#   chunk_dict = chunk[2]

#   keys = [str(paper_id) for paper_id in chunk_dict["PaperId"].tolist()]
#   values = chunk_dict["text_vector"].tolist()
#   outputid2textvec_dict = dict(zip(keys, values))

#   print(f"saving paper id : kw vector")
    # data_helper.save_lmdb(
    #   d=outputid2textvec_dict,
    #   save_dir=f"/data/ppiriyata/all-keyword-model/ACM-JEL-MSC_abstract_vec_lmdb/",
    #   #save_dir=f"/home/pleum/Documents/test-keyword-model/ACM-JEL-MSC_abstract_vec_lmdb/",
    #   recompute=False # since append for chunk
    #   )

"""
breakpoint()
# NOT POSSIBLE TO SAVE ARRAY! BUT CAN"T CONVERT ARRAY IDK WHY
df_single.export_parquet(
    #"/data/ppiriyata/all-wos/ACM-JEL-MSC_PaperFieldsOfStudy_top3000_single_dummy.parquet"
    "/home/pleum/Documents/ACM-JEL-MSC_PaperFieldsOfStudy_top3000_test_single.parquet"
    )
"""

print(f"loading top 3000 kw id")

top3000kwid_list = data_helper.load_txt(
    "/data/ppiriyata/shared-wos/vocab_list/topkeywordcount3000_id_list.txt"
    #"/home/pleum/Documents/shared-wos/vocab_list/topkeywordcount3000_id_list.txt"
    )

for path in [
    "/data/ppiriyata/all-keyword-wos/ACM_infk_PaperFieldsOfStudy_top3000_single.parquet",
    "/data/ppiriyata/all-keyword-wos/JEL_econ_PaperFieldsOfStudy_top3000_single.parquet",
    "/data/ppiriyata/all-keyword-wos/MSC_math_PaperFieldsOfStudy_top3000_single.parquet"
    #"/home/pleum/Documents/test-model/ACM_infk_processed_model_ready.parquet",
    #"/home/pleum/Documents/test-model/JEL_econ_processed_model_ready.parquet"
    ]:
    df_discipline_fos = vaex.open(
        path,
        progress=True
        )

    print(f"sanity check: discipline df shape {df_discipline_fos.shape}")

    print(df_discipline_fos.head(1))

    def vectorize_text(
        df_slice,
        vocab_list: List[str] = top3000kwid_list,
        output_seq_len: int = 200
        ):
        
        vectorizer = TextVectorization(
            output_sequence_length=output_seq_len
            )

        # print(f"... adding vocab list of size {len(vocab_list)} to vectorizer")

        vectorizer.set_vocabulary(vocab_list)

        arr = vectorizer(df_slice).numpy()
        
        return arr if arr.shape[0] > 0 else np.zeros(shape=(output_seq_len,), dtype="int64")


    print(f"vectorizing text via kw id")

    df_discipline_fos["vectorized_seq"] = df_discipline_fos.apply(
        vectorize_text,
        arguments=[df_discipline_fos.FieldOfStudyId_sentence],
        vectorize=True,
        multiprocessing=True # NEED TO BE FALSE IF .FOS ARRAY
        )

    print(f"sanity check: single df shape {df_discipline_fos.shape}")

    print(df_discipline_fos.head(1))

    print(f"making output id : kw vector")

    keys = df_discipline_fos[["output_id"]].to_dict()["output_id"]
    keys = [str(key) for key in keys]

    values = list(df_discipline_fos[["vectorized_seq"]].to_dict()["vectorized_seq"])

    this_db_text_vector_dict = dict(zip(keys, values))

    print(f"... saving this db")

    discipline =  path.split("_")[1]

    data_helper.save_lmdb(
        d=this_db_text_vector_dict,
        save_dir=f"/data/ppiriyata/all-keyword-model/{discipline}_abstract_vec_lmdb/",
        #save_dir=f"/home/pleum/Documents/test-keyword-model/{discipline}_abstract_vec_lmdb/",
        recompute=False # since append for chunk
        )

    del df_discipline_fos, keys, values, this_db_text_vector_dict
    gc.collect()
