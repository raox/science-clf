# python3 script/transformer_model_runner.py --model_dir /home/pleum/Documents/test-model/ --output_dir /home/pleum/Documents/test-model-transformer-output/ --model_name_list model-0 --emb_trainable

from argparse import ArgumentParser, BooleanOptionalAction
import datetime
import gc
import math
import mlflow
import multiprocessing
import numpy as np
import os
from pathlib import Path
import pickle
import platform
import random
import shutil
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from typing import Dict, List

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

import subprocess

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.metrics import CategoricalAccuracy, Precision, Recall
from tensorflow_addons.metrics import F1Score 
from tensorflow.keras.optimizers import RMSprop, Adam
import tensorflow_hub as hub
import tensorflow_text as text # need even if not used

import data_helper
import data_loader
import data_maker
import model_builder
from model_custom_exception import CustomModelException
#from model_tracker import PerfMatrics
import resource_tracker
import var_setup

np.random.seed(var_setup.random_state)
random.seed(var_setup.random_state)
tf.experimental.numpy.random.seed(var_setup.random_state)
tf.keras.utils.set_random_seed(var_setup.random_state)
tf.random.set_seed(var_setup.random_state)
tf.config.experimental.enable_op_determinism()

def acc_threshold(y_true, y_pred, threshold: float = 0.30): 
    y_pred_threshold_adjusted = (y_pred > threshold).numpy().astype("int32")
    y_true_np = y_true.numpy()
    return accuracy_score(y_true_np, y_pred_threshold_adjusted)

# parse arguments

print(f"... parsing arguments {datetime.datetime.now()}")

parser = ArgumentParser()

# parser.add_argument("--shared_dir", type=str, help="directory of shared resource with embedding, vocab_list, etc., e.g. '/data/shared-wos/' in mtec-cae-gpu01 ")
# parser.add_argument("--wos_dir", type=str, help="directory of wos, e.g. '/data/wos/' in mtec-cae-gpu01 ")
parser.add_argument("--model_dir", type=str, help="directory of wos, e.g. '/data/model/' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
parser.add_argument("--model_name_list", nargs="+", default=None, help="list of model names (space between), e.g. 'model-2-18 model-2' ")
# parser.add_argument("--model_type_list",  nargs="+", default=None, help="model type, e.g. 'dnn' ")
# parser.add_argument("--vocab_type", type=str, help="vocabulary type, e.g. 'topcount3000' ")
# parser.add_argument("--emb_type", type=str, help="embedding type, e.g. 'glove' for glove.6B.50 ")
# parser.add_argument("--emb_token_size", type=int, default=None, help="embedding token size, e.g. '6' for glove.6B.50 ")
# parser.add_argument("--emb_dim", type=int, default=None, help="embedding dimension e.g. '50' for glove.6B.50 ")
parser.add_argument("--emb_trainable", action=BooleanOptionalAction, type=bool, help="whether embedding is trainable")

# parser.add_argument("--max_token_id_seq_len", type=int, default=200, help="max. token sequence length to consider from each text")
parser.add_argument("--batch_size", type=int, default=1024, help="batch size, e.g. '1024' ")
parser.add_argument("--num_epoch", type=int, default=1, help="number of epochs, default = 1")
parser.add_argument("--dgen_shuffle", action=BooleanOptionalAction, default=True, help="whether shuffle in data generator, default = False")
# parser.add_argument("--recompute_emb", action=BooleanOptionalAction, default=False, help="whether recompute embedding components, default = False")
parser.add_argument("--recompute_journal", action=BooleanOptionalAction, default=False, help="whether recompute input components, default = False")
parser.add_argument("--compute_journal_list", nargs="+", help="journals to recompute (space between), e.g. 'ACM JEL' ")
parser.add_argument("--label_type", type=str, help="label type, whether 'single' or 'multi' ")

options = parser.parse_args()

print(f"... options: {vars(options)}")

def run_model():
    """
    Train the model
    """

    ### MLFLOW ###
    new_uri = Path(options.output_dir).joinpath("mlruns").resolve().as_uri()
    mlflow.set_tracking_uri(new_uri)

    client = mlflow.tracking.MlflowClient()

    experiment_name = f"test_{datetime.datetime.now()}"
    mlflow.set_experiment(experiment_name)

    experiment = mlflow.get_experiment_by_name(experiment_name)
    ### MLFLOW ###

    if options.recompute_journal and options.compute_journal_list is not None:
        subprocess.run([
            "python3", "script/transformer_parquet_to_lmdb.py",
            "--model_dir", options.model_dir,
            "--compute_journal_list"] + options.compute_journal_list
            )


    for model_name in options.model_name_list:

        print(f"TRAINING MODEL {model_name}: {str(datetime.datetime.now())}")

        ### MLFLOW ###
        mlflow.start_run(
            experiment_id=experiment.experiment_id,
            run_name=f"test_{model_name}"
            )

        # NOTE: .copy() requires, o.w. edit in place
        for file_path in Path(options.model_dir).glob(f"{model_name}_*"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="model_set_up"
                )

        # log conda.yaml
        for file_path in Path(__file__).parent.parent.glob(f"*.yaml"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="env_set_up"
                )

        # log env_vars.sh (for tf setup)
        for file_path in Path(__file__).parent.parent.glob(f"*.sh"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="env_set_up"
                )

        # log script/*
        # NOTE: log_artifacts also log sub-folder
        mlflow.log_artifacts(
            local_dir=str(Path(__file__).parent.resolve()), 
            artifact_path="script"
            )

        model_set_up = vars(options).copy()
        model_set_up.pop("model_name_list", None)
        model_set_up["model_name"] = model_name
        model_set_up["device_name"] = platform.node()

        pid = os.getpid()

        # resource_log_dir = Path(options.output_dir).joinpath("resource_log")
        
        # if resource_log_dir.is_dir():
        #     shutil.rmtree(resource_log_dir)

        # resource_log_dir.mkdir(
        #     mode=0o755,
        #     exist_ok=True, 
        #     parents=True
        #     )

        # overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
        #     pid=pid,
        #     model_name=model_name,
        #     save_dir=resource_log_dir
        #     )
        
        # cpu_ram_p = resource_tracker.log_cpu_ram(
        #     pid=pid,
        #     model_name=model_name,
        #     save_dir=resource_log_dir
        #     )

        ### MLFLOW ###

        lvl = model_name.count("-")

        partition_train = data_loader.load_model_data(
            load_dir=options.model_dir,
            model_name=model_name,
            split_type="train"
            )

        partition_val = data_loader.load_model_data(
            load_dir=options.model_dir,
            model_name=model_name,
            split_type="val"
            )

        input_batch_size = int(options.batch_size)

        batch_size = min(
            2**(math.floor(math.log(len(partition_train), 2))), 
            2**(math.floor(math.log(len(partition_val), 2))),
            input_batch_size
            )

        if batch_size != input_batch_size:
            print(f"... updated batch size from {input_batch_size} to {batch_size}")

        label_dict = data_loader.load_label_dict(
            load_dir=options.model_dir,
            model_name=model_name
            )

        class_list = data_loader.load_class(
            load_dir=options.model_dir,
            model_name=model_name
            )

        num_class = len(class_list)

        binarizer = MultiLabelBinarizer()
        binarizer.fit([tuple(class_list)])

        print(f"Binarizer class: {binarizer.classes_}")

        if model_name == "model":
            all_field = set([partition.split("-")[1] for partition in label_dict])
        else:
            all_field = [var_setup.coding_to_discipline_dict[model_name.split("-")[1]]]

        txn_dict = dict()

        for field in all_field:
            txn_dict[field] = data_helper.load_lmdb(
            folder_path=Path(options.model_dir).joinpath(f"{field}_abstract_lmdb") # CHANGE FROM model_runner.py
            )

        training_generator = data_generator(
            txn_dict=txn_dict,
            load_dir=options.model_dir,
            model_name=model_name,
            partition_list=partition_train,
            label_dict=label_dict,
            batch_size=batch_size,
            max_token_id_seq_len=1, # CHANGE from model_runner.py
            shuffle=(options.dgen_shuffle or options.num_epoch > 1),
            binarizer=binarizer
            )

        validation_generator = data_generator(
            txn_dict=txn_dict,
            load_dir=options.model_dir,
            model_name=model_name,
            partition_list=partition_val,
            label_dict=label_dict,
            batch_size=batch_size,
            max_token_id_seq_len=1, # CHANGE from model_runner.py
            shuffle=(options.dgen_shuffle or options.num_epoch > 1),
            binarizer=binarizer
            )

        ### build model here ### # CHANGE FROM model_runner.py

        pp_source = "https://tfhub.dev/tensorflow/bert_en_uncased_preprocess/3"
        model_source = "https://tfhub.dev/tensorflow/bert_en_uncased_L-12_H-768_A-12/4"

        input_vectorized_seq = Input(
            shape=(), 
            dtype=tf.string
            )

        pp_layer = hub.KerasLayer(pp_source)

        x = pp_layer(input_vectorized_seq)

        hub_layer = hub.KerasLayer(
            model_source, 
            trainable=False # per https://github.com/tensorflow/hub/issues/862
            )

        x = hub_layer(x)["pooled_output"]

        if options.label_type == "single":

            y = Dense(
                    num_class, 
                    activation="softmax"
                    )(x)

            model = Model(
                inputs=[input_vectorized_seq],
                outputs=[y]
                )

            """
            # Let's take a look to see how many layers are in the base model
            print("Number of layers in the model: ", len(model.layers))

            model.trainable = True

            # Fine-tune from this layer onwards
            fine_tune_at = 2

            # Freeze all the layers before the `fine_tune_at` layer
            for layer in model.layers[:fine_tune_at]:
                layer.trainable = False

            for layer in model.layers[fine_tune_at:]:
                layer.trainable = True
            """
            

            model.compile(
                loss="categorical_crossentropy", # output in proba array
                #optimizer=RMSprop(learning_rate=1e-5),
                optimizer=Adam(learning_rate=2e-5),
                metrics=["categorical_accuracy", Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")] 
                )

        elif options.label_type == "multi":

            y = Dense(
                    num_class, 
                    activation="sigmoid"
                    )(x)

            model = Model(
                inputs=[input_vectorized_seq],
                outputs=[y]
                )

            """
            # Let's take a look to see how many layers are in the base model
            print("Number of layers in the model: ", len(model.layers))

            model.trainable = True

            # Fine-tune from this layer onwards
            fine_tune_at = 2

            # Freeze all the layers before the `fine_tune_at` layer
            for layer in model.layers[:fine_tune_at]:
                layer.trainable = False

            for layer in model.layers[fine_tune_at:]:
                layer.trainable = True
            """
            

            model.compile(
                loss="binary_crossentropy", # output in proba array
                #optimizer=RMSprop(learning_rate=1e-5),
                optimizer=Adam(learning_rate=2e-5),
                metrics=["categorical_accuracy", acc_threshold, Precision(), Recall(), F1Score(num_classes=num_class, average="micro", name="f1_micro"), F1Score(num_classes=num_class, average="macro", name="f1_macro")],
                run_eagerly=True
                )

        print(model.summary())


        ### END build model here ### # CHANGE FROM model_runner.py

        # print(f"... fitting model {datetime.datetime.now()}")

        ### MLFLOW ###
        model_set_up["num_class"] = num_class

        mlflow.log_dict(
            model_set_up, 
            artifact_file=Path("model_set_up").joinpath("model_set_up.json")
            )

        with open("temp_binarizer.pickle", "wb") as f:
            pickle.dump(binarizer, f)

        mlflow.log_artifact(
            local_path="temp_binarizer.pickle",
            artifact_path="model_set_up"
            )

        Path("temp_binarizer.pickle").unlink()
        
        ### MLFLOW ###

        # if options.model_type_list[model_name.count("-")] != "test": # CHANGE from model_runner.py
        mlflow.tensorflow.autolog()
        
        model.fit(
            x=training_generator,
            validation_data=validation_generator,
            steps_per_epoch=math.ceil(len(partition_train)/batch_size), # use L1's partition, train/val-specific, BATCH_SIZE-specific : int(len(partition_L1["train"])/BATCH_SIZE)
            validation_steps=math.ceil(len(partition_val)/batch_size), # needed, o.w. hanging at end of epoch!
            use_multiprocessing=True, 
            workers=multiprocessing.cpu_count(),
            epochs=options.num_epoch, 
            verbose=1,
            #callbacks=[PerfMatrics(train=x,validation=validation_data),es]
            #???add callback
            )

        # else:
        #     breakpoint()

        options.model_name_list.remove(model_name)

        ### MLFLOW ###
        # overall_gpu_p.terminate()
        # proc_gpu_p.terminate()
        # cpu_ram_p.terminate()

        # mlflow.log_artifacts(
        #     local_dir=resource_log_dir, 
        #     artifact_path="resource_log"
        #     )

        ### MLFLOW ###

        mlflow.end_run()

        # shutil.rmtree(resource_log_dir)

        # test: model.predict(np.zeros(shape=(1, options.max_token_iq_seq_len)))

        # pid, resource_log_dir, overall_gpu_p, proc_gpu_p, cpu_ram_p, lvl, partition_train, partition_val, batch_size, label_dict, training_generator, validation_generator, model_setup, model
        del model
        tf.keras.backend.clear_session()
        gc.collect()

def data_generator(
    txn_dict: dict,
    load_dir: str,
    model_name: str,
    partition_list: List[str],
    label_dict: Dict[str, int],
    batch_size: int,
    max_token_id_seq_len: int,
    shuffle: bool,
    binarizer: MultiLabelBinarizer
    ):
    """
    Stream-load data for each batch of model training

    #TODO

    Input:
    - load_dir : 
    - model_name : 
    - partition_list : 
    - label_dict : 
    - batch_size : 
    - max_token_id_seq_len : 
    - shuffle : 
    """

    if shuffle: 
        print("... shuffling partition_list")
        random.shuffle(partition_list)

    for batch_partition_list in data_helper.list2chunks(l=partition_list, chunk_size=batch_size):
        # print("... initializing batch X and y")
        X = np.zeros(shape=(batch_size, max_token_id_seq_len), dtype=np.str_) # dtype needed, o.w. can't convert to tensor # CHANGE from model_runner.py
        # raw_y = [(-1, )] * batch_size # not used?

        X = np.array([
            data_helper.get_text_value_lmdb(txn=txn_dict[partition.split("-")[1]], # CHANGE from model_runner.py
                    key=partition,
                    dtype="int64"
                    ) 
            for partition in batch_partition_list]
            )
        y = binarizer.transform(np.array([tuple(label_dict[partition]) for partition in batch_partition_list]))

        yield X, y

if __name__ == "__main__":
    if options.model_name_list != [] and options.model_name_list is not None:
        try:
            run_model()

        except CustomModelException as e:
            print("ERROR, RESPAWN PROCESS WITH REMAINING MODELS")
            cmd_list = ["python3"] + [__file__] + data_helper.arg_dict_to_cmd_list(vars(options))
            
            for name in dir():
                if name not in ["cmd_list", "gc", "subprocess"] and not name.startswith("_"):
                    print(name)
                    try:
                        del globals()[name]
                    except KeyError:
                        pass
                    try:
                        del locals()[name]
                    except KeyError:
                        pass
            del name
                
            gc.collect()

            print(f"to run the following command: {cmd_list}")
            subprocess.run(cmd_list)

        finally:
            print("DONE")