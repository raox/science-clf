import pandas as pd

df_joint_pd_list = list()

for stem in ["ACM_infk", "JEL_econ", "MSC_math"]:
	print(stem)

	# df_with_abstract = vaex.open(f"/data/ppiriyata/all-model/{stem}_processed_model_ready.parquet", progress=True)

	df_with_abstract = pd.read_parquet(
		f"/data/ppiriyata/all-model/{stem}_processed_model_ready.parquet",
		engine="pyarrow",
		columns=["PaperId", "output_id", "abstract", "cat0_coding", "cat1_coding"]
		)

	breakpoint()

	# df_joint = df_kw.join(df_with_abstract, on="PaperId", how="inner", rprefix="dummy", allow_duplication=True)

	df_kw = pd.read_parquet(
		f"/data/ppiriyata/all-keyword-wos/{stem}_PaperFieldsOfStudy_top3000_single.parquet"
		)

	df_joint_pd_all = df_with_abstract.merge(df_kw, left_on=["PaperId", "output_id"], right_on=["PaperId", "output_id"], how="inner")

	df_joint_pd = df_joint_pd_all.groupby(by=["cat0_coding", "cat1_coding"]).sample(
		n=350, 
		random_state=12,
		replace=True
		)

	df_joint_pd.drop_duplicates(subset = ["PaperId"], inplace=True)

	print(df_joint_pd.shape)

	df_joint_pd_list.append(df_joint_pd)

	del df_with_abstract, df_kw, df_joint_pd

print("concat")

breakpoint()

df_all = pd.concat(
	df_joint_pd_list, 
	ignore_index=True
	)

df_all.to_parquet(
	"/data/ppiriyata/keyword-analysis/ACM_JEL_MSC_sample10000ea_PaperFieldsOfStudy_top3000_single_WITH_ABSTRACT.parquet", 
	engine="pyarrow",
	index=False
	)