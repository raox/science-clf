# breakpoint()
# print("DOEST NOT WORK TypeError: ('Could not serialize object of type function', '<function Map.call.<locals>.caller.<locals>.<lambda> at 0x7fb4606e4ee0>')")

# need to be in separate script, since multiprocessing MUST be at top-level
# o.w. multiprocessing.Pool cannot pickle local fn
# AttributeError: Can't pickle local object 'vectorize_text'

# to be callable from data_loader.py
from argparse import ArgumentParser

import datetime
import multiprocessing
import numpy as np
import os
import pandas as pd
from pathlib import Path
import time
from tqdm import tqdm
# import vaex
import dask
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask.distributed import Client, progress

# need to do this, as tf CUDA not compatible with multiprocessing
# add back later
try:
    dummy = os.environ["CUDA_VISIBLE_DEVICES"]
except KeyError:
    dummy = "-1"
# os.environ["CUDA_VISIBLE_DEVICES"] = "-1" (give error)
os.environ["CUDA_VISIBLE_DEVICES"] = ""

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

import tensorflow as tf
from tensorflow.keras.layers import TextVectorization

import data_helper
import data_loader
import resource_tracker
import var_setup

np.random.seed(var_setup.random_state)
tf.keras.utils.set_random_seed(var_setup.random_state)
tf.random.set_seed(var_setup.random_state)
tf.config.experimental.enable_op_determinism()

journal_to_discipline_dict = var_setup.journal_to_discipline_dict

parser = ArgumentParser()

parser.add_argument("--load_dir", type=str, help="directory to load shared resource, e.g. '/home/pleum/Documents/test-shared-wos' ")
parser.add_argument("--save_dir", type=str, help="directory to load model_ready / text vectors, e.g. '/home/pleum/Documents/test-model' ")
parser.add_argument("--journal_list", nargs="+", help="journals to recompute (space between), e.g. 'ACM JEL' ")
parser.add_argument("--vocab_type", type=str, help="vocabulary type, e.g. 'top3000count' ")
parser.add_argument("--output_seq_len", type=int, help="length of output sequence of token IDs")

parser.add_argument("--log_dir", type=str, default=None, help="log directory, if not provided: 'None' ")

options = parser.parse_args()

globals().update(vars(options))

vocab_list = data_loader.load_vocab_list(
    load_dir=Path(load_dir).joinpath("vocab_list"),
    vocab_type=vocab_type
    )

if __name__ == "__main__":
    
    client = Client(
        #processes=True
        )
    

    # thread went gg
    dask.config.set(
        scheduler="processes"
        )
    dask.config.set(
        num_workers=multiprocessing.cpu_count()
        )

    def vectorize_text(
        df_slice: pd.DataFrame,
        vocab_list,
        output_seq_len: int
        ) -> pd.DataFrame:
        """
        Add column for text vector, given column for abstract

        Input:
        - df_slice : DataFrame (slide)
        - vectorizer : TextVectorization object from Keras

        Output:
        - df_slice : modified DataFrame
        """

        vectorizer = TextVectorization(
            output_sequence_length=output_seq_len
            )

        # print(f"... adding vocab list of size {len(vocab_list)} to vectorizer")

        vectorizer.set_vocabulary(vocab_list)

        df_slice = "" if df_slice is None else df_slice

        arr = vectorizer(df_slice).numpy()
        
        return arr if arr.shape[0] > 0 else np.zeros(shape=(output_seq_len,), dtype="int64")

    print(f"MAKING TEXT VECTORS")
    print(f"..... given output sequence length of 'every' text as: {output_seq_len}")

    print(f"... lower-casing vocab")

    vocab_list = [vocab.lower() for vocab in vocab_list]

    sub_journal_to_discipline_dict = {journal: journal_to_discipline_dict[journal] for journal in journal_list}

    for (journal, discipline) in tqdm(sub_journal_to_discipline_dict.items()):
        
        print(f"... processing {journal}, {discipline} {datetime.datetime.now()}")


        load_file_path = Path(save_dir).joinpath(f"{journal}_{discipline}_processed_model_ready.parquet")

        pbar = ProgressBar()
        pbar.register()

        if options.log_dir is not None:
            start_time_load = datetime.datetime.now()

        df_new = dd.read_parquet(
            load_file_path,
            engine="pyarrow",
            columns=["PaperId", "abstract", "output_id"]
            )

        if options.log_dir is not None:
            end_time_load = datetime.datetime.now()

        num_record = df_new["PaperId"].shape[0].compute()

        ### LOGGING MODULES ###

        if options.log_dir is not None:
            print("BENCHMARKING ABSTRACT VECTOR COMPUTE TIME")

            pid = os.getpid()

            print(f"- Process ID: {pid}")


            log_file_name = f"dask_{num_record}r_{journal}_{discipline}_abstract_vec"

            # initialize LOG output dir
                
            Path(options.log_dir).mkdir(
                mode=0o755,
                exist_ok=True, 
                parents=True
                )

            # initialize resource loggers

            overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
                pid=pid,
                model_name=log_file_name,
                save_dir=options.log_dir
                )

            cpu_ram_p = resource_tracker.log_cpu_ram(
                pid=pid,
                model_name=log_file_name,
                save_dir=options.log_dir
                )

            # initialize log

            log_path = Path(options.log_dir).joinpath(f"{log_file_name}_static_log.json")

            if log_path.is_file():
                log_dict = data_helper.load_json(
                    file_path=log_path
                    )
            else:
                log_dict = dict()

            start_time = datetime.datetime.now()

        ### END LOGGING MODULES ###

        print(f"... using {multiprocessing.cpu_count()} CPUs to multi-vectorize the abstract {datetime.datetime.now()}")

        def vectorize_text_fixed(
            df_slice: pd.DataFrame
            ) -> pd.DataFrame:
            # need this instead of lambda, o.w. 'can't pickle lambda fn.' error
            return vectorize_text(df_slice, vocab_list, output_seq_len)

        df_new["vectorized_seq"] = df_new["abstract"].apply(
            vectorize_text_fixed,
            meta=("abstract", "str")
            )

        this_db_text_vector_dict = dict(zip(df_new["output_id"], df_new["vectorized_seq"]))

        ### LOGGING MODULES ###

        if options.log_dir is not None:
            end_time = datetime.datetime.now()

            # end resource loggers
            time.sleep(1)

            overall_gpu_p.terminate()
            proc_gpu_p.terminate()
            cpu_ram_p.terminate()

            log_dict["load-time-seconds"] = (end_time_load - start_time_load).total_seconds()
            log_dict["compute-time-seconds"] = (end_time - start_time).total_seconds()

            # save static resource

            data_helper.save_json(
                file=log_dict,
                file_name=log_path.name,
                file_dir=log_path.parent
                )

        ### END LOGGING MODULES ###

        print(f"... saving this db to {save_dir} {datetime.datetime.now()}")

        print(f"... sample")
        print(df_new.head(1))

        data_helper.save_lmdb(
            d=this_db_text_vector_dict,
            save_dir=Path(save_dir).joinpath(f"{discipline}_abstract_vec_lmdb"),
            recompute=False
            )

        pbar.unregister()
        
        """
        for key, value in tqdm(this_db_text_vector_dict.items()):
            data_helper.save_npy(
                file=value,
                file_name=f"{key}.npy",
                file_dir=Path(save_dir).joinpath(f"{journal}_{discipline}_abstract_vec_npy")
                )
        """

    print(f"... done")

    # add back
    os.environ["CUDA_VISIBLE_DEVICES"] = dummy