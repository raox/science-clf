from argparse import ArgumentParser, BooleanOptionalAction
import datetime
import gc
import math
import mlflow
import multiprocessing
import numpy as np
import os
from pathlib import Path
import platform
import random
import resource
from sklearn.cluster import MiniBatchKMeans
from sklearn.preprocessing import MultiLabelBinarizer
import shutil
import subprocess
from typing import Dict, List, Tuple

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # need to come before import tf
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3" # need to come before import tf

import tensorflow as tf

import data_helper
import data_loader
import data_maker
import model_builder
from model_custom_exception import CustomModelException
from model_tracker import PerfMatrics
import resource_tracker
import var_setup

np.random.seed(var_setup.random_state)
random.seed(var_setup.random_state)
tf.keras.utils.set_random_seed(var_setup.random_state)
tf.random.set_seed(var_setup.random_state)
tf.config.experimental.enable_op_determinism()

print(f"MODEL RUNNER")

print(f"... setting number of files limit")

soft_rlimit, hard_rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)

print(f"..... soft limit: {soft_rlimit}, hard limit: {hard_rlimit}")
print(f"..... setting to hard")

resource.setrlimit(resource.RLIMIT_NOFILE, (hard_rlimit, hard_rlimit))

print("... done")

### ARGUMENT PARSER ###

# NOTE: need to be outside here, in case re-spawning, s.t. we can parse arguments

print(f"... parsing arguments {datetime.datetime.now()}")

parser = ArgumentParser()

parser.add_argument("--shared_dir", type=str, help="directory of shared resource with embedding, vocab_list, etc., e.g. '/data/shared-wos/' in mtec-cae-gpu01 ")
parser.add_argument("--wos_dir", type=str, help="directory of wos, e.g. '/data/wos/' in mtec-cae-gpu01 ")
parser.add_argument("--model_dir", type=str, help="directory of wos, e.g. '/data/model/' in mtec-cae-gpu01 ")
parser.add_argument("--output_dir", type=str, help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
parser.add_argument("--model_name_list", nargs="+", default=None, help="list of model names (space between), e.g. 'model-2-18 model-2' ")
parser.add_argument("--model_type", type=str, help="model type, e.g. 'dnn' ")
parser.add_argument("--vocab_type", type=str, help="vocabulary type, e.g. 'topcount3000' ")
parser.add_argument("--emb_type", type=str, help="embedding type, e.g. 'glove' for glove.6B.50 ")
parser.add_argument("--emb_token_size", type=int, default=None, help="embedding token size, e.g. '6' for glove.6B.50 ")
parser.add_argument("--emb_dim", type=int, default=None, help="embedding dimension e.g. '50' for glove.6B.50 ")
parser.add_argument("--emb_trainable", action=BooleanOptionalAction, type=bool, help="whether embedding is trainable")

parser.add_argument("--max_token_id_seq_len", type=int, default=200, help="max. token sequence length to consider from each text")
parser.add_argument("--batch_size", type=int, default=1024, help="batch size, e.g. '1024' ")
parser.add_argument("--num_epoch", type=int, default=1, help="number of epochs, default = 1")
parser.add_argument("--dgen_shuffle", action=BooleanOptionalAction, default=False, help="whether shuffle in data generator, default = False")
parser.add_argument("--recompute_emb", action=BooleanOptionalAction, default=False, help="whether recompute embedding components, default = False")
parser.add_argument("--recompute_journal", action=BooleanOptionalAction, default=False, help="whether recompute input components, default = False")
parser.add_argument("--compute_journal_list", nargs="+", help="journals to recompute (space between), e.g. 'ACM JEL' ")

options = parser.parse_args()

print(f"... options: {vars(options)}")

def run_model():
    """
    Train the model
    """

    ### MLFLOW ###
    new_uri = Path(options.output_dir).joinpath("mlruns").resolve().as_uri()
    mlflow.set_tracking_uri(new_uri)

    client = mlflow.tracking.MlflowClient()

    experiment_name = f"test_{datetime.datetime.now()}"
    mlflow.set_experiment(experiment_name)

    experiment = mlflow.get_experiment_by_name(experiment_name)
    ### MLFLOW ###

    cached_journal_list, unseen_journal_list = data_maker.check_cached_journal(
        journal_list=options.compute_journal_list,
        save_dir=options.model_dir
        )

    to_compute_journal_list = unseen_journal_list if not options.recompute_journal else options.compute_journal_list

    if to_compute_journal_list != []:
        print(f"... using cache model data for models: {cached_journal_list}")
        print(f"... computing brand-new for models: {to_compute_journal_list}")
        
        data_maker.make_model_data(
            load_dir=options.wos_dir,
            save_dir=options.model_dir,
            journal_list=to_compute_journal_list, # change on recompute
            num_level=var_setup.num_level
            )

        data_maker.make_text_vector(
            load_dir=options.shared_dir,
            save_dir=options.model_dir,
            journal_list=to_compute_journal_list, # change on recompute
            vocab_type=options.vocab_type,
            output_seq_len=options.max_token_id_seq_len
            )

    vocab_list = data_loader.load_vocab_list(
        load_dir=Path(options.shared_dir).joinpath("vocab_list"),
        vocab_type=options.vocab_type,
        load_from_txt=True
        )
    

    emb_dict = data_loader.load_word_embedding(
        load_dir=Path(options.shared_dir).joinpath("word_embeddings"), # set from inside data_loader
        emb_type=options.emb_type,
        token_size=options.emb_token_size,
        emb_dim=options.emb_dim,
        recompute=options.recompute_emb,
        load_from="lmdb"
        )

    emb_matrix = data_maker.make_emb_matrix(
        vocab_list=vocab_list,
        emb_dict=emb_dict,
        unk_entity=var_setup.emb_type_to_unk_entity.get(options.emb_type)
        )

    del vocab_list
    del emb_dict

    gc.collect()

    if options.model_name_list is not None:
        model_list = sorted(options.model_name_list, reverse=True)
    else:
        model_list = data_loader.load_model_list(load_dir=options.model_dir)

    for model_name in model_list:
        print(f"TRAINING MODEL {model_name}: {str(datetime.datetime.now())}")

        ### MLFLOW ###
        mlflow.start_run(
            experiment_id=experiment.experiment_id,
            run_name=f"test_{model_name}"
            )

        # NOTE: .copy() requires, o.w. edit in place
        for file_path in Path(options.model_dir).glob(f"{model_name}_*"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="model_set_up"
                )

        # log conda.yaml
        for file_path in Path(__file__).parent.parent.glob(f"*.yaml"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="env_set_up"
                )

        # log env_vars.sh (for tf setup)
        for file_path in Path(__file__).parent.parent.glob(f"*.sh"):
            mlflow.log_artifact(
                file_path, 
                artifact_path="env_set_up"
                )

        # log script/*
        # NOTE: log_artifacts also log sub-folder
        mlflow.log_artifacts(
            local_dir=str(Path(__file__).parent.resolve()), 
            artifact_path="script"
            )

        model_set_up = vars(options).copy()
        model_set_up.pop("model_name_list", None)
        model_set_up["model_name"] = model_name
        model_set_up["device_name"] = platform.node()

        pid = os.getpid()

        resource_log_dir = Path(options.output_dir).joinpath("resource_log")
        
        if resource_log_dir.is_dir():
            shutil.rmtree(resource_log_dir)

        resource_log_dir.mkdir(
            mode=0o755,
            exist_ok=True, 
            parents=True
            )

        overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
            pid=pid,
            model_name=model_name,
            save_dir=resource_log_dir
            )
        
        cpu_ram_p = resource_tracker.log_cpu_ram(
            pid=pid,
            model_name=model_name,
            save_dir=resource_log_dir
            )

        ### MLFLOW ###

        lvl = model_name.count("-")

        partition_train = data_loader.load_model_data(
            load_dir=options.model_dir,
            model_name=model_name,
            split_type="train"
            )

        partition_val = data_loader.load_model_data(
            load_dir=options.model_dir,
            model_name=model_name,
            split_type="val"
            )

        input_batch_size = int(options.batch_size)

        batch_size = min(
            2**(math.floor(math.log(len(partition_train), 2))), 
            2**(math.floor(math.log(len(partition_val), 2))),
            input_batch_size
            )

        if batch_size != input_batch_size:
            print(f"... updated batch size from {input_batch_size} to {batch_size}")

        label_dict = data_loader.load_label_dict(
            load_dir=options.model_dir,
            model_name=model_name
            )

        num_class = len(
            data_loader.load_class(
                load_dir=options.model_dir,
                model_name=model_name
                )
            )
        
        binarizer = MultiLabelBinarizer()
        binarizer.fit([tuple(range(num_class))])

        print(f"Binarizer class: {binarizer.classes_}")

        training_generator = data_generator(
            load_dir=Path(options.model_dir).joinpath("abstract_vec_lmdb"),
            partition_list=partition_train,
            label_dict=label_dict,
            batch_size=batch_size,
            max_token_id_seq_len=options.max_token_id_seq_len,
            shuffle=options.dgen_shuffle,
            binarizer=binarizer
            )

        validation_generator = data_generator(
            load_dir=Path(options.model_dir).joinpath("abstract_vec_lmdb"),
            partition_list=partition_val,
            label_dict=label_dict,
            batch_size=batch_size,
            max_token_id_seq_len=options.max_token_id_seq_len,
            shuffle=options.dgen_shuffle,
            binarizer=binarizer
            )

        # START KMEANS COMMENTING #
        """
        model = model_builder.build_model(
            model_type=options.model_type,
            emb_matrix=emb_matrix,
            max_token_id_seq_len=options.max_token_id_seq_len,
            emb_trainable=options.emb_trainable,
            num_class=num_class
            )
        """

        emb_model = model_builder.build_model(
            model_type="test",
            emb_matrix=emb_matrix,
            max_token_id_seq_len=options.max_token_id_seq_len,
            emb_trainable=options.emb_trainable,
            num_class=num_class
            )

        model = MiniBatchKMeans(
            n_clusters=num_class,
            random_state=var_setup.random_state,
            batch_size=batch_size
            )

        """
        model.fit(x_train,y_train, epochs=10, 
                  callbacks=[PerfMatrics(train=(x_train,y_train),validation=(x_val,y_val)),es])
        """

        # print(f"... fitting model {datetime.datetime.now()}")

        ### MLFLOW ###
        model_set_up["num_class"] = num_class

        mlflow.log_dict(
            model_set_up, 
            artifact_file=Path("model_set_up").joinpath("model_set_up.json")
            )

        # START KMEANS COMMENTING #
        """
As both KMeans and MiniBatchKMeans optimize a non-convex objective function, their clustering is not guaranteed to be optimal for a given random init. Even further, on sparse high-dimensional data such as text vectorized using the Bag of Words approach, k-means can initialize centroids on extremely isolated data points. Those data points can stay their own centroids all along.
        mlflow.tensorflow.autolog()
        ### MLFLOW ###

        model.fit(
            x=training_generator,
            validation_data=validation_generator,
            steps_per_epoch=math.ceil(len(partition_train)/batch_size), # use L1's partition, train/val-specific, BATCH_SIZE-specific : int(len(partition_L1["train"])/BATCH_SIZE)
            validation_steps=math.ceil(len(partition_val)/batch_size), # needed, o.w. hanging at end of epoch!
            use_multiprocessing=True, 
            workers=multiprocessing.cpu_count(),
            epochs=options.num_epoch, 
            verbose=1,
            #callbacks=[PerfMatrics(train=x,validation=validation_data),es]
            #???add callback
            )
        """

        mlflow.sklearn.autolog()

        for batch in training_generator:
            batch_x = emb_model(batch[0]).numpy()
            pp_batch_x = batch_x.reshape(
                (batch_x.shape[0], -1)
                )
            print(pp_batch_x.shape)
            model.partial_fit(pp_batch_x)

        # model.predict(10*np.ones(shape=(1, 10000), dtype="float32"))


        options.model_name_list.remove(model_name)

        ### MLFLOW ###
        overall_gpu_p.terminate()
        proc_gpu_p.terminate()
        cpu_ram_p.terminate()

        mlflow.log_artifacts(
            local_dir=resource_log_dir, 
            artifact_path="resource_log"
            )

        ### MLFLOW ###

        mlflow.end_run()

        shutil.rmtree(resource_log_dir)

        breakpoint()

        # test: model.predict(np.zeros(shape=(1, options.max_token_iq_seq_len)))

        # pid, resource_log_dir, overall_gpu_p, proc_gpu_p, cpu_ram_p, lvl, partition_train, partition_val, batch_size, label_dict, training_generator, validation_generator, model_setup, model
        del model
        tf.keras.backend.clear_session()
        gc.collect()

def data_generator(
    load_dir: str,
    partition_list: List[str],
    label_dict: Dict[str, int],
    batch_size: int,
    max_token_id_seq_len: int,
    shuffle: bool,
    binarizer: MultiLabelBinarizer
    ):
    """
    Stream-load data for each batch of model training

    #TODO

    Input:
    - load_dir : 
    - partition_list : 
    - label_dict : 
    - batch_size : 
    - max_token_id_seq_len : 
    - shuffle : 
    """

    if shuffle: 
        print("... shuffling partition_list")
        random.shuffle(partition_list)

    for batch_partition_list in data_helper.list2chunks(l=partition_list, chunk_size=batch_size):
        # print("... initializing batch X and y")
        X = np.zeros(shape=(batch_size, max_token_id_seq_len), dtype=np.int64) # dtype needed, o.w. can't convert to tensor
        raw_y = [(-1, )] * batch_size

        txn = data_helper.load_lmdb(
            folder_path=load_dir
            )

        for i, partition in enumerate(batch_partition_list):
            field = partition.split("-")[1]

            try:
                X[i, :] = data_helper.get_npy_value_lmdb(
                    txn=txn,
                    key=partition,
                    dtype="int64"
                    )
            except KeyError:
                X[i, :] = data_helper.get_npy_value_lmdb(
                    txn=txn,
                    key=f"id-{field}-1",
                    dtype="int64"
                    )

            raw_y[i] = tuple(label_dict[partition])

        yield X, binarizer.transform(raw_y)

if __name__ == "__main__":
    if options.model_name_list != [] and options.model_name_list is not None:
        try:
            run_model()

        except CustomModelException as e:
            print("ERROR, RESPAWN PROCESS WITH REMAINING MODELS")
            cmd_list = ["python3"] + [__file__] + data_helper.arg_dict_to_cmd_list(vars(options))
            
            for name in dir():
                if name not in ["cmd_list", "gc", "subprocess"] and not name.startswith("_"):
                    print(name)
                    try:
                        del globals()[name]
                    except KeyError:
                        pass
                    try:
                        del locals()[name]
                    except KeyError:
                        pass
            del name
                
            gc.collect()

            print(f"to run the following command: {cmd_list}")
            subprocess.run(cmd_list)

        finally:
            print("DONE")