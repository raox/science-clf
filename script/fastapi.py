from fastapi import FastAPI
from script import inference_by_model, inference_auto_model

# APP

app = FastAPI(
	title="science-clf API",
	description="API server for making inference on scientific text"
	)

app.include_router(inference_by_model.router)
app.include_router(inference_auto_model.router)

@app.get("/")
async def root():
	return {"message": "science-clf API for scientific texts"}