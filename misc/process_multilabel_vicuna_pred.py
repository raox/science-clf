# -*- coding: utf-8 -*-
import pandas as pd

DATA_PATH: str = "/Users/tuyilei/Desktop/Science-Clf/science-clf-main/science-clf/llm/vicuna/data/multilabel/ECON_multilabel_pred_vicuna13b.csv"

# 读取CSV文件
df = pd.read_csv(DATA_PATH)

# 这些列的名字需要根据你的数据集进行修改
option_cols = ['Option1', 'Option2', 'Option3']
reason_cols = ['Reason1', 'Reason2', 'Reason3']

# 对于每一对Option和Reason列进行操作
for option, reason in zip(option_cols, reason_cols):
	# 检查'Reason'列中的关键词，并据此设置'Option'列
	df.loc[df[reason].str.contains(r'\bdisagree\b', case=False, na=False), option] = 'Disagree'
	df.loc[df[reason].str.contains(r'\bagree\b', case=False, na=False), option] = 'Agree'
	df.loc[~df[reason].str.contains(r'\bdisagree\b|\bagree\b', case=False, na=False), option] = ''

# 保存处理后的CSV文件
df.to_csv(DATA_PATH, index=False)
