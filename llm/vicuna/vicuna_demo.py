# -*- coding: utf-8 -*-
from typing import Dict, List

import pandas as pd
from transformers import AutoModelForCausalLM, AutoTokenizer

MODEL_PATH: str = "/data-nfs/yilei/llama/vicuna-7b"

tokenizer = AutoTokenizer.from_pretrained(MODEL_PATH)
model = AutoModelForCausalLM.from_pretrained(MODEL_PATH)

NOT_FOUND: int = -9999  # 字典若找不到key则输出此value
# CAT1_ENCODING_FILE_PATH: str = "/Users/tuyilei/Desktop/Science-Clf/science-clf-main/science-clf/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_FILE_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_DF = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
CAT1_NAME_TO_CODE: Dict[str, int] = dict(zip(CAT1_ENCODING_DF["cat1"], CAT1_ENCODING_DF["cat1_coding"]))
CAT1_NAMES: List[str] = list(CAT1_NAME_TO_CODE.keys())

# Test corpus
test_abstract = \
	"We introduce a distributed opportunistic scheduling (DOS) strategy, based on two pre-determined thresholds, for uplink K-cell networks with time-invariant channel coefficients. Each base station (BS) opportunistically selects a mobile station (MS) who has a large signal strength of the desired channel link among a set of MSs generating a sufficiently small interference to other BSs. Then, performance on the achievable throughput scaling law is analyzed. As our main result, it is shown that the achievable sum-rate scales as K log(SNR log N) in a high signal-to-noise ratio (SNR) regime, if the total number of users in a cell, N, scales faster than SNR K-1/1-e for a constant e∈(0,1). This result indicates that the proposed scheme achieves the multiuser diversity gain as well as the degrees-of-freedom gain even under multi-cell environments. Simulation results show that the DOS provides a better sum-rate throughput over conventional schemes."
test_cat1: str = "Industrial Organization"
input_text: str = f"\
You are a annotator who knows the economics well. I have trained a machine learning llm whose input is the abstract of a scientific article, and the predicted output is its predicted Category. I want to test the llm. Please give me some feedback. \n \
The abstract is: {test_abstract}. \n \
Candidate categories include: `{CAT1_NAMES}`. \n \
My llm-predicted category is: {test_cat1}. \n \
Do you think my llm-predicted category is correct? Please output 'Not ECON' and the reason if you think this abstract is NOT in the field of economics. If you think my llm-predicted category is correct, please output 'Agree' and your reason. If you think my llm-predicted category is incorrect, please output 'Disagree' and your reason, and which another category do you think this abstract should belong to?"

PRE_INSTRUCTION: str = f"\
You are a annotator who knows the economics well. I have trained a machine learning llm whose input is the abstract of a scientific article, and the predicted output is its predicted Category. I want to test the llm. Please give me some feedback. \n \
Candidate categories include: `{CAT1_NAMES}`. \n"
POST_INSTRUCTION: str = "\nYou have three options: Agree, Disagree, NOT ECON. Please choose only one of these options to output. \
If you think this article belongs to the field of economics and the category predicted by the llm is correct, please output 'Agree' and give reasons; \
If you think this article belongs to the field of economics but the category predicted by the llm is incorrect, please output 'Disagree', state which category it should belong to according to your opinion, and then provide reasons; \
If you think this article does not belong to the field of economics, output 'NOT ECON', and give reasons."

input1: str = f"The abstract is: {test_abstract}. \n My llm-predicted category is: {test_cat1}."
input1 = PRE_INSTRUCTION + input1 + POST_INSTRUCTION

abstract2: str = "Laboratory studies were conducted to investigate the adsorption/desorption behaviour in alluvial soils of seleniferous and non-seleniferous regions of Punjab. Adsorption of Se by different soils from the equilibrating solutions containing 1 to 100 Œºg mL‚àí1 could be described by Langmuir and Freundlich equations for the entire range of concentration used in the study. Adsorption maximum was highest for Barwa soil (460.8 Œºg g‚àí1) from seleniferous region, whereas, binding energy was more for Gurdaspur soil (0.207 mL g‚àí1) from non-seleniferous region. Significant positive relationship existed between adsorption maximum and cation exchange capacity (r = 0.930*) as well as clay (r = 0.986**) content of soils. The amount of Se desorbed in Kel (water-soluble) was relatively higher than desorbed in KH2PO4 (exchangeable) in all the soils except Gurdaspur soil. On an average, water-soluble and exchangeable Se together constituted 78.7 to 95.6 per cent of total adsorbed Se on all the soils."
abstract2_cat: str = "Agricultural and Natural Resource Economics; Environmental and Ecological Economics"
input2: str = f"The abstract is: {abstract2}. \n My llm-predicted category is: {abstract2_cat}."
input2 = PRE_INSTRUCTION + input2 + POST_INSTRUCTION


def text_raper(text):
	return f"\nUSER: {text}\nASSISTANT:"


# input_text = text_raper(INSTRUCTION)
# input_ids = tokenizer.encode(input_text, return_tensors='pt')
# output = llm.generate(input_ids, max_length=4096, do_sample=True, temperature=0.7)
# response = tokenizer.decode(output[0], skip_special_tokens=True)

input1 = text_raper(input1)
input_ids1 = tokenizer.encode(input1, return_tensors='pt')
output1 = model.generate(input_ids1, max_length=4096, do_sample=True, temperature=0.7)
response1 = tokenizer.decode(output1[0], skip_special_tokens=True)

input2 = text_raper(input2)
input_ids2 = tokenizer.encode(input2, return_tensors='pt')
output2 = model.generate(input_ids2, max_length=4096, do_sample=True, temperature=0.7)
response2 = tokenizer.decode(output2[0], skip_special_tokens=True)

# print("\n0th Response: \n", response)
print("\n1st Response: \n", response1)
print("\n2nd Response: \n", response2)
