# -*- coding: utf-8 -*-
import argparse
import os
from typing import Dict, List

import pandas as pd
import torch
from fuzzywuzzy import process
from transformers import AutoModelForCausalLM, AutoTokenizer

# set environment variables for GPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
print(f"... setting up GPU")
# list available GPUs
gpu_list = [torch.cuda.get_device_name(i) for i in range(torch.cuda.device_count())]
print(f"... {len(gpu_list)} visible 'logical' GPUs: {gpu_list}")
# Set up GPUs for multi-GPU training
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"... using {device}")

# Load your csv data
NOT_FOUND: int = -9999  # 字典若找不到key则输出此value
CAT1_ENCODING_FILE_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_DF = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
CAT1_NAME_TO_CODE: Dict[str, int] = dict(zip(CAT1_ENCODING_DF["cat1"], CAT1_ENCODING_DF["cat1_coding"]))
CAT1_NAMES: List[str] = list(CAT1_NAME_TO_CODE.keys())
abstract_df = pd.read_csv("/data-nfs/yilei/sciclf/llm/vicuna/data/ECON_orig.csv")
# abstract_df = pd.read_csv("/data-nfs/yilei/sciclf/file_archive/ECON_half.csv", index_col=0)
LLM_PATH: str = "/data-nfs/yilei/vicuna/13B"


def load_model(model_name, device, num_gpus, load_8bit=False):
	if device == "cpu":
		kwargs = {}
	elif device == "cuda":
		kwargs = {"torch_dtype": torch.float16}
		if load_8bit:
			if num_gpus != "auto" and int(num_gpus) != 1:
				print("8-bit weights are not supported on multiple GPUs. Revert to use one GPU.")
			kwargs.update({"load_in_8bit": True, "device_map": "auto"})
		else:
			if num_gpus == "auto":
				kwargs["device_map"] = "auto"
			else:
				num_gpus = int(num_gpus)
				if num_gpus != 1:
					kwargs.update(
						{
							"device_map": "auto",
							"max_memory": {i: "13GiB" for i in range(num_gpus)},
							}
						)
	elif device == "mps":
		# Avoid bugs in mps backend by not using in-place operations.
		kwargs = {"torch_dtype": torch.float16}
	# replace_llama_attn_with_non_inplace_operations()
	else:
		raise ValueError(f"Invalid device: {device}")

	tokenizer = AutoTokenizer.from_pretrained(model_name, use_fast=False)
	model = AutoModelForCausalLM.from_pretrained(model_name, low_cpu_mem_usage=True, **kwargs)

	# calling llm.cuda() mess up weights if loading 8-bit weights
	if device == "cuda" and num_gpus == 1 and not load_8bit:
		model.to("cuda")
	elif device == "mps":
		model.to("mps")

	return model, tokenizer


# Load Vicuna
args = dict(
	model_name=LLM_PATH,
	device="cuda",
	num_gpus='1',
	load_8bit=True,
	conv_template='vicuna_v1.1',
	temperature=0.7,
	max_new_tokens=512,
	debug=False
	)
args = argparse.Namespace(**args)
vicuna, tokenizer = load_model(args.model_name, args.device, args.num_gpus, args.load_8bit)

# LLM instructions
PRE_INSTRUCTION: str = f"\
I have trained a machine learning llm whose input is the abstract of a scientific article, and the predicted output is its predicted category. \n \
Candidate categories include: `{CAT1_NAMES}`. \n"
POST_INSTRUCTION: str = "\nYou have three options: 'Agree, Disagree, NOT ECON'. \
Please choose ONLY ONE to output. \
If you think this article belongs to the field of economics and the category predicted by the llm is correct, please output 'Agree' and give reasons; \
Otherwise, If you think this article belongs to the field of economics but the category predicted by the llm is incorrect, please output 'Disagree', state which category it should belong to according to your opinion, and then provide reasons; \
Otherwise, If you think this article does not belong to the field of economics, output 'NOT ECON', and give reasons."


def text_raper(text):
	return f"\nUSER: {text}\nASSISTANT:"


def find_predicted_cat(reason, model_pred_cat):
	matches = process.extract(reason, CAT1_NAMES, limit=2)
	for match, score in matches:
		if match != model_pred_cat and score > 80:  # adjust threshold as needed
			return match
	return None


def get_model_output(row):
	# Extract row index, abstract and category from the row
	idx = row.name
	abstract = row['abstract']
	cat1 = row['cat1']

	# If this row has already been processed, skip it
	if pd.notna(abstract_df.loc[idx, 'Option']):
		return

	# Combine the abstract and category into a single string
	input_text = f"The abstract is: {abstract}. \n My llm-predicted category is: {cat1}."
	input_text = PRE_INSTRUCTION + input_text + POST_INSTRUCTION
	input_text = text_raper(input_text)
	input_ids = tokenizer.encode(input_text, return_tensors='pt')
	input_ids = input_ids.to(device)  # move input to the GPU
	output = vicuna.generate(input_ids, max_length=100000, do_sample=True, temperature=0.7)
	output_text = tokenizer.decode(output[0], skip_special_tokens=True)
	print(output_text)

	# Split the output text by 'ASSISTANT: ' and take the second part
	assistant_output = output_text.split('ASSISTANT:')[1]

	# Find keyword and reason in the assistant output
	option = ''
	reason = ''
	vicuna_pred_cat = ''
	for word in ['Agree', 'Disagree', 'NOT ECON']:
		if word in assistant_output:
			option = word
			reason = assistant_output.replace('\n\n', '\n')
			break
		if option == 'Disagree':
			# If the keyword is 'Disagree', find the new category in the reason
			vicuna_pred_cat = find_predicted_cat(reason, cat1)

	# Add the new data to the DataFrame
	abstract_df.loc[idx, 'Option'] = option
	abstract_df.loc[idx, 'Reason'] = reason
	abstract_df.loc[idx, 'Vicuna_Pred_Cat'] = vicuna_pred_cat

	# Save the updated DataFrame to a CSV file after each row is processed
	# abstract_df.to_csv("/data-nfs/yilei/sciclf/File_Archive/ECON_vicuna_zero_shot_half.csv", index=False)
	abstract_df.to_csv("/data-nfs/yilei/sciclf/llm/vicuna/data/ECON_pred_vicuna13b.csv", index=False)


# Apply the function to each row
abstract_df.apply(get_model_output, axis=1)
