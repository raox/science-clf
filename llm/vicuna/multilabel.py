# -*- coding: utf-8 -*-
import argparse
import os
from typing import Dict, List

import pandas as pd
import torch
from fuzzywuzzy import fuzz
from transformers import AutoModelForCausalLM, AutoTokenizer

# set environment variables for GPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
print(f"... setting up GPU")
# list available GPUs
gpu_list = [torch.cuda.get_device_name(i) for i in range(torch.cuda.device_count())]
print(f"... {len(gpu_list)} visible 'logical' GPUs: {gpu_list}")
# Set up GPUs for multi-GPU training
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"... using {device}")

# Load your csv data
NOT_FOUND: int = -9999  # 字典若找不到key则输出此value
CAT1_ENCODING_FILE_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_DF = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
CAT1_NAME_TO_CODE: Dict[str, int] = dict(zip(CAT1_ENCODING_DF["cat1"], CAT1_ENCODING_DF["cat1_coding"]))
CAT1_NAMES: List[str] = list(CAT1_NAME_TO_CODE.keys())
DATA_PATH: str = "/data-nfs/yilei/sciclf/llm/vicuna/data/multilabel/ECON_multilabel_pred_vicuna7b.csv"
LLM_PATH: str = "/data-nfs/yilei/vicuna/7B"

data = pd.read_csv(DATA_PATH)


def load_model(model_name, device, num_gpus, load_8bit=False):
	if device == "cpu":
		kwargs = {}
	elif device == "cuda":
		kwargs = {"torch_dtype": torch.float16}
		if load_8bit:
			if num_gpus != "auto" and int(num_gpus) != 1:
				print("8-bit weights are not supported on multiple GPUs. Revert to use one GPU.")
			kwargs.update({"load_in_8bit": True, "device_map": "auto"})
		else:
			if num_gpus == "auto":
				kwargs["device_map"] = "auto"
			else:
				num_gpus = int(num_gpus)
				if num_gpus != 1:
					kwargs.update(
						{
							"device_map": "auto",
							"max_memory": {i: "13GiB" for i in range(num_gpus)},
							}
						)
	elif device == "mps":
		# Avoid bugs in mps backend by not using in-place operations.
		kwargs = {"torch_dtype": torch.float16}
	# replace_llama_attn_with_non_inplace_operations()
	else:
		raise ValueError(f"Invalid device: {device}")

	tokenizer = AutoTokenizer.from_pretrained(model_name, use_fast=False)
	model = AutoModelForCausalLM.from_pretrained(model_name, low_cpu_mem_usage=True, **kwargs)

	# calling llm.cuda() mess up weights if loading 8-bit weights
	if device == "cuda" and num_gpus == 1 and not load_8bit:
		model.to("cuda")
	elif device == "mps":
		model.to("mps")

	return model, tokenizer


# Load Vicuna
args = dict(
	model_name=LLM_PATH,
	device="cuda",
	num_gpus='1',
	load_8bit=True,
	conv_template='vicuna_v1.1',
	temperature=0.7,
	max_new_tokens=512,
	debug=False
	)
args = argparse.Namespace(**args)
vicuna, tokenizer = load_model(args.model_name, args.device, args.num_gpus, args.load_8bit)

# LLM instructions
PRE_INSTRUCTION: str = f"\
I have trained a machine learning llm whose input is the abstract of a scientific article, and the predicted output is its predicted categories (up to 3). \n \
Candidate categories include: `{CAT1_NAMES}`. \n"


def text_raper(text: str) -> str:
	return f"\nUSER: {text}\nASSISTANT:"


def get_llm_output(input_str: str) -> str:
	input_str = text_raper(input_str)
	input_ids = tokenizer.encode(input_str, return_tensors='pt')
	input_ids = input_ids.to(device)  # move input to the GPU
	output = vicuna.generate(input_ids, max_length=100000, do_sample=True, temperature=0.7)
	output_text = tokenizer.decode(output[0], skip_special_tokens=True)
	print(output_text)
	assistant_output = output_text.split('ASSISTANT:')[1]
	return assistant_output


def find_categories(output_other_cat, threshold=80):
	# 初始化一个空列表来存放找到的类别
	found_cats = []

	# 遍历CAT1_NAMES中的每一个值
	for cat in CAT1_NAMES:
		# 如果cat和output_other_cat的相似度超过了设定的阈值，则认为匹配成功
		if fuzz.token_set_ratio(cat, output_other_cat) > threshold:
			found_cats.append(cat)

	# 使用join()函数将找到的类别合并为一个由逗号分隔的字符串
	other_cat = ', '.join(found_cats)

	return other_cat


def update_llm_pred(row):
	# Extract row index, abstract and category from the row
	idx = row.name
	abstract = row['abstract']
	cat1 = ""
	cat2 = ""
	cat3 = ""

	# If this row has already been processed, skip it
	if pd.notna(data.loc[idx, 'Econ']):
		return

	## Econ or not
	input0: str = f"\n The abstract is: \"{abstract}\". \n Do you think this abstract belongs to the field of Economics? \
	If so, output 'Yes' and the reason; if not, output 'No' and the reason."
	input0 = PRE_INSTRUCTION + input0
	output0 = get_llm_output(input0)
	econ: str = ""
	if "yes" in output0.lower():
		econ = "Yes"
	elif "no" in output0.lower():
		econ = "No"
	else:
		pass
	data.loc[idx, 'Econ'] = econ

	## Cat1
	if econ == "Yes":
		cat1 = row['cat1_0']
		input1: str = f"The abstract is: \"{abstract}\" \n Do you think this abstract belongs to the first model-predicted category '{cat1}'? If you agree with the first model-predicted category, please output 'Agree' and the reason; if you do not agree with the first model-predicted category, please output 'Disagree' and the reason."
		output1 = get_llm_output(input1)
		option1 = ''
		reason1 = ''
		for word in ['Agree', 'Disagree']:
			if word in output1:
				option1 = word
				reason1 = output1.replace('\n\n', '\n')
				break
		data.loc[idx, 'Option1'] = option1
		data.loc[idx, 'Reason1'] = reason1

	## Cat2
	if econ == "Yes" and (not pd.isnull(row['cat1_1'])):
		cat2 = row['cat1_1']
		input2: str = f"The abstract is: \"{abstract}\" \n Do you think this abstract belongs to the second model-predicted category '{cat2}'? If you agree with the second model-predicted category, please output 'Agree' and the reason; if you do not agree with the second model-predicted category, please output 'Disagree' and the reason."
		output2 = get_llm_output(input2)
		option2 = ''
		reason2 = ''
		for word in ['Agree', 'Disagree']:
			if word in output2:
				option2 = word
				reason2 = output2.replace('\n\n', '\n')
				break
		data.loc[idx, 'Option2'] = option2
		data.loc[idx, 'Reason2'] = reason2

	## Cat3
	if econ == "Yes" and (not pd.isnull(row['cat1_2'])):
		cat3 = row['cat1_2']
		input3: str = f"The abstract is: \"{abstract}\" \n Do you think this abstract belongs to the third model-predicted category '{cat3}'? If you agree with the third model-predicted category, please output 'Agree' and the reason; if you do not agree with the third model-predicted category, please output 'Disagree' and the reason."
		output3 = get_llm_output(input3)
		option3 = ''
		reason3 = ''
		for word in ['Agree', 'Disagree']:
			if word in output3:
				option3 = word
				reason3 = output3.replace('\n\n', '\n')
				break
		data.loc[idx, 'Option3'] = option3
		data.loc[idx, 'Reason3'] = reason3

	## Other categories
	if econ == "Yes":
		input_other_cat: str = f"Are there any other categories that you think are more suitable for this abstract, besides {cat1} {cat2} {cat3}? \
		If so, please output some other categories among candidate categories {CAT1_NAMES} and the reasons. If not, please output the reason why not."
		output_other_cat = get_llm_output(input_other_cat)
		other_cat = find_categories(output_other_cat)
		data.loc[idx, 'Other_cat'] = other_cat
		data.loc[idx, 'Reason_other_cat'] = output_other_cat.replace('\n\n', '\n')

	# Save the updated DataFrame to a CSV file after each row is processed
	data.to_csv(DATA_PATH, index=False)


columns_to_add = ['Econ', 'Option1', 'Reason1', 'Option2', 'Reason2', 'Option3', 'Reason3', 'Other_cat',
                  'Reason_other_cat']
for col in columns_to_add:
	if col not in data.columns:
		data[col] = None

# Apply the function to each row
data.apply(update_llm_pred, axis=1)
