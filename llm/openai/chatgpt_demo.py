# -*- coding: utf-8 -*-
from typing import Dict, List

import openai
import pandas as pd

NOT_FOUND: int = -9999  # 字典若找不到key则输出此value
CAT1_ENCODING_FILE_PATH: str = "/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_DF = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
CAT1_NAME_TO_CODE: Dict[str, int] = dict(zip(CAT1_ENCODING_DF["cat1"], CAT1_ENCODING_DF["cat1_coding"]))
CAT1_NAMES: List[str] = list(CAT1_NAME_TO_CODE.keys())

# Test corpus
test_abstract = \
	"Abstract Several investigations indicate that the Bidirectional Reflectance Distribution Function (BRDF) contains information that can be used to complement spectral information for improved land cover classification accuracies. Prior studies on the addition of BRDF information to improve land cover classifications have been conducted primarily at local or regional scales. Thus, the potential benefits of adding BRDF information to improve global to continental scale land cover classification have not yet been explored. Here we examine the impact of multidirectional global scale data from the first Polarization and Directionality of Earth Reflectances (POLDER) spacecraft instrument flown on the Advanced Earth Observing Satellite (ADEOS-1) platform on overall classification accuracy and per-class accuracies for 15 land cover categories specified by the International Geosphere Biosphere Programme (IGBP). A set of 36,648 global training pixels (7 × 6 km spatial resolution) was used with a decision tree classifier to evaluate the performance of classifying POLDER data with and without the inclusion of BRDF information. BRDF ‘metrics’ for the eight-month POLDER on ADEOS-1 archive (10/1996–06/1997) were developed that describe the temporal evolution of the BRDF as captured by a semi-empirical BRDF llm. The concept of BRDF ‘feature space’ is introduced and used to explore and exploit the bidirectional information content. The C5.0 decision tree classifier was applied with a boosting option, with the temporal metrics for spectral albedo as input for a first test, and with spectral albedo and BRDF metrics for a second test. Results were evaluated against 20 random subsets of the training data. Examination of the BRDF feature space indicates that coarse scale BRDF coefficients from POLDER provide information on land cover that is different from the spectral and temporal information of the imagery. The contribution of BRDF information to reducing classification errors is also demonstrated: the addition of BRDF metrics reduces the mean, overall classification error rates by 3.15% (from 18.1% to 14.95% error) with larger improvements for producer's accuracies of individual classes such as Grasslands (+ 8.71%), Urban areas (+ 8.02%), and Wetlands (+ 7.82%). User's accuracies for the Urban (+ 7.42%) and Evergreen Broadleaf Forest (+ 6.70%) classes are also increased. The methodology and results are widely applicable to current multidirectional satellite data from the Multi-angle Imaging Spectroradiometer (MISR), and to the next generation of POLDER-like multi-directional instruments."
test_cat1: str = "Agricultural and Natural Resource Economics; Environmental and Ecological Economics"

# ChatGPT
# openai.api_key = os.getenv("OPENAI_API_KEY")
openai.api_key = "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"
response = openai.ChatCompletion.create(
	model="gpt-3.5-turbo",
	messages=[
		{"role": "system", "content": "You are a annotator who knows the economics well."},
		{"role": "user", "content": "You are a annotator who knows the economics well. I have trained a machine learning llm whose input is the abstract of a scientific article, and the predicted output is its predicted Category. I want to test the llm. Please give me some feedback."},
		{"role": "user", "content": f"The abstract is: {test_abstract}"},
		{"role": "user", "content": f"Candidate categories include: `{CAT1_NAMES}`"},
		{"role": "user", "content": f"My llm-predicted category is: {test_cat1}"},
		{"role": "user", "content": "Do you think my llm-predicted category is correct? Please output 'Not ECON' if you think this abstract is NOT in the field of economics. If you think my llm-predicted category is correct, please output 'Agree'. f you think my llm-predicted category is incorrect, please output 'Disagree'. If you disagree, which category do you think this abstract should belong to?"},
		]
	)
print(response)
