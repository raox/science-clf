## Annotation Guidelines for Publications in Economics


1. Click on the link we shared with you via email invitation.
2. Register with your preferred email address, which does not have to be your ETHZ address.
3. Notify [Susie](mailto:srao@ethz.ch), [Prakhar](pbhandari@student.ethz.ch) or [Yilei](mailto:yileitu@student.ethz.ch) when you have registered and we will assign you as an annotator in our project.
4. Once you are approved as an annotator in our project, you can go into the project of **`ECON` (single-label)**, **`ECON-multilabel` (multi-label)**  and will see an Excel alike worksheet.
5. You can start annotating instance by instance, see the video as reference. Video link: https://youtu.be/Gqqsp_-ALfs
6. You can also filter by `cat1` and type in keywords (optional) such as `business` or `international` to pick up your favorite category to annotate.
7. We have prepared about 700 instances and would like to have at least 3 annotations per instance. The system will automatically compute the agreement score among the annotators.
8. Your concrete annotation tasks:
   - **Must do**: Based on the `abstract` and `keywords`, whether you agree with the label or not. If not, please pick a new label. If you think this abstract belongs to the field of economics, you could choose `agree` or `disagree` with the predicted category. If you think this abstract does NOT belong to the field of economics, please choose `Not ECON`.
   - Optional/Nice to have: Please indicate the `keywords` you will mark. You hit the green `keyword` and then click on the word(s) of your choice.
   - Optional/Nice to have: Please help removing noises from the given keywords. You hit the red `Unqualified keyword (to remove)` and then click on the word(s) of your choice.
   - Do not forget to hit the `Submit` button on the upper right once you are done with one annotation task.
   - Please consult our video or contact Susie/Yilei for further assistance you need.

Thank you so much for participating in the annotation task!

FYI, there are 19 categories in ECON dataset and you can consult the official [JEL classification](https://www.aeaweb.org/econlit/jelCodes.php?view=jel) when deciding the annotation.

```
Agricultural and Natural Resource Economics; Environmental and Ecological Economics
International Economics
Business Administration and Business Economics; Marketing; Accounting; Personnel Economics
Industrial Organization
(Political Economy and Comparative) Economic Systems
Mathematical and Quantitative Methods
History of Economic Thought, Methodology, and Heterodox Approaches
Macroeconomics and Monetary Economics
Public Economics
Labor and Demographic Economics
Law and Economics
Microeconomics
Economic Development, Innovation, Technological Change, and Growth
General Economics and Teaching
Financial Economics
Health, Education, and Welfare
Economic History
Urban, Rural, Regional, Real Estate, and Transportation Economics
Other Special Topics
```
