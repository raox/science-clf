# -*- coding: utf-8 -*-
import random
from collections import Counter
from typing import Dict, List

import pandas as pd

from script import data_helper

random.seed(42)

CAT0: str = "ECON"
LABELED_DATA_PATH = "/data-nfs/yilei/sciclf/labelstudio/labeled_data/labelstudio_econ.json"
LABELED_DICT_PATH = "./labeled_data"
MODEL_NAME: str = "model-1"
NOT_FOUND: int = -9999  # 字典若找不到key则输出此value

CAT1_ENCODING_FILE_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data/ECON_cat1_encoding.csv"
CAT1_ENCODING_DF = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
CAT1_NAME_TO_CODE: Dict[str, int] = dict(zip(CAT1_ENCODING_DF["cat1"], CAT1_ENCODING_DF["cat1_coding"]))


# NEW_CAT1 = ["Financial Economics", "Health, Education, and Welfare", "Economic History",
#             "(Political Economy and Comparative) Economic Systems",
#             "Urban, Rural, Regional, Real Estate, and Transportation Economics"]  # ECON新加入的categories
# idx = len(CAT1_NAME_TO_CODE)
# for new_cat in NEW_CAT1:
# 	CAT1_NAME_TO_CODE[new_cat] = idx
# 	idx += 1


def cat1_name_to_code(cat1_name: str) -> int:
	return CAT1_NAME_TO_CODE.get(cat1_name, NOT_FOUND)


_output_id_df = pd.read_parquet(
	'/data-nfs/wos/wos/JEL-model/JEL_econ_processed_model_ready.parquet', columns=["PaperId", "output_id"]
	)
PAPER_ID_TO_OUTPUT_ID: Dict[int, int] = dict(zip(_output_id_df["PaperId"], _output_id_df["output_id"]))


def paper_id_to_output_id(paper_id: int) -> str:
	return PAPER_ID_TO_OUTPUT_ID.get(paper_id, NOT_FOUND)


def delete_unreadable_str(s: str) -> str:
	if " &bull" in s:
		s = s.replace(" &bull", "")
	return s


labeled_data = pd.read_json(LABELED_DATA_PATH)
labeled_data["cat1"] = labeled_data["cat1"].apply(delete_unreadable_str).apply(cat1_name_to_code)
paper_ids = labeled_data["PaperId"].unique()
label_dict: Dict[str, List[int]] = {}

total_cnt = len(paper_ids)
econ_cnt: int = 0  # 是ECON的个数
label_preserved_cnt: int = 0  # 保持原本label的样本数
label_renewed_cnt: int = 0  # 更改原本label的样本数
tied_cnt: int = 0  #
sample_cnt: int = 0

for paper_id in paper_ids:
	data_per_paper = labeled_data[labeled_data["PaperId"] == paper_id]
	sentiments: List[str] = data_per_paper["sentiment"].values.tolist()
	# 删除所有Not Econ的数据
	if "Not ECON" not in data_per_paper["sentiment"].values:
		econ_cnt += 1
		output_id = paper_id_to_output_id(paper_id)
		sentiments_cnt = Counter(sentiments)
		agree_cnt: int = sentiments_cnt["Agree"]
		disagree_cnt: int = sentiments_cnt["Disagree"]
		if not (agree_cnt == 0 and disagree_cnt == 0):
			if agree_cnt > disagree_cnt:
				# 若同意个数大于不同意个数，仍然维持原有cat
				label_dict[output_id] = [int(data_per_paper["cat1"].values.tolist()[0])]
				label_preserved_cnt += 1
				sample_cnt += 1
			else:
				# 否则
				sentiments2: List[str] = data_per_paper["sentiment2"].values.tolist()
				sentiments2 = [a_ for a_ in sentiments2 if a_ == a_]  # 删除nan，利用性质np.nan == np.nan的值为False
				# sentiment2_mode = max(sentiments2, key=sentiments2.count)  # 取新cat中最多的，平局情况随机取一个
				sentiments2_cnts = Counter(sentiments2)
				sentiments2_maximums = [x for x in sentiments2_cnts if
				                        sentiments2_cnts[x] == sentiments2_cnts.most_common(1)[0][1]]
				if len(sentiments2_maximums) >= 2:
					tied_cnt += 1
				if len(sentiments2_maximums) > 0:
					sample_cnt += 1
					label_renewed_cnt += 1
					sentiment2_mode = random.choice(sentiments2_maximums)
					print(sentiment2_mode)
					cat1_code = cat1_name_to_code(sentiment2_mode)
					if cat1_code != NOT_FOUND:
						label_dict[output_id] = [cat1_code]  # 暂时不加入5个新的cat1

print("Total:", total_cnt)
print("Econ:", econ_cnt)
print("Preserved:", label_preserved_cnt)
print("Renewed:", label_renewed_cnt)
print("Tied:", tied_cnt)
print("Sample:", sample_cnt)

train_ids = list(label_dict.keys())
data_helper.save_json(file=label_dict, file_name=f"{MODEL_NAME}_label_dict.json", file_dir=LABELED_DICT_PATH)
data_helper.save_json(file=train_ids, file_name=f"{MODEL_NAME}_id_train.json", file_dir=LABELED_DICT_PATH)
