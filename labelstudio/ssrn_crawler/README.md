# SSRN Abstract Scraper
Scrapes SSRN Abstracts: first by searching for abstract urls (by JEL code), then by downloading contents of those urls

**!!! Restricted to the first 10 pages of each JEL Code (500 papers per code #tba) !!!**


## Process Summary
1. `url_collector.py` - scrape abstract url's for a given JEL code
   (Contribution from this [repo](https://github.com/talsan/ssrn))
2. `abstract_downloader_dummy.py` - download the content of each abstract url (dummy)
   (Contribution from this [repo](https://github.com/karthiktadepalli1/ssrn-scraper/blob/master/scraper.py))
3. `abstract_downloader.py` - download the content of each abstract url from each JEL category 
4. `sample_reader.py`- generate testing samples for each category


## Data Files
1. `url`: output of `url_collector.py`, in json, `/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract_urls`
2. `paper_details` (incl. abstracts): output of `abstract_downloader.py`, in json, `/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract/{$JEL_CODE}`
3. `only abstracts for testing`: output of `sample_reader.py`, in csv (pandas generated), `/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract/abstract_cat{$JEL_CODE_INITIAL}.csv`


## SSRN JEL Codes
https://papers.ssrn.com/sol3/displayjel.cfm


## Example of Log (Printed to Screen)
```
processing G00
-------------------------
requested https://papers.ssrn.com/sol3/JELJOUR_Results.cfm?npage=1&form_name=Jel&code=G00&lim=false
post-request-sleeping for 6 seconds ... 
jel_code=G00 | 50 total pages
jel_code=G00 | page=1 | 50 papers
saved json jel=G00__page=1__date=20220301

requested https://papers.ssrn.com/sol3/JELJOUR_Results.cfm?npage=2&form_name=Jel&code=G00&lim=false
post-request-sleeping for 2 seconds ... 
jel_code=G00 | page=2 | 50 papers
saved json jel=G00__page=2__date=20220301
```
