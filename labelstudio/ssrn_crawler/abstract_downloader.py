import requests
from ordered_set import OrderedSet
from bs4 import BeautifulSoup
from multiprocessing import Pool
from multiprocessing import cpu_count
import time
import sys
import glob
import os
import json
import config
import random

def quickSoup(url):
    try:
        header = {}
        header['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
        # page = requests.get(url, headers=header, timeout=10)
        soup = BeautifulSoup(requests.get(url, headers=header, timeout=10).content, 'html.parser')
        return(soup)
    except Exception:
        return(None)


def getPaper(url):
    print('Start processing the article')
    try:

        article = quickSoup(url)
        t = article.get_text()
        if "The abstract you requested was not found" in t:
            return("{},".format(url))
        title = article.find('h1').get_text().replace("\n", "")
        test_list = OrderedSet(t.split("\n"))
        authors = test_list[0].replace(title, "").replace(" :: SSRN", "").replace(" by ", "").replace(", ", ":")
        date = [line.replace("Last revised: ", "") for line in test_list if "Last revised: " in line]
        if date == []:
            date = [line.replace("Posted: ", "") for line in test_list if "Last revised: " in line]
        date = date[0]
        text = t.split("Abstract\n")[1]
        

        # TODO: The current abstract scraper is not ideal as it only generates the abstract and clf by string splitting. 
        # Only works for abstracts and clf codes.
        # TODO: However, the keywords field is split by "Suggested Citations", which makes string splitting useless. Can use selenium as a fix. 
        abstract = "\"{}\"".format(text.split("Suggested Citation:")[0].replace("\n", ""))
        abstract = "\"{}\"".format(abstract.split("Keywords:")[0].replace("\n", ""))
        clf = "\"{}\"".format(text.split("JEL Classification:")[1].replace("\n", ""))
        clf = "{}\"".format(clf.split("Suggested Citation:")[0].replace("\n", ""))

        print('abstract:', abstract, '\n ----', 'clf:', clf)


        # get paper statistics
        stats = OrderedSet(article.find('div', attrs = {'class': 'box-paper-statics'}).get_text().split("\n"))
        views, dl, rank, refs = "", "", "", ""
        try:
            views = stats[stats.index('Abstract Views') + 1].strip().replace(",", "")
        except:
            pass
        try:
            dl = stats[stats.index('Downloads') + 1].strip().replace(",", "")
        except:
            pass
        try:
            rank = stats[stats.index('rank') + 1].strip().replace(",", "")
        except:
            pass
        try:
            refs = stats[stats.index('References') + 1].strip().replace(",", "")
        except:
            pass
        # results = [url, "\"{}\"".format(title), abstract, clf, authors, date, views, dl, rank, refs]
        # return(",".join(results))

        # Fixed: csv is not ideal due to newline characters within the abstract, changed to json instead
        results = {'url': url, 'title': "\"{}\"".format(title), 'abstract': abstract, 'clf': clf, 'authors': authors, 'date': date, 'views': views, 'dl': dl, 'rank': rank, 'refs': refs}
        return(results)
    except:
        return({'url': url, 'title': None, 'abstract': None, 'clf': None, 'authors': None, 'date': None, 'views': None, 'dl': None, 'rank': None, 'refs': None})


def dummyscrape(start, stop):
    numWorkers = cpu_count() * 12
    print(numWorkers)
    p = Pool(numWorkers)
    linkList = ["https://papers.ssrn.com/sol3/papers.cfm?abstract_id=" + str(x) for x in range(start, stop)]

    papers = p.map(getPaper, linkList)
    p.terminate()
    p.join()
    # writeString = "\n".join(papers)
    # with open('test.csv', 'w+') as f:
    #     f.write(writeString)

    with open(f'ssrn-links.json','w') as f:
        json.dumps(papers, indent=4)
        print(f'ssrn-links.json\n')
        file_counter += 1

def scrape(clf, start, stop):
    numWorkers = cpu_count() * 2 # changed from 12 to 2
    p = Pool(numWorkers)
    linkList = ["https://papers.ssrn.com/sol3/papers.cfm?abstract_id={}".format(str(x)) for x in range(start, stop)]

    papers = p.map(getPaper, linkList)
    print('papers:', papers)
    p.terminate()
    p.join()
    # writeString = "\n".join(papers)
    # with open(f'./abstract/{clf}/ssrn-links.csv', 'a') as f:
    #     print('Writing a file ...', time.ctime())
    #     f.write(writeString)

    # The best outfile strategy is to write a single json array in one file, so that we can parse it directly with pd
    with open(f'./abstract/{clf}/ssrn-links_{time.time()}.json', 'w') as f:
        print(papers)
        f.write(json.dumps(papers))
        print(f'writing a new ./abstract/{clf}/ssrn-links.json\n')


if __name__ == "__main__":
    # import cProfile
    # import scraper
    # globals().update(vars(scraper))
    # sys.modules['__main__'] = scraper
    # dummyscrape(4000, 4100)

    ### Dummy scraper for random abstract
    # breaks = [10000 * x for x in range(179, 250)]
    # t = time.time()
    # for i in range(len(breaks)-1):
    #     print(breaks[i])
    #     b = time.time()
    #     scrape(breaks[i], breaks[i+1])
    #     print("TIME FOR 1000: " + str(time.time() - b))
    #     print("TIME SINCE START: " + str(time.time() - t))

    ### Take a specific url from the {code} json file and starts downloading

    for file in glob.glob('./abstract_urls/raw/*'):
        clf = file.split('__')[0].split('=')[-1]
        print(clf)
        # if not os.path.exists(f'./abstract/{clf}/ssrn-links.json'):
        # Generate json over all the urls again
        if not os.path.exists(f'./abstract/{clf}'):
            os.mkdir(f'./abstract/{clf}')
        lines = json.load(open(file))
        
        for line in lines:
            abstract_id = line['url'].split('=')[-1]
            print(abstract_id)
            sleep_time = random.randint(*config.sleep_range)
            print(f'post-request-sleeping for {sleep_time} seconds ... ')
            time.sleep(sleep_time)
            t = time.time()
            scrape(clf, int(abstract_id) - 1, int(abstract_id) + 1)
            print("TIME SINCE START: " + str(time.time() - t))
                