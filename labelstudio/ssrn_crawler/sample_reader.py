# This script reads a random sample for the categories (Initials in UPPERCASE) of the JEL abstract. 
# JEL classification: https://papers.ssrn.com/sol3/displayjel.cfm

import pandas as pd
import glob
import string
import itertools
import json

sample_per_cat = 10

upper = list(string.ascii_uppercase)

for initial in upper:
    for dir in glob.glob('/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract/*'):
        if initial in dir:
            print(f'Working on {dir}')
            print('dir:', dir)
            final_df = pd.DataFrame()
            for file in glob.glob(f'/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract/{initial}*/ssrn-links*.json'):
                df = pd.read_json(file)
                final_df = pd.concat([final_df, df])
            print(final_df)
            abstracts = final_df['abstract'].dropna().reset_index()
            abstracts = abstracts.replace(r'\\n',' ', regex=True)

            abstracts.to_csv(f'/data-nfs/wos/labelstudio/SSRN-scraper/ssrn/abstract/abstract_cat{initial}.csv')



