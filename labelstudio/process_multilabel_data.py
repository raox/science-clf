# -*- coding: utf-8 -*-
from typing import List

import pandas as pd

CAT0: str = "ECON"
MULTILABEL_DATA_PATH: str = "/data-nfs/wos/wos/all-multilabel-wos-post-thesis/JEL_econ_processed.parquet"
CAT1_ENCODING_FILE_PATH: str = f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_cat1_encoding.csv"
STRATIFIED_RATIO: float = 2e-3
SEED: int = 42

cat1_encoding_df = pd.read_csv(CAT1_ENCODING_FILE_PATH, index_col=0)
cat1_encoding = dict(zip(cat1_encoding_df["cat1_coding"], cat1_encoding_df["cat1"]))


def code_to_cat_name(code_str: str) -> List[str]:
	codes = code_str.split(" ")
	cat1_names: List[str] = [cat1_encoding[int(code.split("-")[1])] for code in codes]
	return cat1_names


# abstract_kw_df = pd.read_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1.csv",
# 	lineterminator='\n'
# 	)
# abstract_kw_df.drop(columns=["cat1", "cat1_coding"], inplace=True)
#
# multilabel_df = pd.read_parquet(MULTILABEL_DATA_PATH)
# print("Before filter:", multilabel_df.shape)
# multilabel_df = multilabel_df[["PaperId", "cat_coding"]]
# multilabel_df = multilabel_df[multilabel_df["cat_coding"].map(len) >= 2]
# print("After filter:", multilabel_df.shape)
#
# abstract_kw_multilabel_df = pd.merge(left=multilabel_df, right=abstract_kw_df, on="PaperId")
# print("Merge:", abstract_kw_multilabel_df.shape)
# abstract_kw_multilabel_df.rename(columns={"cat_coding": "cat1_name"}, inplace=True)
# abstract_kw_multilabel_df["cat1_name"].apply(code_to_cat_name)
# abstract_kw_multilabel_df.to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_multilabel.csv",
# 	index=False,
# 	line_terminator="\r\n"
# 	)

abstract_kw_multilabel_df = pd.read_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_multilabel.csv"
	)
print(abstract_kw_multilabel_df.shape)
print(abstract_kw_multilabel_df.head(2))

stratified_multilabel_df = abstract_kw_multilabel_df.groupby('cat1_name', group_keys=False).apply(
	lambda x: x.sample(frac=STRATIFIED_RATIO, random_state=SEED)
	)
stratified_multilabel_df["cat1_name"] = stratified_multilabel_df["cat1_name"].apply(code_to_cat_name)
cat1_names_df = pd.DataFrame(stratified_multilabel_df["cat1_name"].values.tolist()).add_prefix('cat1_')
print(cat1_names_df.head(2))
stratified_multilabel_df.drop(columns=["cat1_name"], inplace=True)
print("Main df:", stratified_multilabel_df.shape)
print("Cat df:", cat1_names_df.shape)
stratified_multilabel_df.index = cat1_names_df.index
stratified_multilabel_df = pd.concat([stratified_multilabel_df, cat1_names_df], axis=1)
print("After concat:", stratified_multilabel_df.shape)
stratified_multilabel_df.to_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_multilabel_sampled.csv",
	index=False,
	line_terminator="\r\n"
	)
