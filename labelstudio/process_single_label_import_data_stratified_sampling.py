# -*- coding: utf-8 -*-
from typing import List

import pandas as pd
import vaex

CAT0: str = "BIO"
ABSTRACT_DATA_PATHS: List[str] = ["/data-nfs/wos/wos/all-wos/Bio_bio_processed.parquet"]
SAMPLE_RATIO: float = 1e-4

# CAT0: str = "CS"
# ABSTRACT_DATA_PATHS: List[str] = ["/data-nfs/wos/wos/all-wos/ACM_infk_processed.parquet"]
# SAMPLE_RATIO: float = 2e-5

SEED: int = 42
ENCODING: str = "utf-8"

# print(f"loading our pq")
# our_df = vaex.open_many(ABSTRACT_DATA_PATHS)
# print(f"... sanity check: shape = {our_df.shape}")
# print(our_df.head(1))
# print(f"fetching unique PaperIds from our pq")
# unique_paperid = our_df.PaperId.unique(dropnan=True, dropmissing=True, limit_raise=False, progress=True)
# print(f"... sanity check: shape = {len(unique_paperid)}")
# print(f"creating dummy pq for our PaperId")
# dummy_paperid_df = vaex.from_arrays(PaperId=unique_paperid)
# print(f"loading paper2kw df")
# paper2kw_df = vaex.from_csv(
# 	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/PaperFieldsOfStudy.txt",
# 	header=None,
# 	usecols=[0, 1, 2],
# 	names=["PaperId", "FieldOfStudyId", "FieldOfStudyProba"],
# 	delimiter="\t",
# 	progress=True
# 	)
# print(f"filtering only our paper2kw df")
# our_paper2kw_df = paper2kw_df.join(
# 	dummy_paperid_df,
# 	on="PaperId",
# 	how="inner",
# 	allow_duplication=True
# 	)
# print(f"... sanity check: shape after joining = {our_paper2kw_df.shape}")
# print(our_paper2kw_df.head(1))
# print(f"saving OUR paper2kw df")
# our_paper2kw_df.export_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv", progress=True
# 	)
# del our_paper2kw_df

print(f"loading kw df again")
kw_df = vaex.from_csv(
	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/FieldsOfStudy.txt",
	header=None,
	usecols=[0, 2],
	names=["FieldOfStudyId", "FieldOfStudy"],
	delimiter="\t",
	progress=True
	)
print(f"loading our_paper2kw_df again")
our_paper2kw_df = vaex.open(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv",
	progress=True,
	)

# Use dask to read parquet
import dask.dataframe as dd

abstract_ddf = dd.read_parquet(ABSTRACT_DATA_PATHS, engine="pyarrow")
abstract_ddf = abstract_ddf.drop(["cat0", "cat0_coding", "cat2", "cat2_coding"], axis=1)
abstract_ddf = abstract_ddf.dropna(subset=['abstract'])
# Sample the abstract_ddf
sampled_abstract_ddf = abstract_ddf.sample(frac=SAMPLE_RATIO, random_state=SEED).compute()
print(f"sampled_abstract_ddf shape = {sampled_abstract_ddf.shape}")
# Export
# sampled_abstract_ddf.to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_abstract.csv",
# 	index=False,
# 	encoding=ENCODING
# 	)

sampled_abstract_ddf.to_parquet(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_abstract.parquet",
	engine='pyarrow',
	index=False
	)

# import dask.dataframe as dd
#
# # Read the data
# abstract_ddf = dd.read_parquet(ABSTRACT_DATA_PATHS)
# abstract_ddf = abstract_ddf.drop(["cat0", "cat0_coding", "cat2", "cat2_coding"], axis=1)
# abstract_ddf = abstract_ddf.dropna(subset=['abstract'])
#
# # Sample according to the ratio
# sampled_abstract_ddf = abstract_ddf.sample(frac=SAMPLE_RATIO, random_state=SEED)
#
# # Group by 'cat1' and get the first 'cat1_coding' for each 'cat1' value in Dask environment
# cat1_mapping_ddf = abstract_ddf.groupby('cat1')['cat1_coding'].first()
# cat1_mapping_series = cat1_mapping_ddf.compute()
# cat1_mapping_df = cat1_mapping_series.reset_index()
# cat1_mapping_df.to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_cat1_to_cat1_coding_mapping.csv", index=False
# 	)
#
# # Compute the unique labels in original data and in the sampled data
# original_labels = abstract_ddf['cat1'].unique().compute()
# sampled_labels = sampled_abstract_ddf['cat1'].unique().compute()
# print(f"original_labels:\n{original_labels}")
# print(f"sampled_labels:\n{sampled_labels}")
#
# # Find labels that are missing in the sampled data
# missing_labels = set(original_labels) - set(sampled_labels)
# print(f"missing_labels:\n{missing_labels}")
#
# # Sample 5 samples for those missing labels
# extra_samples = []
# for label in missing_labels:
# 	label_data = abstract_ddf[abstract_ddf['cat1'] == label]
# 	label_data_count = len(label_data)
#
# 	# If there are less than 5 samples, take all, otherwise take 5 samples.
# 	if label_data_count <= 5:
# 		extra_samples.append(label_data.compute())
# 	else:
# 		extra_sample = label_data.head(5)
# 		extra_samples.append(extra_sample)
# print(f"extra_samples:\n{extra_samples}")
# # Concatenate extra samples as Pandas DataFrame
# extra_samples_pd = pd.concat(extra_samples, ignore_index=True)
#
# # Convert to Dask DataFrame with the same number of partitions as the original sampled data
# extra_samples_ddf = dd.from_pandas(extra_samples_pd, npartitions=sampled_abstract_ddf.npartitions)
#
# # Concatenate original sample and extra samples
# final_sampled_ddf = dd.concat([sampled_abstract_ddf, extra_samples_ddf])
#
# # Export
# final_sampled_ddf.compute().to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_abstract.csv", index=False, encoding=ENCODING
# 	)

# Use panda to read sampled abstract and convert to vaex
# sampled_abstract_df = pd.read_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_abstract.csv",
# 	on_bad_lines='skip',
# 	encoding=ENCODING,
# 	low_memory=False
# 	)
# sampled_abstract_df = vaex.from_pandas(sampled_abstract_df)

sampled_abstract_df = vaex.open(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_abstract.parquet")

# Get Keywords
sampled_kw_df = sampled_abstract_df.join(our_paper2kw_df, on="PaperId", how="inner", allow_duplication=True)
sampled_kw_df = sampled_kw_df.join(kw_df, on="FieldOfStudyId", how="inner")
sampled_kw_df.drop(columns=["FieldOfStudyId"], inplace=True)
sampled_kw_df = sampled_kw_df.groupby("PaperId").agg({"FieldOfStudy": "list"})
sampled_kw_df["Keywords"] = sampled_kw_df["FieldOfStudy"].apply(
	lambda kw_list: ", ".join(map(str, kw_list))
	)
sampled_kw_df.drop(columns=["FieldOfStudy"], inplace=True)

print(f"... sanity check: sampled_abstract_kw_df shape = {sampled_kw_df.shape}")
print(f"... sanity check: sampled_abstract_kw_df columns = {sampled_kw_df.get_column_names()}")
sampled_kw_df.export(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_kw.csv", progress=True, encoding=ENCODING
	)

sampled_df = sampled_abstract_df.join(sampled_kw_df, on="PaperId", how="inner")
# sampled_df.export(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled.csv", progress=True, encoding=ENCODING
# 	)
sampled_df: pd.DataFrame = sampled_df.to_pandas_df()
sampled_df.to_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.csv",
	index=False,
	encoding=ENCODING
	)

sampled_df = pd.read_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled.csv",
	encoding=ENCODING,
	)
counts = sampled_df.groupby(['cat1', 'cat1_coding']).size().reset_index(name='count')
counts.to_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_cat1_counts.csv", index=False, encoding=ENCODING
	)
