# -*- coding: utf-8 -*-
import pandas as pd

# df = pd.read_csv("bio-part.0.csv", lineterminator='\n')

# cat1_df = df[["cat1", "cat1_coding"]]
# cat1_df.drop_duplicates(inplace=True)
# cat1_df.sort_values("cat1_coding", inplace=True)
# cat1_df.drop(columns=["cat1_coding"], inplace=True)
# cat1_df.to_csv("bio_part0_cat1.csv", index=False)

# cat2_df = df[["cat2", "cat2_coding"]]
# cat2_df.drop_duplicates(inplace=True)
# cat2_df.sort_values("cat2_coding", inplace=True)
# cat2_df.to_csv("bio_part0_cat2.csv", index=False)

# filtered_df = df[["PaperId", "abstract"]]
# filtered_df.rename(columns={"abstract": "text"}, inplace=True) # Label Studio's requirement
# filtered_df.to_csv("bio_part0_main.csv", index=False, line_terminator="\r\n")

bio_fos_df = pd.read_csv("/Users/tuyilei/Desktop/Science-Clf/science-clf-main/science-clf/parquet_files/Bio_generated/Bio_paper_kw.csv")
bio_fos_df = bio_fos_df[["PaperId", "FieldOfStudy"]]
bio_kw_df = pd.DataFrame(columns=["PaperId", "Keywords"])

paper_ids = bio_fos_df["PaperId"].unique()
for paper_id in paper_ids:
	kw_per_paper_df = bio_fos_df[bio_fos_df["PaperId"] == paper_id]
	keywords = kw_per_paper_df["FieldOfStudy"].values.tolist()
	keywords_str = ", ".join(map(str, keywords))
	bio_kw_df.loc[bio_kw_df.shape[0]] = {"PaperId": paper_id, "Keywords": keywords_str}

bio_kw_df.to_csv("bio_part0_kw.csv")


bio_kw_df = pd.read_csv("bio_part0_kw.csv")

df = pd.read_csv("bio-part.0.csv", lineterminator='\n')
filtered_df = df[["PaperId", "abstract", "cat1"]]
bio_main_df = bio_kw_df.merge(
	filtered_df,
	on="PaperId",
	how="inner",
	)
bio_main_df = bio_main_df[["PaperId", "Keywords", "abstract", "cat1"]]
bio_main_df.to_csv("bio_main.csv", index=False, line_terminator="\r\n")
