# -*- coding: utf-8 -*-
from pathlib import Path

import numpy as np

from script import data_helper, data_loader

MODEL_NAME = "model-0-9"
MODEL_DIR = "../model-0-9_files"
FIELD = "infk"

label_dict = data_loader.load_label_dict(
	load_dir=MODEL_DIR,
	model_name=MODEL_NAME
	)

partition_train = data_loader.load_model_data(
	load_dir=MODEL_DIR,
	model_name=MODEL_NAME,
	split_type="train"
	)

all_field = set([partition.split("-")[1] for partition in label_dict])
txn_dict = dict()
for field in all_field:
	txn_dict[field] = data_helper.load_lmdb(
		folder_path=Path(MODEL_DIR).joinpath(f"{field}_abstract_vec_lmdb")
		)

print(txn_dict)

# X = np.array(
# 	[data_helper.get_npy_value_lmdb(txn=txn_dict[partition.split("-")[1]], key=partition, dtype="int64") for partition
# 	 in partition_train]
# 	)
# print(X)

ex = partition_train[0]
print(ex)
# tmp = data_helper.get_npy_value_lmdb(txn=txn_dict[FIELD], key=ex, dtype="int64")
txn = txn_dict[FIELD]
lmdb_cursor = txn.cursor()
for k, v in lmdb_cursor:
	print(k, v)




# test_npy_byte = txn.get(ex.encode(), None)
# print("tmp:", test_npy_byte)
