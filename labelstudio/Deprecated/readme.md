# Data directories on /data-nfs/wos

- Raw of MAG: `/data-nfs/wos/mag/v2018-05`, but probably it will not be that useful for you at the moment.<br>
  (1) keywords of papers: each paper is tagged with fieldofstudies, there is a script extracting and counting top keywords, see https://gitlab.ethz.ch/raox/science-clf/-/blob/labelstudio/script/get_top_keywords_by_count.py <br>
  (2) data schema: https://learn.microsoft.com/en-us/academic-services/graph/reference-data-schema
- Publication-discipline mapping (papers in the training set): `/data-nfs/wos/HBase_old_steps/HBase`. This is not for all the disciplines, but you get a feel how things look like. If you need one particular file, just let me know.
- Training: `/data-nfs/wos/wos`, for _model-0-9_, e.g., the corresponding files are under `/data-nfs/wos/wos/ACM-model`. The number-to-field mappings are under this [setup](https://gitlab.ethz.ch/raox/science-clf/-/blob/main/script/var_setup.py).

- DF of training files (single-label): under `/data-nfs/wos/wos/all-wos`

    1. You can use [this script](https://gitlab.ethz.ch/raox/science-clf/-/blob/main/script/data_maker.py) [lines 92-96](https://gitlab.ethz.ch/raox/science-clf/-/blob/main/script/data_maker.py#L92) to open the DF. 
    2. Use the above script to open the parquet files `{journal}_{discipline}_processed.parquet` like `ACM_infk_processed.parquet`, e.g., `/data-nfs/wos/wos/all-wos/ACM_infk_processed.parquet`
    3. Normally, each line in DF should be `index, paperid, abstract, cat1, cat1-coding, cat2, cat2-coding, cat3, cat3-coding`, like `0,2142155698,"The antidiuretic responses ... [the rest of the abstract]",agri,0,Applied computing,4,Physical sciences and engineering,3`, if we take `agri` as a discipline.


- DF of training files (multi-label): under `/data-nfs/wos/wos/all-multilabel-wos-post-thesis`. E.g., 
    ```
    df = pd.read_parquet('JEL_econ_processed.parquet')
    In [15]: df.loc[13]
    Out[15]: 
    PaperId                                              2007628447
    abstract      No matter what condition an organisation may b...
    cat_coding                                             [1-13-0]
    Name: 13, dtype: object

    In [16]: df.loc[133]
    Out[16]: 
    PaperId                                              2126573767
    abstract      The economic impact of changing land-use polic...
    cat_coding                                [1-4-0, 1-8-2, 1-7-2]
    Name: 133, dtype: object

    # Note not every instance is multi-label. 
    # Only when len(cat_coding) > 1, the instance is coded multi-label.
    ```

- Sub-model(s) to improve, we make a demo with that model and LabelStudio. <br>
  [Susie:] I think we can start with _model-0-9_. But we can pick other models in Appendices D & F.


- Input data: See the section below. Note the pre-computed embeddings for each discipline are under `/data-nfs/wos/wos/{$journal}-model/{$discipline}_abstract_vec_lmdb`, e.g., `/data-nfs/wos/wos/JEL-model/econ_abstract_vec_lmdb`

- Output data: You can filter by `tags.mlflow.runName` and `label_type` in this [Excel file](https://gitlab.ethz.ch/raox/science-clf/-/blob/labelstudio/labelstudio/annotated_inference_transformer_all-result_20221115_2311_manual.xlsx).<br>
  E.g., for _model-0-9, multilabel_ the model is stored under `/data-nfs/wos/wos/all-transformer-output-POST-THESIS/mlruns/59/eb6d2cbfcdc64141ade1214ef3f6494e/artifacts`. How to use the pre-trained model for inference you can find the relevant scripts under `script` and following the instructions [here](https://gitlab.ethz.ch/raox/science-clf/-/tree/labelstudio/#inference).<br>
  E.g., for single-label, model-0, the model is stored under `/data-nfs/wos/wos/all-output/mlruns/265/5831a2afe97c456a9b68ac460894b686/artifacts`.

Note that the Excel above was generated automatically. Let me know if you need the script for that. Essentially, for each experiment the user runs we keep a record.

# How to run the classifier, e.g., _model-0-9_

```bash
# singlelabel training with cnn or rnn (`--model_type_list dummy dummy cnn` for rnn)

bash
conda activate science-clf-py3.9
cd /data-nfs/wos/clf_v2/science-clf

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/
export CUDA_DEVICE_ORDER=PCI_BUS_ID
export CUDA_VISIBLE_DEVICES=0
export TF_CPP_MIN_LOG_LEVEL=3

python3 script/model_runner.py \
  --shared_dir /data-nfs/wos/wos/shared-wos/ \
  --wos_dir /data-nfs/wos/wos/all-wos/ \
  --model_dir /data-nfs/wos/wos/all-model/ \
  --output /data-nfs/wos/wos/all-output/ \
  --model_type_list dummy dummy cnn \
  --vocab_type topcount3000 \
  --emb_type glove \
  --emb_token_size 6 \
  --emb_dim 50 \
  --no-emb_trainable \
  --label_type single \
  --model_name model-0-9



# single/multi-label training with transformers 
# See Appendix D, 0-9 transformer-single (`--label_type single`)
# See Appendix F, 0-9 transformer-multi (`--label_type multi`)

bash
conda activate science-clf-py3.9
cd /data-nfs/wos/clf_v2/science-clf

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/
export CUDA_DEVICE_ORDER=PCI_BUS_ID
export CUDA_VISIBLE_DEVICES=1
export TF_CPP_MIN_LOG_LEVEL=3

python3 script/transformer_model_runner.py \
  --model_dir /data-nfs/wos/wos/all-model/ \
  --output_dir /data-nfs/wos/wos/all-transformer-output-POST-THESIS/ \
  --model_name_list model-0-9 \
  --no-emb_trainable \
  --num_epoch 2 \
  --batch_size 16 \
  --dgen_shuffle \
  --label_type multi 
```

# Things to try

[Susie:]

- I am thinking we can use the tranformer models in labelstudio, let's take _model-0-9_, singlelabel as an example.
- Step 1: we put the pre-trained model into the labelstudio engine and enable user annotations (to provide more labels, or debug on the existing discipline-publication mappings).
