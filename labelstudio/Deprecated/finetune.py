# -*- coding: utf-8 -*-
import datetime
import gc
import multiprocessing
import os
import shutil
from argparse import ArgumentParser, BooleanOptionalAction
from pathlib import Path

import math
import mlflow
from keras.engine.functional import Functional
from keras.models import load_model
from math import ceil
from sklearn.preprocessing import MultiLabelBinarizer
from tensorflow.keras.metrics import Precision, Recall
from tensorflow.keras.optimizers import RMSprop
from tensorflow_addons.metrics import F1Score
import tensorflow as tf

from script import data_helper, data_loader, resource_tracker
from script.model_runner import data_generator

MODEL_NAME: str = "model-1"
FIELD: str = "econ"
MODEL_INPUTS_PATH: str = "/data-nfs/yilei/sciclf/labelstudio/labeled_data"

MODEL_SAVED_PATH: str = "/data-nfs/wos/wos/all-output/mlruns/265/5831a2afe97c456a9b68ac460894b686/artifacts/model/data/model"
FINETUNE_ONWARDS: int = 3  # Finetune from this layer onwards
PRETRAIN_EPOCHS: int = 1
FINETUNE_EPOCHS: int = 1

parser = ArgumentParser()
parser.add_argument("--batch_size", type=int, default=1024, help="batch size, e.g. '1024' ")
parser.add_argument("--pretrain_epoch", type=int, default=1, help="number of pretrain epochs, default = 1")
parser.add_argument("--finetune_epoch", type=int, default=10, help="number of finetune epochs, default = 1")
parser.add_argument(
	"--dgen_shuffle", action=BooleanOptionalAction, default=True,
	help="whether shuffle in data generator, default = False"
	)
parser.add_argument(
	"--max_token_id_seq_len", type=int, default=200, help="max. token sequence length to consider from each text"
	)
parser.add_argument("--output_dir", type=str, default="/data-nfs/wos/wos/all-output/", help="directory of wos, e.g. '/data/output/' in mtec-cae-gpu01 ")
options = parser.parse_args()

txn_dict = dict()
txn_dict[FIELD] = data_helper.load_lmdb(folder_path="/data-nfs/wos/wos/JEL-model/econ_abstract_vec_lmdb")
partition_train = data_loader.load_model_data(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME,
	split_type="train"
	)
label_dict = data_loader.load_label_dict(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME
	)
input_batch_size = int(options.batch_size)
batch_size = min(
	2 ** (math.floor(math.log(len(partition_train), 2))),
	input_batch_size
	)
class_list = data_loader.load_class(
	load_dir=MODEL_INPUTS_PATH,
	model_name=MODEL_NAME
	)
num_class: int = len(class_list)
binarizer = MultiLabelBinarizer()
binarizer.fit([tuple(class_list)])

training_generator = data_generator(
	txn_dict=txn_dict,
	partition_list=partition_train,
	label_dict=label_dict,
	batch_size=batch_size,
	max_token_id_seq_len=options.max_token_id_seq_len,
	shuffle=(options.dgen_shuffle or options.num_epoch > 1),
	binarizer=binarizer
	)

### MLFLOW ###
new_uri = Path(options.output_dir).joinpath("mlruns").resolve().as_uri()
mlflow.set_tracking_uri(new_uri)
client = mlflow.tracking.MlflowClient()
experiment_name = f"test_{datetime.datetime.now()}"
mlflow.set_experiment(experiment_name)
experiment = mlflow.get_experiment_by_name(experiment_name)
mlflow.start_run(
	experiment_id=experiment.experiment_id,
	run_name=f"test_{MODEL_NAME}"
	)

pid = os.getpid()
resource_log_dir = Path(options.output_dir).joinpath("resource_log")
if resource_log_dir.is_dir():
	shutil.rmtree(resource_log_dir)
resource_log_dir.mkdir(
	mode=0o755,
	exist_ok=True,
	parents=True
	)
overall_gpu_p, proc_gpu_p = resource_tracker.log_gpu(
	pid=pid,
	model_name=MODEL_NAME,
	save_dir=resource_log_dir
	)
cpu_ram_p = resource_tracker.log_cpu_ram(
	pid=pid,
	model_name=MODEL_NAME,
	save_dir=resource_log_dir
	)

mlflow.tensorflow.autolog()
gpu_list = tf.config.list_logical_devices("GPU")
mlflow.log_param("num_gpu_used", len(gpu_list))
### MLFLOW ###

model: Functional = load_model(MODEL_SAVED_PATH, compile=False)
model.compile(
	loss="categorical_crossentropy",  # output in proba array
	optimizer=RMSprop(learning_rate=0.001),
	metrics=["categorical_accuracy", Precision(), Recall(),
	         F1Score(num_classes=num_class, average="micro", name="f1_micro"),
	         F1Score(num_classes=num_class, average="macro", name="f1_macro")]
	)
model.trainable = True
# Freeze all the layers before the `fine_tune_at` layer
for layer in model.layers[:FINETUNE_ONWARDS]:
	layer.trainable = False

total_epochs = options.pretrain_epoch + options.finetune_epoch
finetune_history = model.fit(
	x=training_generator,
	steps_per_epoch=ceil(len(partition_train) / batch_size),
	use_multiprocessing=True,
	workers=multiprocessing.cpu_count(),
	epochs=total_epochs,
	initial_epoch=options.pretrain_epoch,
	)

### MLFLOW ###
overall_gpu_p.terminate()
proc_gpu_p.terminate()
cpu_ram_p.terminate()
mlflow.log_artifacts(
	local_dir=resource_log_dir,
	artifact_path="resource_log"
	)
mlflow.end_run()
### MLFLOW ###

shutil.rmtree(resource_log_dir)
del model
tf.keras.backend.clear_session()
gc.collect()
