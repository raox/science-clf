# -*- coding: utf-8 -*-
from typing import List

import pandas as pd
import vaex
from vaex.dataframe import DataFrameLocal

CAT0: str = "CS"
# ABSTRACT_DATA_PATHS: List[str] = ["/data-nfs/wos/wos/all-wos/Bio_bio_processed.parquet"]
ABSTRACT_DATA_PATHS: List[str] = ["/data-nfs/wos/wos/all-wos/ACM_infk_processed.parquet"]
SEED: int = 21946520
SAMPLE_RATIO: float = 5e-5

# print(f"loading our pq")
# our_df = vaex.open_many(ABSTRACT_DATA_PATHS)
# print(f"... sanity check: shape = {our_df.shape}")
# print(our_df.head(1))
# print(f"fetching unique PaperIds from our pq")
# unique_paperid = our_df.PaperId.unique(dropnan=True, dropmissing=True, limit_raise=False, progress=True)
# print(f"... sanity check: shape = {len(unique_paperid)}")
# print(f"creating dummy pq for our PaperId")
# dummy_paperid_df = vaex.from_arrays(PaperId=unique_paperid)
# print(f"loading paper2kw df")
# paper2kw_df = vaex.from_csv(
# 	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/PaperFieldsOfStudy.txt",
# 	header=None,
# 	usecols=[0, 1, 2],
# 	names=["PaperId", "FieldOfStudyId", "FieldOfStudyProba"],
# 	delimiter="\t",
# 	progress=True
# 	)
# print(f"filtering only our paper2kw df")
# our_paper2kw_df = paper2kw_df.join(
# 	dummy_paperid_df,
# 	on="PaperId",
# 	how="inner",
# 	allow_duplication=True
# 	)
# print(f"... sanity check: shape after joining = {our_paper2kw_df.shape}")
# print(our_paper2kw_df.head(1))
# print(f"saving OUR paper2kw df")
# our_paper2kw_df.export_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv", progress=True
# 	)
# del our_paper2kw_df

print(f"loading our pq")
our_df = vaex.open_many(ABSTRACT_DATA_PATHS)
print(f"... sanity check: shape = {our_df.shape}")
print(our_df.head(1))
print(f"fetching unique PaperIds from our pq")
unique_paperid = our_df.PaperId.unique(dropnan=True, dropmissing=True, limit_raise=False, progress=True)
print(f"... sanity check: shape = {len(unique_paperid)}")
print(f"creating dummy pq for our PaperId")
dummy_paperid_df = vaex.from_arrays(PaperId=unique_paperid)
print(f"loading paper2kw df")
paper2kw_df = vaex.from_csv(
	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/PaperFieldsOfStudy.txt",
	header=None,
	usecols=[0, 1, 2],
	names=["PaperId", "FieldOfStudyId", "FieldOfStudyProba"],
	delimiter="\t",
	progress=True
	)
print(f"filtering only our paper2kw df")
our_paper2kw_df = paper2kw_df.join(
	dummy_paperid_df,
	on="PaperId",
	how="inner",
	allow_duplication=True
	)
print(f"... sanity check: shape after joining = {our_paper2kw_df.shape}")
print(our_paper2kw_df.head(1))
print(f"saving OUR paper2kw df")
our_paper2kw_df.export_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv", progress=True
	)
del our_paper2kw_df

print(f"loading kw df again")
kw_df = vaex.from_csv(
	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/FieldsOfStudy.txt",
	header=None,
	usecols=[0, 2],
	names=["FieldOfStudyId", "FieldOfStudy"],
	delimiter="\t",
	progress=True
	)

print(f"loading our_paper2kw_df again")
our_paper2kw_df: DataFrameLocal = vaex.open(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv",
	progress=True,
	)
paper_kw_df: DataFrameLocal = our_paper2kw_df.join(kw_df, on="FieldOfStudyId", how="inner")
paper_kw_df.drop(columns=["FieldOfStudyId"], inplace=True)
paper_kw_df = paper_kw_df.groupby("PaperId").agg({"FieldOfStudy": "list"})
paper_kw_df.rename(name="FieldOfStudy", new_name="Keywords")
paper_kw_df["Keywords"] = paper_kw_df["Keywords"].apply(lambda kw_list: ", ".join(map(str, kw_list)))
print(paper_kw_df.head(1))

abstract_df: DataFrameLocal = vaex.open_many(ABSTRACT_DATA_PATHS)
abstract_df.drop(columns=["cat0", "cat0_coding", "cat2", "cat2_coding"], inplace=True)
abstract_df = abstract_df.dropna()
abstract_kw_df: DataFrameLocal = abstract_df.join(paper_kw_df, on="PaperId", how="inner")
print(f"... sanity check: abstract_kw_df shape = {abstract_kw_df.shape}")
# abstract_kw_df.export_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1.csv", progress=True)

cat1 = abstract_kw_df.cat1.unique(dropnan=True, dropmissing=True, limit_raise=False, progress=True)
print(f"Num of cat1: {len(cat1)}")
cat1_df = pd.DataFrame(cat1, columns=['cat1'])
cat1_df.to_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_cat1.csv")
# cat1_cnt_df: DataFrameLocal = abstract_kw_df.groupby(by="cat1", agg={"cat1": "count"})
# cat1_cnt_df.export_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_cat1_cnt.csv", progress=True)


sampled_df: DataFrameLocal = abstract_kw_df.sample(frac=SAMPLE_RATIO, random_state=SEED)
print(sampled_df.dtypes)
print(sampled_df.describe())
print(f"... sanity check: sampled_df shape = {sampled_df.shape}")
sampled_cat1_cnt_df = sampled_df.groupby("cat1").agg({"cat1": "count"})
sampled_cat1_cnt_df.export_csv(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_cat1_cnt.csv", progress=True
	)
sampled_df.export(
	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.parquet", shuffle=False,
	progress=True
	)

# sampled_df.export(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.parquet", progress=True)
# df = pd.read_parquet(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.parquet")
# df.to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.csv",
# 	index=False,
# 	line_terminator="\r\n"
# 	)
# sampled_df: pd.DataFrame = sampled_df.to_pandas_df(chunk_size=100)
# sampled_df.to_csv(
# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.csv",
# 	index=False,
# 	line_terminator="\r\n"
# 	)
