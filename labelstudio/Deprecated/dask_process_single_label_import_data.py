# -*- coding: utf-8 -*-
import dask.dataframe as dd
from dask.distributed import Client

if __name__ == '__main__':
	CAT0: str = "CS"
	# ABSTRACT_DATA_PATHS: List[str] = ["/data-nfs/wos/wos/all-wos/Bio_bio_processed.parquet"]
	ABSTRACT_DATA_PATHS = "/data-nfs/wos/wos/all-wos/ACM_infk_processed.parquet"
	SEED: int = 21946520
	SAMPLE_RATIO: float = 5e-5

	client = Client()  # This will also start a local Dask cluster if you don't have one

	# from dask.distributed import Client
	# client = Client()  # start a local Dask client

	# print(f"loading our pq")
	# our_df = dd.read_parquet(ABSTRACT_DATA_PATHS)
	# print(f"... sanity check: shape = {our_df.shape[0].compute()}")
	# print(our_df.head(1))
	#
	# print(f"fetching unique PaperIds from our pq")
	# unique_paperid = our_df['PaperId'].drop_duplicates().compute()
	#
	# print(f"... sanity check: shape = {len(unique_paperid)}")
	# print(f"creating dummy pq for our PaperId")
	#
	# unique_paperid_df = unique_paperid.to_frame()
	# dummy_paperid_df = dd.from_pandas(unique_paperid_df, npartitions=1)
	#
	# print(f"loading paper2kw df")
	# paper2kw_df = dd.read_csv(
	# 	"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/PaperFieldsOfStudy.txt",
	# 	header=None,
	# 	usecols=[0, 1, 2],
	# 	names=["PaperId", "FieldOfStudyId", "FieldOfStudyProba"],
	# 	delimiter="\t"
	# 	)
	#
	# print(f"filtering only our paper2kw df")
	# our_paper2kw_df = paper2kw_df.merge(dummy_paperid_df, on="PaperId", how="inner")
	#
	# print(f"... sanity check: shape after joining = {our_paper2kw_df.shape[0].compute()}")
	# print(our_paper2kw_df.head(1))
	#
	# print(f"saving our paper2kw df")
	# our_paper2kw_df.to_csv(
	# 	f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv", single_file=True, index=False
	# 	)
	# del our_paper2kw_df

	print(f"loading kw_df")
	kw_df = dd.read_csv(
		"/data/ppiriyata/backup-mag/raw-txt-mag-2018-05-17_copyBachRaoxScratch/FieldsOfStudy.txt",
		header=None,
		usecols=[0, 2],
		names=["FieldOfStudyId", "FieldOfStudy"],
		delimiter="\t",
		assume_missing=True
		)

	# 1. Load our_paper2kw_df using dask
	print(f"loading our_paper2kw_df again")
	our_paper2kw_df = dd.read_csv(
		f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_PaperFieldsOfStudy.csv"
		)

	# 2. Join with kw_df dataframe
	paper_kw_df = our_paper2kw_df.merge(kw_df, on="FieldOfStudyId", how="inner")

	# 3. Drop FieldOfStudyId column
	paper_kw_df = paper_kw_df.drop("FieldOfStudyId", axis=1)

	# 4. Group by PaperId and aggregate FieldOfStudy as list
	paper_kw_df = paper_kw_df.groupby("PaperId").agg({"FieldOfStudy": list})

	# 6. Load abstract_df using dask
	print(f"loading abstract_df")
	abstract_df = dd.read_parquet(ABSTRACT_DATA_PATHS)
	print("abstract_df loaded")

	# 7. Drop unwanted columns and drop NA rows
	abstract_df = abstract_df.drop(["cat0", "cat0_coding", "cat2", "cat2_coding"], axis=1)
	abstract_df = abstract_df.dropna()

	if abstract_df.index.name != 'PaperId':
		abstract_df = abstract_df.set_index('PaperId')
	if paper_kw_df.index.name != 'PaperId':
		paper_kw_df = paper_kw_df.set_index('PaperId')


	# Sampling
	def stratified_sample(group, sample_frac):
		return group.sample(frac=sample_frac, random_state=SEED)


	# print("Performing stratified sampling on abstract_df")
	# sampled_abstract_df = abstract_df.groupby('cat1').apply(
	#     stratified_sample, sample_frac=SAMPLE_RATIO, meta=abstract_df._meta
	#     ).compute()
	# print("sampled_df created")

	print("Performing direct sampling on abstract_df")
	sampled_abstract_df = abstract_df.sample(frac=SAMPLE_RATIO, random_state=SEED)
	print("sampled_abstract_df created")

	# Joining the sampled data with paper_kw_df
	if sampled_abstract_df.index.name != 'PaperId':
		sampled_abstract_df = sampled_abstract_df.set_index('PaperId')


	print("Joining sampled_abstract_df with paper_kw_df")
	sampled_abstract_kw_df = sampled_abstract_df.merge(paper_kw_df, on="PaperId", how="inner")
	print(f"... sanity check: sampled_abstract_kw_df shape = {sampled_abstract_kw_df.shape}")

	sampled_abstract_kw_df = sampled_abstract_kw_df.persist()

	# Now, apply join_keywords only to the sampled_df
	def join_keywords_partition(partition_df):
		partition_df["Keywords"] = partition_df["FieldOfStudy"].apply(
			lambda kw_list: ", ".join(map(str, kw_list)) if isinstance(kw_list, list) else kw_list
			)
		return partition_df


	# print("Creating Keywords column in sampled_df")
	# sampled_kw_df = sampled_kw_df.map_partitions(join_keywords_partition).compute()
	# print("Keywords column in sampled_df created")
	# sampled_kw_df = sampled_kw_df.drop("FieldOfStudy", axis=1)

	print(f"... sanity check: sampled_df shape = {sampled_abstract_kw_df.shape}")
	sampled_abstract_kw_df.to_csv(
		f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_abstract_kw_cat1_sampled.csv", index=False,
		single_file=True
		)
	print("sampled_df saved to CSV")

	sampled_cat1_cnt_df = sampled_abstract_kw_df.groupby("cat1").size().reset_index(name='count').compute()
	sampled_cat1_cnt_df.to_csv(
		f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled_cat1_cnt.csv", index=False, single_file=True
		)
	print("sampled_cat1_cnt_df saved to CSV")

	client.close()

# # 9. Get unique cat1 values
# # compute() is needed as drop_duplicates doesn't return a Dask Series
# cat1 = abstract_kw_df["cat1"].drop_duplicates().compute()
# print(f"Num of cat1: {len(cat1)}")
#
# # 10. Save cat1 values to CSV
# cat1_df = pd.DataFrame(cat1, columns=['cat1'])
# cat1_df.to_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_cat1.csv", index=False)
