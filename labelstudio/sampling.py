# -*- coding: utf-8 -*-
import pandas as pd

CAT0: str = "BIO"
FILE_PATH = f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_sampled.csv"
SAMPLE_RATIO = 0.3

data = pd.read_csv(FILE_PATH)
sample_data = data.sample(frac=SAMPLE_RATIO)
sample_data.to_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_resampled.csv", index=False)
counts = sample_data.groupby(['cat1', 'cat1_coding']).size().reset_index(name='count')
counts.to_csv(f"/data-nfs/yilei/sciclf/Generated_files/{CAT0}/{CAT0}_resampled_cat1_counts.csv", index=False)