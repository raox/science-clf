# Data Pre- and Post-Processing for Label Studio

```
├── Deprecated
├── README.md
├── annotation_guideline.md
├── convert_parquet_to_csv.py
├── labeled_data
│   ├── ECON_cat1_encoding.csv
│   ├── labelstudio_econ.json
│   ├── model-1_class.json
│   ├── model-1_id_train.json
│   └── model-1_label_dict.json
├── ls_interface_template
│   ├── ECON.html
│   └── ECON_multilabel.html
├── process_ls_json.py
├── process_multilabel_data.py
├── process_single_label_import_data.py
├── ssrn_crawler
│   ├── README.md
│   ├── abstract_downloader.py
│   ├── abstract_downloader_dummy.py
│   ├── config.py
│   ├── sample_reader.py
│   └── url_collector.py
└── stratified_sampling.py
```

## Folder Summary

- `Deprecated`  contains files that are not in use at the moment and may be used as reference for new code in the future.
- `labeled_data` contains data (in json format) that conforms to our model input format after processing based on annotated data from *Label Studio*.
- `ls_interface_template` contains some html files that are used for the custom user interface of *Label Studio*.
- `ssrn_crawler` is used to crawl inference data. Please refer to `README.md` under this folder for details.

## Script Summary

- `annotation_guideline.md` describes to annotators how to annotate our preprocessed data on *Label Studio*.
- `convert_parquet_to_csv.py` is a simple python script to convert parquet format into csv format data for the convenience of data processing.
- `process_ls_json.py` is to post-process the annotated data from *Label Studio*. It generates data (in json format) that matches the input format of our model.
-  `process_single_label_import_data.py` is to pre-process our single-label MAG database (in Parquet format, take JEL as an example) and generate a dataset (in CSV format) that is suitable for annotators to label in *Label Studio*.
- Similarly, `process_multilabel_data.py` to pre-process our multi-lable MAG database.

