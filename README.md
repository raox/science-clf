# science-clf

**Modularized Hierarchical Classification for Scholarly Publications** <br>
Codebase for Quantitative Science Studies (QSS) submission, 2023. <br> <br>

**Manuscript**: "Hierarchical Classification of Research Fields in the “Web of Science” Using Deep Learning" ([Preprint](https://arxiv.org/abs/2302.00390))<br>
**Authors**: Susie Xi Rao [1,2*], Peter H. Egger [1] and Ce Zhang [2]<br>
[1] Department of Management, Technology, and Economics, ETH Zurich, Leonhardstrasse 21, Zurich, 8092, Zurich, Switzerland.<br>
[2]  Department of Computer Science, ETH Zurich, Stampfenbachstrasse 114, Zurich, 8092, Zurich, Switzerland.<br>
[*] Corresponding author(s). E-mail(s): srao@ethz.ch;<br>
Contributing authors: pegger@ethz.ch; ce.zhang@inf.ethz.ch<br><br>

**License**: Copyright and license term – CC BY<br>
In opting for open access, the author(s) agree to publish the article under the Creative Commons Attribution License.<br>
Find more about the [license agreement](https://creativecommons.org/licenses/by/4.0/).<br>

- [science-clf](#science-clf)
  - [Set up the Environment](#environment)
    - [Prepare the Machine](#machine)
    - [Set up Conda](#conda)
  - [Prepare the Dataset](#dataset)
  - [Run the classification system](#classification-system)
    - [Run on the Test Input](#test-input)
  - [Check the results of the system](#result)
  - [Run inference on trained models](#inference)
  - [Miscellaneous](#miscellaneous)
    - [tmux for multi-screens](#tmux)
    - [Git for code version control](#git)
  - [Conda tips](#conda-tips)
    - [YAML file](#yaml-file)
    - [Environment variables](#environment-variables)
    - [LMDB permission](#lmdb-permission)

## Environment

### Machine

We assume that your machine is on Linux or has access to it, e.g. via Windows Subsystem for Linux (WSL).

Ideally, the machine should also have GPUs for fast training.

### Conda

Perform the following steps:

1. Install [Miniconda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html). Make sure you choose installer for Python 3.9 or higher.

   *Miniconda is a small, bootstrap version of Anaconda. It comes with conda, Python, and a few dependencies.*
2. Install [Mamba](https://mamba.readthedocs.io/en/latest/installation.html#existing-conda-install):

   ```sh
   conda install mamba -n base -c conda-forge
   ```

   **Note:** Some may need to run first:

   ```sh
   source /home/{$nethzname}/miniconda3/etc/profile.d/conda.sh
   ```

   *Mamba is a re-implementation of conda in C++. It allows a more efficient processing for parallel downloading and dependency solving.*
3. (Optional) Prepare .yaml file to set up the environment.

   **Note:** Already provided in this repository: `science-clf-py3.9.yaml`.
4. Set up conda environment using mamba and the prepared .yaml file:

   ```sh
   mamba env create -f science-clf-py3.9.yaml
   ```
5. Activate the conda environment using conda:

   ```sh
   conda activate science-clf-py3.9
   ```
6. (Optional) [Save enviornment variables](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#macos-and-linux): Prepare `env_vars.sh` file to set-up necessary variables (e.g., for using GPUs in tensorflow) upon each time the conda environment is activated

   **Note:** Already provided in this repository: `env_vars.sh`
7. Put the prepared `env_vars.sh` in `$CONDA_PREFIX/etc/conda/activate.d`:

   ```sh
   mkdir -p $CONDA_PREFIX/etc/conda/activate.d
   mv env_vars.sh $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
   ```
8. Restart the conda environment:

   ```sh
   conda deactivate
   conda activate science-clf-py3.9
   ```

   (Optional) Verify that the variables are correctly set using `echo ${$VARIABLE_NAME}`
9. (Later) If you ever need to update packages in .yaml file, run:

   ```sh
   mamba env update -f science-clf-py3.9.yaml
   ```

## Input

The input files must be organized in two folders:

- The "shared" folder for training-independent inputs, including

  - vocabulary list file(s) in .txt format
  - pre-trained word embeddings file(s) (such as GLoVe) in [lmdb](https://lmdb.readthedocs.io/en/release/) format

  ```
  shared-folder/
  ├── vocab_list/
  │   └── {$VOCAB_LIST_NAME}_vocab_list.txt
  └── word_embeddings/
      └── {$WORD_EMBEDDING_TYPE}/
          └── {$WORD_EMBEDDING_NAME}_npy_lmdb/
              ├── data.mdb
              └── lock.mdb
  ```
- The "training" folder for training inputs, including

  - training file(s) from each journal in .parquet format

  ```
  training-folder/
  └── {$DATASET_NAME}_{$DISCIPLINE_NAME}_processed.parquet
  ```

The two folders **do not** necessarily have to be under the same parent directory.

**Note:**

- The vocabulary list currently **has to be prepared independently.**
- The GLoVe word embeddings are currently integrated to the pipeline, therefore do not need to be prepared separately.
- The training file in .parquet format has to be prepared independently. To convert from .pkl files, take a look at the script `script/pickle_to_parquet.py`

## Classification System

The general command for running the classification system is as follows:

```sh
python3 script/model_runner.py --shared_dir {$SHARED_DIR} --wos_dir {$WOS_DIR} --model_dir {$MODEL_DIR} --output_dir {$OUTPUT_DIR} --model_name_list {$MODEL_NAME_LIST} --model_type {$MODEL_TYPE} --vocab_type {$VOCAB_TYPE} --emb_type {$EMB_TYPE} --emb_token_size {$EMB_TOKEN_SIZE} --emb_dim {$EMB_DIM} --(no-)emb_trainable --batch_size {$BATCH_SIZE} --(no-)recompute_emb --(no-)recompute_journal --compute_journal_list {$JOURNAL_LIST}
```

The command must be run **from inside** this repository directory (`science-clf/`).

The code will perform the following steps:

1. Generate the model-ready data (including pre-trained word embeddings as well as text vectors from the abstracts) and save them into `{$MODEL_DIR}`
2. Train the model using the generated data and save the result into `{$OUTPUT_DIR}`

After the first time you run the code, if you re-use the same input dataset, you can use the cached model-ready data by the following flags: `--no-recompute_emb --no-recompute_journal`.

### Test Input

We provide the following [test input](https://u.ethz.ch/Qtam3) which you can download and run with the following code on:

```sh
python3 script/model_runner.py --shared_dir /home/{$nethzname}/test-shared-wos/ --wos_dir /home/{$nethzname}/test-wos/ --model_dir /home/{$nethzname}/test-model/ --output_dir /home/{$nethzname}/test-output/ --model_name_list model-0-1 model-0-2 model-0-3 --model_type dnn --vocab_type topcount3000 --emb_type glove --emb_token_size 6 --emb_dim 50 --no-emb_trainable --batch_size 16 --recompute_emb --recompute_journal --compute_journal_list ACM JEL
```

assuming you put the extracted folders from .zip in `/home/{$nethzname}/`

As mentioned, after the first time you run, you may replace the ending part `--recompute_emb --recompute_journal --compute_journal_list ACM JEL` with `--no-recompute_emb --no-recompute_journal`, since the model-ready data has already been generated and cached.

## Result

To check the model run logs recorded by [MLFlow](https://mlflow.org/docs/latest/index.html), go inside the output directory you specified and run:

```sh

cd {$OUTPUT_DIR}
mlflow ui

```

The result will be available at [localhost:5000](http://localhost:5000/) on port 5000 or other port as specified with `-p {$PORT_NUMBER}`.

**Note:**

- Each `python3` execution will create a new "experiment" with date and time log.
- Each model run will create a new "run" under the experiment.
- The output directory will look like following:

  ```
  experiment-folder/
  └── run-folder/
      ├── artifacts/
      │   ├── model_summary.txt
      │   ├── env_set_up/
      │   ├── model/
      │   ├── model_set_up/  
      │   ├── resource_log/   
      │   ├── script/
      │   └── tensorboard_logs/         
      ├── metrics/
      │   ├── {$METRIC}
      │   ├── val_{$METRIC}
      │   ├── loss
      │   └── val_loss
      └── params/
          └── {$PARAM}
  ```

  You may check the content of the logs also on the MLFlow interface.

*MLFLow is a platform for managing machine learning lifecycle. It allows automatic tracking of the ML pipeline.*

## Inference

To run inference on the trained models, download the parent folder of `mlruns` folder and perform the following steps:

0. Download the parent folder of `mlruns` folder.
1. Paste the `mlflow_fix_path.py` script inside the downloaded folder and run it: `python3 mlflow_fix_path.py`. This will fix MLFlow path from remote paths to local paths on your machine.
2. Paste the `mlflow_to_csv.py` script inside the downloaded folder and run it: `python3 mlflow_to_csv.py`. This will create a csv file `all-result.csv` containing all the experiment results.
3. Browse to your local GitHub repository folder and update `shared_wos_dir` and `output_dir` variable in `script/inference_utils.py`.

- `shared_wos_dir` example: `/data/{$nethzname}/shared-wos`
- `output_dir` example: `/data/{$nethzname}/all-output/mlruns`

4. Create a `model_map.json` file in the parent directory of `mlruns`, in this format `{$model_name: $experiment_ID}`, e.g., `{"model-1": "265"}`
                   
5. Run `science-clf$ uvicorn script.fastapi:app --reload`.

The API along with usage instruction will be available at [localhost:8000/docs](http://localhost:8000/docs) on port 8000 or other port as specified with `-p {$PORT_NUMBER}`. The API has been implemented using [FastAPI](https://fastapi.tiangolo.com/) with help from [Pydantic](https://pydantic-docs.helpmanual.io/).

Note that you can channel the host on (a) a remote server to the local host on (b) your personal device by running this on (b), 
`ssh -NfL 127.0.0.1:8000:127.0.0.1:8000 {$user_name}@{$remote_host}`

## Miscellaneous

### tmux

Using [tmux](https://github.com/tmux/tmux/wiki) to manage separate sessions which may run on the background. In particular, one may use separate windows for

- "conda": setting up conda environment to test the code

  - need to source and activate
- "git": setting up git to pull and push latest codebase

  - need to

  ```sh
  eval "$(ssh-agent -s)"
  ```

  and

  ```sh
  ssh-add ~/.ssh/{$SSH_PRIVATE_KEY}
  ```
- "directory": for checking out files and directories via terminal

To start a new tmux session, use the command:

```sh
tmux new -s {$SESSION_NAME}
```

To minimize tmux session, use Ctrl+B+D.

To re-join tmux session, use the command:

```sh
tmux attach -t {$SESSION_NAME}
```

To enable scrolling **while inside tmux session** (but disable command paste on terminal), use the command:

```sh
tmux set mouse on
```

Setting mouse off, the scrolling is disabled, but pasting on terminal is possible again.

To make a default setting, add command like `set mouse on` to `~/.tmux.conf`

### Git

It is recommended to set up a SSH keypair between the machine and the remote Git server. In our case, GitHub provides the following [instruction](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).

If not set up properly, the following error might be displayed:

> git@github.com: Permission denied (publickey).
>
> fatal: Could not read from remote repository.

## Conda tips

### YAML file

The following are important fields for making env.yaml file:

- **name**: environment name
- **channels**: list channels to download packages, most likely conda-forge is sufficient. Should check where has the most updated packages
- **dependencies**: list packages to install in the environment
  - Use Conda to install as much as possible, afterwards use pip3
    - For Conda install, list in the following format: {$CHANNEL}::{$PACKAGE_NAME}={$PACKAGE_VERSION} (note the single =)
    - For pip3 install, list in the following format: {$PACKAGE_NAME}=={$PACKAGE_VERSION} (note the double ==)
    - If dependency conflicts arise, use <= instead of strict = or ==
  - package-specific remarks:
    - tensorflow is **never** to be installed with conda per official installation guide, always with pip3
    - lmdb and mlflow official installation guide do not mention Conda, therefore pip3 should be safer
- **prefix**: where to save the environment setup

### Environment variables

One should set up environment variable to be "export" upon activating the environment. Specifically,

- For tensorflow,
  - Double-check the **exact versions** of `cudnn` and `cudatoolkit` from the [official table](https://www.tensorflow.org/install/source#gpu)
  - Set up GPU in `$CONDA_PREFIX/etc/conda/activate.d` following the [instruction](https://www.tensorflow.org/install/pip) by exporting `CUDA_DEVICE_ORDER` and `CUDA_VISIBLE_DEVICES`
- For LMDB,
  - **Do not** set `LMDB_FORCE_CFFI` at all, otherwise LMDB would use c-compiler and raise error

### Bash script permission

Make sure that .sh files in script/\*.sh has the permission at least with `chmod 555`

- Read permission for mlflow logging scripts
- Execute permission for logging resource

### LMDB permission

Make sure that LMDB folders generated in 'model' folder from the model training (usually named `{$DISCIPLINE_NAME}_abstract_vec_lmdb/` or `{$DISCIPLINE_NAME}_abstract_lmdb/`) has all the permissions. Write permission is required even if no writing is made.
